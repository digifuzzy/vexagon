#!/bin/bash
#
# generate icons from SVG file
# dependent on presence of imagemagick
#
CURFLDR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
PROJROOT="$(readlink -f ${CURFLDR}/../)"
# imagemagick command
# convert +antialias
#         -units PixelsPerInch
#         -background none
#         -resizeXxY [src file][out file]
MAGICK="+antialias -units PixelsPerInch -background none"
SVGFILE="${PROJROOT}/images/vexagon.svg"
OUTROOT="${PROJROOT}/runtime/"
SIZES=(16 22 24 32 48)
for sz in "${SIZES[@]}"
do
    RESIZE=" -resize ${sz}x${sz}"
    OUTNAME="${OUTROOT}/vexagon_${sz}.png"
    convert ${MAGICK} ${RESIZE} ${SVGFILE} ${OUTNAME}
done

