/*==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
 ==============================================================================
    vexGameSlot.hpp
    declaration of Game Slot class
    Game Slot can contain a Game Tile
*/
#include "vexGameSlot.hpp"

typedef std::map< short, short >    BoardValues;

//==================== GameSlot - Instance
BoardSlot::BoardSlot() :
    m_bPause( false ),
    m_nTileFontSz( MINIMUM_FONT_SIZE ),
    m_nShadeSz( MINIMUM_SHADE_SIZE ),
    m_PropertySlot(),
    m_PtCenter( wxDefaultPosition ),
    m_polyPerimeter(),
    m_polyInterior(),
    m_regionPerimieter(),
    m_Data()
{}

BoardSlot::BoardSlot( const TileProperty& prop ) :
    m_bPause( false ),
    m_nTileFontSz( MINIMUM_FONT_SIZE ),
    m_nShadeSz( MINIMUM_SHADE_SIZE ),
    m_PropertySlot( prop ),
    m_PtCenter( wxDefaultPosition ),
    m_polyPerimeter(),
    m_polyInterior(),
    m_regionPerimieter(),
    m_Data()
{}

BoardSlot::~BoardSlot()
{
    m_polyPerimeter.clear();
    m_polyInterior.clear();
    m_regionPerimieter.Clear();
}

void  BoardSlot::SlotPaint( wxGraphicsContext* gc )
{
    if(m_bPause) PaintPaused(gc);
    else
    {
        if( !hasTile() ) PaintEmpty(gc);
        else PaintSlotFull(gc);
    }
}

void BoardSlot::CursorPaint( wxGraphicsContext* gc )
{
    //draw socket - whole tile -> game play
    ColourMap cmColours = m_PropertySlot.GetTileColourMap();
    wxPoint ptLogical = GetTileLogicalOrigin();
    wxPoint ptCenterLogical = m_PtCenter - ptLogical;
    for( size_t indexA = 0, indexB = 1;
         indexA < m_Data.getDataSize();
         ++indexA, ++indexB )
    {

        if( indexB >= m_Data.getDataSize() ) indexB = 0;
        wxPoint ptA = m_polyInterior[indexA] - ptLogical;
        wxPoint ptB = m_polyInterior[indexB] - ptLogical;
        PaintTileSide( gc,
                       ptA,
                       ptB,
                       ptCenterLogical,
                       m_Data.getSideValue( indexA ),
                       cmColours );
    }
    //draw shape shading
    ShapeColours shading = m_PropertySlot.PopulateShading();
    wxPoint ptA = m_polyInterior[0] - ptLogical;
    wxPoint ptB;
    for( size_t indexA = 0, indexB = 1;
         indexA < m_polyInterior.size();
         ++indexA, ++indexB )
    {
        if( indexB >= m_polyInterior.size() ) indexB = 0;
        ptB = m_polyInterior[indexB] - ptLogical;
        wxPoint2DDouble ptDblA( ptA );
        wxPoint2DDouble ptDblB( ptB );
        gc->SetPen( wxPen( shading[indexA], m_nShadeSz - 1, wxPENSTYLE_SOLID ) );
        gc->StrokeLine( ptDblA.m_x, ptDblA.m_y, ptDblB.m_x, ptDblB.m_y );
        ptA = ptB;
    }
    gc->Flush();
}

void BoardSlot::SlotResize( const int& drawSize,
                            const wxPoint& origin )
{
    // initialize both perimeter and interior with data points
    m_polyPerimeter.clear();
    m_polyPerimeter = m_PropertySlot.PopulatePoints( drawSize );
    m_polyInterior.clear();
    m_polyInterior = m_polyPerimeter;
    // translate points to a new origin
    for( auto& point : m_polyPerimeter ) point += origin;
    for( auto& point : m_polyInterior ) point += origin;
    // center point calculated from perimeter
    if( m_polyPerimeter.size() > 0 )
    {
        m_regionPerimieter = wxRegion( m_polyPerimeter.size(),
                                       &m_polyPerimeter[0],
                                       wxODDEVEN_RULE );
    }

    m_PtCenter = m_PropertySlot.GetSlotCenter( m_polyPerimeter );
    wxSize tileSize = m_PropertySlot.GetSlotSize( m_polyPerimeter );
    m_nTileFontSz = tileSize.GetHeight() / 8;

    // calculate padding
    // adjust points based on directionality
    //TODO - shading/padding calculation
    // m_nShadeSz = ?

    // set inner tile draw points
    LineAngles angles = m_PropertySlot.GetInsideAngles();
    for( size_t index = 0; index < m_polyPerimeter.size(); ++index )
    {
        m_polyInterior[index] = m_polyPerimeter[index]
                                + vexMath::PolarShrink( angles[index], m_nShadeSz );
    }
}

bool BoardSlot::hasTile()
{
    return m_Data.hasValue();
}

void BoardSlot::swapData( BoardSlot& rhs)
{
    TileData tempData   = m_Data;
    this->m_Data = rhs.m_Data;
    rhs.m_Data = tempData;
}

void BoardSlot::swapData( TileData& data )
{
    TileData tempData   = m_Data;
    m_Data  = data;
    data = tempData;
}

TileData BoardSlot::getData() const
{
	return m_Data;
}

void BoardSlot::setData( const TileValues& inValues,
                         const wxPoint& ptStart,
                         const wxPoint& ptSolve )
{
    m_Data = TileData( inValues,
                       m_PropertySlot.PtToMap( ptStart ),
                       m_PropertySlot.PtToMap( ptSolve ) );
}

short BoardSlot::getSideValue( const short& side ) const
{
    return m_Data.getSideValue( side );
}

short BoardSlot::getMapStart() const
{
	return m_Data.getStart();
}

short BoardSlot::getMapSolve() const
{
	return m_Data.getSolve();
}


bool BoardSlot::containsPt( const wxPoint& ptSearch )
{
    return ( m_regionPerimieter.Contains( ptSearch ) == wxInRegion );
}

void BoardSlot::setPause( const bool& bPause )
{
    m_bPause = bPause;
}

wxPoint BoardSlot::GetTileLogicalOrigin() const
{
    wxPoint retPt( m_PtCenter );
    for( ShapePoints::const_iterator cIT = m_polyPerimeter.begin();
         cIT != m_polyPerimeter.end();
         ++cIT )
    {
        wxPoint localPt( *cIT );
        retPt.x = wxMin( retPt.x, localPt.x );
        retPt.y = wxMin( retPt.y, localPt.y );
    }
    return retPt;
}

wxSize BoardSlot::GetSlotSize()
{
    return m_PropertySlot.GetSlotSize( m_polyPerimeter );
}

void BoardSlot::PaintEmpty( wxGraphicsContext* gc )
{
    // empty socket - no data
    //default to pen boarder and brush shading
    wxGraphicsPath path = gc->CreatePath();
    //gc->SetPen( wxPen( DEFAULT_PEN_BORDER ) );
    gc->SetPen( wxPen( wxColour( "LIGHT GREY" ) ) );
    gc->SetAntialiasMode( wxANTIALIAS_DEFAULT );
    gc->SetInterpolationQuality( wxINTERPOLATION_GOOD );

    path.MoveToPoint( wxPoint2DDouble( m_polyPerimeter[0] ) );
    for(unsigned int index = 1; index < m_polyPerimeter.size(); ++index)
    {
        path.AddLineToPoint( wxPoint2DDouble( m_polyPerimeter[index] ) );
    }
    path.CloseSubpath();

    //gc->SetBrush( wxBrush( DEFAULT_BRUSH_BORDER ) );
    gc->SetBrush( wxBrush( wxColour( "GREY" ) ) );
    gc->FillPath( path );
    gc->StrokePath( path );
    gc->Flush();
}

void BoardSlot::PaintPaused( wxGraphicsContext* gc )
{
    //draw socket - whole tile -> game paused
    //pen boarder and brush pause shading
    //gc->SetPen( wxPen( DEFAULT_PEN_PAUSE ) );
    gc->SetPen( wxPen( wxColour( "WHITE" ) ) );
    //gc->SetBrush( wxBrush( DEFAULT_BRUSH_PAUSE ) );
    gc->SetBrush( wxBrush( wxColour( 24, 24, 24) ) );
    gc->SetAntialiasMode( wxANTIALIAS_DEFAULT );
    gc->SetInterpolationQuality( wxINTERPOLATION_GOOD );

    wxGraphicsPath path = gc->CreatePath();
    path.MoveToPoint( wxPoint2DDouble( m_polyPerimeter[0] ) );
    for(unsigned int index = 1; index < m_polyPerimeter.size(); ++index)
    {
        path.AddLineToPoint( wxPoint2DDouble( m_polyPerimeter[index] ) );
    }
    path.CloseSubpath();
    gc->FillPath( path, wxODDEVEN_RULE );
    gc->StrokePath( path );
    gc->Flush();

    // Display value
    wxString displayText = "?";
    wxDouble extX = 0;
    wxDouble extY = 0;
    gc->SetFont( wxFont( ( m_nTileFontSz * 3),
                         wxFONTFAMILY_MODERN,
                         wxFONTSTYLE_NORMAL,
                         wxFONTWEIGHT_BOLD),
                         wxColour( "WHITE" ) ); // DEFAULT_PEN_PAUSE );
    gc->GetTextExtent( displayText, &extX, &extY, NULL, NULL );
    // get center of text extent
    wxPoint textext( ( extX / 2 ), ( extY / 2 ) );
    // translate to x,y of where to draw
    wxRealPoint Draw( m_PtCenter - textext );
    // draw text/value
    gc->DrawText( displayText, Draw.x, Draw.y );
    gc->Flush();
}

void BoardSlot::PaintSlotFull( wxGraphicsContext* gc )
{
    //draw socket - whole tile -> game play
    ColourMap cmColours = m_PropertySlot.GetTileColourMap();
    gc->SetAntialiasMode( wxANTIALIAS_DEFAULT );
    gc->SetInterpolationQuality( wxINTERPOLATION_GOOD );
    for( size_t indexA = 0, indexB = 1;
         indexA < m_Data.getDataSize();
         ++indexA, ++indexB )
    {
        if( indexB >= m_Data.getDataSize() ) indexB = 0;
        PaintTileSide( gc,
                       m_polyInterior[indexA],
                       m_polyInterior[indexB],
                       m_PtCenter,
                       m_Data.getSideValue( indexA ),
                       cmColours );
    }

    //draw shape perimeter
    ShapeColours shading = m_PropertySlot.PopulateShading();
    wxPoint ptA = m_polyInterior[0];
    wxPoint ptB;
    for( size_t indexA = 0, indexB = 1;
         indexA < m_polyInterior.size();
         ++indexA, ++indexB )
    {
        if( indexB >= m_polyInterior.size() ) indexB = 0;
        ptB = m_polyInterior[indexB];
        wxPoint2DDouble ptDblA( ptA );
        wxPoint2DDouble ptDblB( ptB );
        gc->SetPen( wxPen( shading[indexA], m_nShadeSz, wxPENSTYLE_SOLID ) );
        gc->StrokeLine( ptDblA.m_x, ptDblA.m_y, ptDblB.m_x, ptDblB.m_y );
        ptA = ptB;
    }
    gc->Flush();
}

void BoardSlot::PaintTileSide( wxGraphicsContext* gc,
                               const wxPoint& ptA,
                               const wxPoint& ptB,
                               const wxPoint& ptC,
                               const short& sideValue,
                               const ColourMap& cmColours )
{
    // draw side of tile - triangle shape
    wxPoint2DDouble ptDblB( ptB );
    wxPoint2DDouble ptDblC( ptC ); // center of slot

    wxColour tileBrushColour;
    wxColour tilePenColour;
    if( (sideValue < 0 ) || ( sideValue >= m_PropertySlot.GetBase() ) )
    {
        //tileBrushColour = DEFAULT_BRUSH_SHADING;
        tileBrushColour = wxColour( "LIGHT GREY" );
        //tilePenColour   = DEFAULT_PEN_SHADING;
        tilePenColour   = wxColour( "GREY" );
        std::cout << "Draw Triangle Bad Value: " << sideValue << std::endl;
    }
    else
    {
        tileBrushColour = cmColours.at(sideValue).mColourBrush;
        tilePenColour   = cmColours.at(sideValue).mColourPen;
    }

    gc->SetBrush( wxBrush( tileBrushColour ) );
    gc->SetAntialiasMode( wxANTIALIAS_DEFAULT );

    wxGraphicsPath valuepath = gc->CreatePath();
    valuepath.MoveToPoint( ptDblC );
    valuepath.AddLineToPoint( wxPoint2DDouble( ptA ) );
    valuepath.AddLineToPoint( ptDblB );
    valuepath.CloseSubpath();
    gc->FillPath(valuepath,wxWINDING_RULE );

    // draw border/outline
    //gc->SetPen( wxPen( DEFAULT_PEN_SHADING, 2 ) );
    gc->SetPen( wxPen( wxColour( "GREY" ), 2 ) );
    gc->StrokeLine( ptDblC.m_x, ptDblC.m_y, ptDblB.m_x, ptDblB.m_y );

    // Display value
    wxString displayText = wxString::Format(" %x ",sideValue );
    displayText.MakeUpper();
    wxDouble extX = 0;
    wxDouble extY = 0;

    gc->SetFont( wxFont( m_nTileFontSz,
                         wxFONTFAMILY_MODERN,
                         wxFONTSTYLE_NORMAL,
                         wxFONTWEIGHT_BOLD),
                         tilePenColour );

    gc->GetTextExtent( displayText, &extX, &extY, NULL, NULL );
    // get center of text extent
    wxRealPoint textext( ( extX / 2 ), ( extY / 2 ) );
    // translate to x,y of where to draw
    // Point Q is end pt of line from center
    // line Center/Q is tangent to line ptA/ptB
    wxPoint Q( ( ptA.x + ( ( ptB.x - ptA.x ) / 2)),
               ( ptA.y + ( ( ptB.y - ptA.y ) / 2)));
    wxRealPoint Draw( ( ptC.x + ( 7 * ( Q.x - ptC.x ) / 10 ) ),
                      ( ptC.y + ( 7 * ( Q.y - ptC.y ) / 10 ) ) );
    wxRealPoint locText = Draw - textext;
    // draw text/value
    gc->DrawText( displayText, locText.x, locText.y );
    gc->Flush();
}
