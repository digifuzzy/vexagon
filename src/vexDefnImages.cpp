/*==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
 ==============================================================================
    vexDefnImages.cpp
    definition of image variables and implementation of image copy routines
*/

#include "vexDefnImages.hpp"

const wxSize DEFAULT_BITMAP_SIZE    { wxSize(32,32) };

namespace vexImage
{
    std::map<unsigned short, vexImage_Item> vexImage_Items = {
        // game icon
        { vexIMG_IDX_ICON,       VEXIMAGE( vexagon_img ) },
        // toolbar images
        { vexIMG_IDX_CHANGE,     VEXIMAGE( Game_Change_32 ) },
        { vexIMG_IDX_NEW,        VEXIMAGE( Game_New_32 ) },
        { vexIMG_IDX_PAUSE,      VEXIMAGE( Game_Pause_32 ) },
        { vexIMG_IDX_RESET,      VEXIMAGE( Game_Reset_32 ) },
        { vexIMG_IDX_RESUME,     VEXIMAGE( Game_Resume_32 ) },
        { vexIMG_IDX_SOLVE,      VEXIMAGE( Game_Solve_32 ) },
        { vexIMG_IDX_MV_UP,      VEXIMAGE( Game_Move_Up_32 ) },
        { vexIMG_IDX_MV_UPRT,    VEXIMAGE( Game_Move_Up_Rt_32 ) },
        { vexIMG_IDX_MV_RT,      VEXIMAGE( Game_Move_Right_32 ) },
        { vexIMG_IDX_MV_DNRT,    VEXIMAGE( Game_Move_Dn_Rt_32 ) },
        { vexIMG_IDX_MV_DN,      VEXIMAGE( Game_Move_Down_32 ) },
        { vexIMG_IDX_MV_DNLT,    VEXIMAGE( Game_Move_Dn_Lt_32 ) },
        { vexIMG_IDX_MV_LT,      VEXIMAGE( Game_Move_Left_32 ) },
		{ vexIMG_IDX_MV_UPLT,    VEXIMAGE( Game_Move_Up_Lt_32 ) },
        // menu images
        { vexIMG_IDX_CHANGE_SM,  VEXIMAGE( Game_Change_16 ) },
        { vexIMG_IDX_NEW_SM,     VEXIMAGE( Game_New_16 ) },
        { vexIMG_IDX_PAUSE_SM,   VEXIMAGE( Game_Pause_16 ) },
        { vexIMG_IDX_RESET_SM,   VEXIMAGE( Game_Reset_16 ) },
        { vexIMG_IDX_RESUME_SM,  VEXIMAGE( Game_Resume_16 ) },
        { vexIMG_IDX_SOLVE_SM,   VEXIMAGE( Game_Solve_16 ) },
        { vexIMG_IDX_MV_UP_SM,   VEXIMAGE( Game_Move_Up_16 ) },
        { vexIMG_IDX_MV_UPRT_SM, VEXIMAGE( Game_Move_Up_Rt_16 ) },
        { vexIMG_IDX_MV_RT_SM,   VEXIMAGE( Game_Move_Right_16 ) },
        { vexIMG_IDX_MV_DNRT_SM, VEXIMAGE( Game_Move_Dn_Rt_16 ) },
        { vexIMG_IDX_MV_DN_SM,   VEXIMAGE( Game_Move_Down_16 ) },
        { vexIMG_IDX_MV_DNLT_SM, VEXIMAGE( Game_Move_Dn_Lt_16 ) },
		{ vexIMG_IDX_MV_LT_SM,   VEXIMAGE( Game_Move_Left_16 ) },
		{ vexIMG_IDX_MV_UPLT_SM, VEXIMAGE( Game_Move_Up_Lt_16 ) },
        // How To images
        { vexIMG_IDX_CTRLS,      VEXIMAGE( Game_Ctrls ) },
        { vexIMG_IDX_TIPS,       VEXIMAGE( Game_Tips ) },
        { vexIMG_IDX_CANCEL,     VEXIMAGE( Action_Cancel ) }
    };

    wxBitmap vexBitmap( vexIMAGE_INDEX imageindex )
    {
        wxBitmap retBM;
        vexImage_Item item = vexImage_Items[imageindex];
        // TODO - error guard if not found
        if(VEX_BM_TYPE == wxBITMAP_TYPE_PNG)
        {
            retBM = wxBitmap::NewFromPNGData(item.IMGdata, item.IMGsize);
        }
        else if(VEX_BM_TYPE == wxBITMAP_TYPE_XPM)
        {
            retBM = wxBitmap(item.IMGdata);
        }
        else
        {
            retBM = wxNullBitmap;
        }
        return retBM;
    }

    wxIcon vexIcon( vexIMAGE_INDEX imageindex )
    {
        wxIcon retIcon;
        retIcon.CopyFromBitmap(vexBitmap(imageindex));
        return retIcon;
    }
};
