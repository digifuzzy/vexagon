/*==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
 ==============================================================================
    vexDialogs.cpp
    implementation of dialog classes in vexagon

    DlgNewBoard   - change type/size/base for new puzzle
    DlgGameSolved - user has solved current puzzle
    DlgHowTo      - information on how to play vexagon
*/

#include "vexDialogs.hpp"
#include "vexGameBoard.hpp"
#include "vexGamePanel.hpp"

//==================== Change Board Dialog
DlgNewBoard::DlgNewBoard( const TileProperty& property,
                          wxWindow* parent ):
    wxDialog( parent,
              wxID_ANY,
              wxEmptyString,
              wxDefaultPosition,
              wxDefaultSize,
              wxDEFAULT_DIALOG_STYLE ),
    m_PropertyDialog( property )
{
    wxString vexCHGDLG_TYPE_LABEL( " Board Type " );
    wxString vexCHGDLG_SIZE_LABEL( " Board Dimensions " );
    wxString vexCHGDLG_SIZE_HORZ( " Horizontal " );
    wxString vexCHGDLG_SIZE_VERT( " Vertical " );
    wxString vexCHGDLG_BASE_LABEL( " Numeric Base " );

    wxGridBagSizer* MainSizer = new wxGridBagSizer();

    //----- controls to manipulate chosen board
    // ===== Board Type =====
    wxBoxSizer* CtrlSizer = new wxBoxSizer( wxVERTICAL );
    wxStaticBoxSizer* pbxType = new wxStaticBoxSizer( wxVERTICAL,
                                                      this,
                                                      vexCHGDLG_TYPE_LABEL );
    mpch_BoardType = new  wxChoice( this,
                                    ID_DLG_BOARD_TYPE,
                                    wxDefaultPosition,
                                    wxDefaultSize,
                                    TileProperty::nBoardTypesCount,
                                    TileProperty::wxsaBoardTypes );
    int nSel = mpch_BoardType->FindString( m_PropertyDialog.GetTypeAsString() );
    mpch_BoardType->SetSelection( nSel );
    pbxType->Add( mpch_BoardType, wxSizerFlags( 1 ).Align( wxALIGN_CENTER ).Border( wxALL, 10 ) );
    CtrlSizer->Add( pbxType, wxSizerFlags( 0 ).Expand().Border( wxALL, 5 ) );

    // ===== Board Dimensions =====
    wxStaticBoxSizer* pbxDimensions = new wxStaticBoxSizer( wxVERTICAL,
                                                            this,
                                                            vexCHGDLG_SIZE_LABEL );
    wxBoxSizer* pHorz = new wxBoxSizer( wxHORIZONTAL );
    // = Horizontal Value =
    wxStaticText* pHtxt = new wxStaticText( this,
                                            wxID_ANY,
                                            vexCHGDLG_SIZE_HORZ,
                                            wxDefaultPosition,
                                            wxDefaultSize,
                                            wxALIGN_RIGHT );
    pHorz->Add( pHtxt, wxSizerFlags( 1 ).Align( wxALIGN_CENTER_VERTICAL ).Border( wxALL, 10 ) );
    mpctrl_dim_horz = new wxSpinCtrl( this,
                                       ID_DLG_DIM_HORZ,
                                       vexCHGDLG_SIZE_HORZ,
                                       wxDefaultPosition,
                                       wxDefaultSize,
                                       ( wxSP_ARROW_KEYS | wxSP_VERTICAL ) );
    mpctrl_dim_horz->SetRange( BOARD_SIZE_MIN, BOARD_SIZE_MAX );
    mpctrl_dim_horz->SetValue( m_PropertyDialog.GetDimensions().x );
    pHorz->Add( mpctrl_dim_horz, wxSizerFlags( 0 ).Align( wxALIGN_CENTER_VERTICAL ).Border( wxALL, 10 ) );
    pbxDimensions->Add( pHorz, wxSizerFlags( 0 ).Expand().Border( wxALL, 5 ) );
    // = Vertical Value =
    wxBoxSizer* pVert = new wxBoxSizer( wxHORIZONTAL );
    wxStaticText* pVtxt = new wxStaticText( this,
                                            wxID_ANY,
                                            vexCHGDLG_SIZE_VERT,
                                            wxDefaultPosition,
                                            wxDefaultSize,
                                            wxALIGN_RIGHT );
    pVert->Add( pVtxt, wxSizerFlags( 1 ).Align( wxALIGN_CENTER_VERTICAL ).Border( wxALL, 10 ) );
    mpctrl_dim_vert = new wxSpinCtrl( this,
                                  ID_DLG_DIM_VERT,
                                  vexCHGDLG_SIZE_VERT,
                                  wxDefaultPosition,
                                  wxDefaultSize,
                                  ( wxSP_ARROW_KEYS | wxSP_VERTICAL ) );
    mpctrl_dim_vert->SetRange( BOARD_SIZE_MIN, BOARD_SIZE_MAX );
    mpctrl_dim_vert->SetValue( m_PropertyDialog.GetDimensions().y );
    pVert->Add( mpctrl_dim_vert, wxSizerFlags( 0 ).Align( wxALIGN_CENTER_VERTICAL ).Border( wxALL, 10 ) );
    pbxDimensions->Add( pVert, wxSizerFlags( 0 ).Expand().Border( wxALL, 5 ) );
    CtrlSizer->Add( pbxDimensions, wxSizerFlags( 0 ).Expand().Border( wxALL, 5 ) );
    // ===== Board Base =====
    wxStaticBoxSizer* pbxBase = new wxStaticBoxSizer( wxVERTICAL,
                                                      this,
                                                      vexCHGDLG_BASE_LABEL );
    mpch_BoardBase = new  wxChoice( this,
                                    ID_DLG_TILE_BASE,
                                    wxDefaultPosition,
                                    wxDefaultSize,
                                    TileProperty::nBoardBasesCount,
                                    TileProperty::wxsaBoardBases );
    nSel = mpch_BoardBase->FindString( m_PropertyDialog.GetBaseAsString() );
    mpch_BoardBase->SetSelection( nSel );
    pbxBase->Add( mpch_BoardBase, wxSizerFlags( 1 ).Align( wxALIGN_CENTER ).Border( wxALL, 10 ) );
    CtrlSizer->Add( pbxBase, wxSizerFlags( 0 ).Expand().Border( wxALL, 5 ) );

    //TODO - update when wxGridBagSizer updated to use wxSizerFlags
    MainSizer->Add( CtrlSizer, wxGBPosition( 0, 0 ), wxDefaultSpan, wxEXPAND | wxALL, 5 );
    //----- End controls

    //----- Sample of chosen board
    wxBoxSizer* SampleSizer = new wxBoxSizer( wxHORIZONTAL );
    mp_BoardSample = new GamePanel( m_PropertyDialog, this , wxID_ANY, true );
    SampleSizer->Add( mp_BoardSample, wxSizerFlags( 1 ).Expand().Border( wxALL, 10 ) );
    //TODO - update when wxGridBagSizer updated to use wxSizerFlags
    MainSizer->Add( SampleSizer, wxGBPosition( 0, 1 ), wxDefaultSpan, wxEXPAND | wxALL, 5 );
    //----- End Sample

    //----- Dialog Cancel/Okay
    wxSizer* dlgCtrlsizer = this->CreateButtonSizer( wxCANCEL | wxOK );
    //TODO - update when wxGridBagSizer updated to use wxSizerFlags
    MainSizer->Add( dlgCtrlsizer, wxGBPosition( 1, 1 ), wxDefaultSpan, wxEXPAND | wxALL, 10 );

    SetSizerAndFit( MainSizer );
    Layout();
    Update();
    Bind( wxEVT_CHOICE, &DlgNewBoard::OnChoiceSelect, this, wxID_ANY );
    Bind( wxEVT_BUTTON, &DlgNewBoard::OnClose, this, wxID_ANY );
    Bind( wxEVT_SPINCTRL, &DlgNewBoard::OnChangeDimensions, this, wxID_ANY );
};

DlgNewBoard::~DlgNewBoard()
{
    Unbind( wxEVT_CHOICE, &DlgNewBoard::OnChoiceSelect, this, wxID_ANY );
    Unbind( wxEVT_BUTTON, &DlgNewBoard::OnClose, this, wxID_ANY );
    Unbind( wxEVT_SPINCTRL, &DlgNewBoard::OnChangeDimensions, this, wxID_ANY );
}

void DlgNewBoard::OnChoiceSelect( wxCommandEvent& event )
{
    long sel = event.GetSelection();
    if( event.GetId() == ID_DLG_BOARD_TYPE )
    {
        m_PropertyDialog.SetType( TileProperty::wxsaBoardTypes[ sel ] );
    }
    else if( event.GetId() == ID_DLG_TILE_BASE )
    {
        m_PropertyDialog.SetBase( TileProperty::wxsaBoardBases[ sel ] );
    }
    mp_BoardSample->ReInitPanel( m_PropertyDialog );
    Update();
}

void DlgNewBoard::OnChangeDimensions( wxSpinEvent& event )
{
    if( event.GetId() == ID_DLG_DIM_HORZ )
    {
        m_PropertyDialog.SetDimensions( event.GetPosition(),
                                        m_PropertyDialog.GetDimensions().y );
    }
    else if( event.GetId() == ID_DLG_DIM_VERT )
    {
        m_PropertyDialog.SetDimensions( m_PropertyDialog.GetDimensions().x,
                                       event.GetPosition() );
    }
    mp_BoardSample->ReInitPanel( m_PropertyDialog );
    Update();
}

void DlgNewBoard::OnClose( wxCommandEvent& event )
{
    EndModal( event.GetId() );
    Destroy();
}

//==================== GameSolved Dialog
DlgGameSolved::DlgGameSolved ( const TileProperty& property,
                               const long& elapsedtime,
                               wxWindow* parent ) :
    wxDialog( parent,
              wxID_ANY,
              wxEmptyString,
              wxDefaultPosition,
              wxDefaultSize,
              wxFRAME_TOOL_WINDOW | wxCLOSE_BOX ),
    monoFont( wxFont( DIALOG_FONT_MONO_SIZE,
                      wxFONTFAMILY_TELETYPE,
                      wxFONTSTYLE_NORMAL,
                      wxFONTWEIGHT_NORMAL,
                      false ) )
{
    SetSize( wxSize( 300, 300 ) );
    SetFont( monoFont );

    wxString BoardName = property.GetTypeAsString();
    wxString BaseName  = property.GetBaseAsString();
    wxPoint  BoardDim = property.GetDimensions();
    wxString strCHANGE( "Change Board" );
    wxString strSAME( "Same Board" );
    wxString strCANCEL( "Cancel" );
    wxString strFormatSOLVED("You solved a %d x %d %s puzzle\nwith %s base in %s.\nPlay Again?");

    wxString msgStr = wxString::Format( strFormatSOLVED,
                                        BoardDim.x,
                                        BoardDim.y,
                                        BoardName,
                                        BaseName,
                                        vexTime::TimeMsg( elapsedtime, true ) );

    wxStdDialogButtonSizer* btnSizer = new wxStdDialogButtonSizer();
    wxButton* btnGameChg = new wxButton( this, ID_GAMENEWCHG, strCHANGE );
    btnGameChg->SetBitmap( vexImage::vexBitmap( vexIMG_IDX_CHANGE ) );
    btnSizer->Add( btnGameChg, wxSizerFlags( 3 ).Border( wxALL, 2 ) );

    wxButton* btnGameNew = new wxButton( this, ID_GAMENEW, strSAME );
    btnGameNew->SetBitmap( vexImage::vexBitmap( vexIMG_IDX_NEW ) );
    btnSizer->Add( btnGameNew, wxSizerFlags( 3 ).Border( wxALL, 2 ) );

    btnSizer->AddStretchSpacer();

    wxButton* btnCancel  = new wxButton( this, wxID_CANCEL, strCANCEL );
    btnCancel->SetBitmap( vexImage::vexBitmap( vexIMG_IDX_CANCEL ) );
    btnSizer->Add( btnCancel, wxSizerFlags( 2 ).Expand().Border( wxALL, 2 ) );

    wxBoxSizer* CtrlSizer = new wxBoxSizer( wxVERTICAL );
    CtrlSizer->AddSpacer( 10 );
    CtrlSizer->Add( this->CreateTextSizer( msgStr ),
                    wxSizerFlags( 0 ).Align( wxALIGN_CENTER_HORIZONTAL) );
    CtrlSizer->AddSpacer( 10 );
    CtrlSizer->Add( btnSizer, wxSizerFlags( 0 ).Expand().Border( wxALL, 5 ) );
    SetSizerAndFit( CtrlSizer );
    Layout();

    btnCancel->SetFocus();

    Bind( wxEVT_BUTTON, &DlgGameSolved::OnClose, this, wxID_ANY );
}

DlgGameSolved::~DlgGameSolved()
{
    Unbind( wxEVT_BUTTON, &DlgGameSolved::OnClose, this, wxID_ANY );
}

void DlgGameSolved::OnClose( wxCommandEvent& event )
{
    EndModal( event.GetId() );
    Destroy();
}

//==================== How-To Dialog
int  DlgHowTo::m_nHorzCharCount = 72;
DlgHowTo::DlgHowTo( wxWindow* parent,
                    wxWindowID id,
                    const wxString& title,
                    const wxPoint& pos,
                    const wxSize& size,
                    long style,
                    const wxString& name ) :
    wxDialog( parent, id, title, pos, size, style, name ),
	szImage( 32, 32 ),
	monoFont( wxFont( DIALOG_FONT_MONO_SIZE,
                      wxFONTFAMILY_TELETYPE,
                      wxFONTSTYLE_NORMAL,
                      wxFONTWEIGHT_NORMAL,
                      false ) )
{
    SetFont( monoFont );
	// wxW Bug - wxMac variant does not respect wrap sizing of static text
	// force the issue by defining control AND text wrap size
	wxSize WrapSize = GetTextExtent( wxT("W"));
    wxSize TempSize;
#if defined(_WXVER_3100_)
	TempSize = FromDIP(WrapSize);
#else
    // backporting wxWindow::FromDIP code from wxW version 3.1.0
    int localBASE_DPI = 96;
    wxSize dpi = wxScreenDC().GetPPI();
    // Take care to not scale -1 because it has a special meaning of
    // "unspecified" which should be preserved.
    TempSize = wxSize(WrapSize.x == -1 ? -1 : wxMulDivInt32(WrapSize.x, dpi.x, localBASE_DPI),
                      WrapSize.y == -1 ? -1 : wxMulDivInt32(WrapSize.y, dpi.y, localBASE_DPI));
#endif
	m_nLineWrap = TempSize.x * DlgHowTo::m_nHorzCharCount;

    wxBoxSizer* boxDialog = new wxBoxSizer( wxVERTICAL );

    wxImageList* m_imageList = new wxImageList( szImage.GetWidth(),
                                                szImage.GetHeight() );
    m_imageList->Add( vexImage::vexBitmap( vexIMG_IDX_ICON ) );
    m_imageList->Add( vexImage::vexBitmap( vexIMG_IDX_CTRLS ) );
    m_imageList->Add( vexImage::vexBitmap( vexIMG_IDX_SOLVE ) );
    m_imageList->Add( vexImage::vexBitmap( vexIMG_IDX_CHANGE ) );
    m_imageList->Add( vexImage::vexBitmap( vexIMG_IDX_TIPS ) );

    mp_Book = new wxToolbook( this,
                              wxID_ANY,
                              wxDefaultPosition,
                              wxDefaultSize,
                              wxBK_LEFT );
    mp_Book->SetImageList( m_imageList );

    int sequence_value = 0;
    MakeBasics( sequence_value++, true );
    MakeControls( sequence_value++ );
    MakeSolvedDlg( sequence_value++ );
    MakeChangeDlg( sequence_value++ );
    MakeTips( sequence_value++ );
    mp_Book->Layout();
    boxDialog->Add( mp_Book, wxSizerFlags( 1 )
                             .Expand()
                             .Border( wxALL, 5 ) );

    wxStdDialogButtonSizer* btnSizer = new wxStdDialogButtonSizer();
    wxButton* m_btnOK = new wxButton( this, wxID_OK );
    btnSizer->Add( m_btnOK );
    boxDialog->Add( btnSizer, wxSizerFlags( 0 )
                              .Align( wxALIGN_RIGHT )
                              .Border( wxALL, 10 ) );

    SetSizerAndFit( boxDialog );
    Layout();
    m_btnOK->SetFocus();
}

DlgHowTo::~DlgHowTo()
{}

void DlgHowTo::OnClose( wxCommandEvent& WXUNUSED( event ) )
{
    EndModal( wxID_OK );
    Destroy();
}

void DlgHowTo::MakeBasics( const int& sequence, const bool bSelected )
{
    wxString vexHT_TAB_BASIC( " Basics " );
    wxString vexHT_BASIC_LABEL( "  Playing Vexagon  " );
    wxString vexHT_BASICS_TEXT (
        "Game Panel is divided into two halves:\n"
        "( Left ) Game Board     ( Right ) Well Board\n"
        "\n"
        "All tiles are scrambled and placed in the Well Board at start. Move"
        " all pieces from Well Board to the Game Board such that the sides of"
        " adjacent tiles match in value.\n"
        "\n"
        "Moving a tile means to left click on a tile, holding the mouse"
        " down, and drag the tile to a selected location. Releasing the"
        " mouse button places the tile.\n"
        "\n"
        "If the tile is not within one of the two boards, or the tile is in"
        " the Game Board but the side values do not match an adjoining tile,"
        " the tile will be returned to its location before selection.\n"
        "\n"
        "When moving tiles onto the Well Board, side values are not"
        " considered. The Well Board is a \"storage area\".\n"
        "\n"
        "Puzzle is solved when all tiles are placed in the Game Board.\n"
        "\n"
        "All games have a solution."
    );

    wxPanel* pPanel = new wxPanel( mp_Book );
    wxStaticBoxSizer* sbsGamePlay = new wxStaticBoxSizer( wxVERTICAL,
                                                          pPanel,
                                                          vexHT_BASIC_LABEL );
    wxStaticText* pText = new wxStaticText( sbsGamePlay->GetStaticBox(),
											wxID_ANY,
											vexHT_BASICS_TEXT);
    pText->SetFont( monoFont );
	pText->Wrap(m_nLineWrap);

    sbsGamePlay->Add( pText, wxSizerFlags( 1 )
                             .Expand()
                             .Border( wxALL, 10 ) );

    pPanel->SetSizerAndFit( sbsGamePlay );
    mp_Book->AddPage( pPanel, vexHT_TAB_BASIC, bSelected, sequence );
}

void DlgHowTo::MakeControls( const int& sequence, const bool bSelected )
{
    wxString vexHT_TAB_CONTROLS( "Controls" );
    wxString vexHT_LABEL_CONTROLS( "  Controls  " );
    std::vector<wxString> vexHT_CONTROLS_TEXT = {
    //[Change Board]
        wxString(
        "Starts \"Change Board\" dialog and can start a new game with new settings."),
    //[New Game]
        wxString(
        "Starts a new game with the current board settings."),
    //[Solve Game]
        wxString(
        "Solves the current puzzle board."),
    //[Restart Game]
        wxString(
        "Restores all tiles to their starting locations on the Well Board and"
        " resets the game clock."),
    //[Pause Game]
        wxString(
        "Pause will hide tile values and pause game clock."
        " Also, Game is paused when window is minimized."),
    //[Resume Game]
        wxString(
        "Shows tiles and resumes game clock."),
    };

    wxPanel* pPanel = new wxPanel( mp_Book );
    wxStaticBoxSizer* sbsControls = new wxStaticBoxSizer( wxVERTICAL,
                                                          pPanel,
                                                          vexHT_LABEL_CONTROLS );

    wxImageList* ctrlImgs = new wxImageList( szImage.GetWidth(),
                                             szImage.GetHeight() );
    ctrlImgs->Add( vexImage::vexBitmap( vexIMG_IDX_CHANGE ) );
    ctrlImgs->Add( vexImage::vexBitmap( vexIMG_IDX_NEW ) );
    ctrlImgs->Add( vexImage::vexBitmap( vexIMG_IDX_SOLVE ) );
    ctrlImgs->Add( vexImage::vexBitmap( vexIMG_IDX_RESET ) );
    ctrlImgs->Add( vexImage::vexBitmap( vexIMG_IDX_PAUSE ) );
    ctrlImgs->Add( vexImage::vexBitmap( vexIMG_IDX_RESUME ) );
    sbsControls->AddSpacer( 10 );
    int nImgIndex = 0;
    for( wxString& str: vexHT_CONTROLS_TEXT )
    {
        wxBoxSizer* pbxRow = new wxBoxSizer( wxHORIZONTAL );
        wxStaticBitmap* pImg = new wxStaticBitmap( sbsControls->GetStaticBox(),
                                                   wxID_ANY,
                                                   ctrlImgs->GetBitmap(nImgIndex));
        pbxRow->Add( pImg, wxSizerFlags( 0 )
                           .Align( wxALIGN_CENTER_VERTICAL )
                           .Border( wxLEFT, 10) );
        wxStaticText* pLabel = new wxStaticText( sbsControls->GetStaticBox(),
												 wxID_ANY,
												 str);
        pLabel->SetFont( monoFont );
		pLabel->Wrap(m_nLineWrap);

        pbxRow->Add( pLabel, wxSizerFlags( 1 )
                             .Align( wxALIGN_CENTER_VERTICAL )
                             .Border( wxLEFT | wxRIGHT, 10 ) );
        sbsControls->Add( pbxRow, wxSizerFlags( 0 )
                                  .Expand()
                                  .Border( wxTOP | wxBOTTOM, 5 ) );
        ++nImgIndex;
    }
    sbsControls->AddSpacer( 10 );
/*
    //[Move Tiles Up]
        wxString(
        "Move tiles on Game Board up one row."),
    //[Move Tiles Right]
        wxString(
        "Move tiles on Game Board right one column.\n"
        "Note: Currently disabled for Hexagon tile boards"),
    //[Move Tiles Down]
        wxString(
        "Move tiles on Game Board down one row."),
    //[Move Tiles Left]
        wxString(
        "Move tiles on Game Board left one column.\n"
        "Note: Currently disabled for Hexagon tile boards")
    ctrlImgs->Add( vexImage::vexBitmap( vexIMG_IDX_MV_UP ) );
    ctrlImgs->Add( vexImage::vexBitmap( vexIMG_IDX_MV_RT ) );
    ctrlImgs->Add( vexImage::vexBitmap( vexIMG_IDX_MV_DN ) );
    ctrlImgs->Add( vexImage::vexBitmap( vexIMG_IDX_MV_LT ) );
 */

    pPanel->SetSizerAndFit( sbsControls );
    mp_Book->AddPage( pPanel, vexHT_TAB_CONTROLS, bSelected, sequence );
}

void DlgHowTo::MakeSolvedDlg( const int& sequence, const bool bSelected )
{
    wxString vexHT_TAB_SOLVED( " Solved Dialog " );
    wxString vexHT_LABEL_SOLVED( "  Solved Dialog  " );
    wxString vexHT_DLG_Solved(
        "When the user solves the current board, success is indicated"
        " by the appearance of this dialog. The user can then choose from"
        " one of the following choices:"
    );

    std::vector<wxString> vexHT_Solved = {
        wxString( "Change board settings\n( brings up Change Board Dialog )" ),
        wxString( "Start a new puzzle with current settings" ),
        wxString( "Cancel - closes dialog\n( no action taken )" )
    };

    wxPanel* pPanel = new wxPanel( mp_Book );
    wxStaticBoxSizer* sbsSolved = new wxStaticBoxSizer( wxVERTICAL,
                                                        pPanel,
                                                        vexHT_LABEL_SOLVED );

    wxImageList* ctrlImgs = new wxImageList( szImage.GetWidth(),
                                             szImage.GetHeight() );
    ctrlImgs->Add( vexImage::vexBitmap( vexIMG_IDX_CHANGE ) );
    ctrlImgs->Add( vexImage::vexBitmap( vexIMG_IDX_NEW ) );
    ctrlImgs->Add( vexImage::vexBitmap( vexIMG_IDX_CANCEL ) );

    int nRowSpan = 1;
    wxGridBagSizer* gbSizer = new wxGridBagSizer( 5, 20 );

    wxStaticText* pText = new wxStaticText( sbsSolved->GetStaticBox(),
											wxID_ANY,
											vexHT_DLG_Solved);
    pText->SetFont( monoFont );
	pText->Wrap(m_nLineWrap);
    gbSizer->Add( pText, wxGBPosition( 0, 0), wxGBSpan( nRowSpan, 2 ), wxEXPAND | wxALL, 5 );

    int nImgIndex = 0;
    for( const wxString& str: vexHT_Solved )
    {
        // add space in front of image
        wxStaticBitmap* pImg = new wxStaticBitmap( sbsSolved->GetStaticBox(),
                                                   wxID_ANY,
                                                   ctrlImgs->GetBitmap( nImgIndex ) );
        gbSizer->Add( pImg,
                      wxGBPosition( ( nImgIndex + nRowSpan ), 0),
                      wxDefaultSpan,
                      wxEXPAND | wxALL, 10 );

        wxStaticText* pLabel = new wxStaticText( sbsSolved->GetStaticBox(),
	                                             wxID_ANY,
	                                             str);
        pLabel->SetFont( monoFont );
		pLabel->Wrap(m_nLineWrap);
        gbSizer->Add( pLabel,
                      wxGBPosition( ( nImgIndex + nRowSpan ), 1),
                      wxDefaultSpan,
                      wxEXPAND | wxALL, 10 );
        ++nImgIndex;
    }
    gbSizer->AddGrowableCol( 1, 1 );
    sbsSolved->Add( gbSizer, wxSizerFlags( 1 ).Expand().Border( wxALL, 10 ) );

    pPanel->SetSizerAndFit( sbsSolved );
    mp_Book->AddPage( pPanel, vexHT_TAB_SOLVED, bSelected, sequence );
}

void DlgHowTo::MakeChangeDlg( const int& sequence, const bool bSelected )
{
    wxString vexHT_TAB_DLGCHG( " Change Board Dialog " );
    wxString vexHT_DLGCHG_LABEL( "  Change Board Dialog  " );
    wxString vexHT_DLG_Change(
        "Selecting \"Change Board\" brings up a dialog where the user can"
        " change details of the puzzle board to be played."
        " Left side of dialog has the controls the user can alter."
        " Right side of dialog is a sample presentation of the selected board.\n"
        "\n"
        "Board type will determine layout and shape of tile used."
        " Current shapes are Squares and Hexagons.\n"
    );
    wxString vexHT_DLG_Chg_Size = wxString::Format(
        "\n"
        "Board Dimensions can range from [%d x %d] to [%d x %d] tiles in size."
        " Game Board is not limited to a \"square matrix\"."
        " Any value of row/columns between %d and %d can be selected."
        " (e.g. [%d x %d] or [%d x %d] ).\n",
        BOARD_SIZE_MIN,
        BOARD_SIZE_MIN,
        BOARD_SIZE_MAX,
        BOARD_SIZE_MAX,
        BOARD_SIZE_MIN,
        BOARD_SIZE_MAX,
        BOARD_SIZE_MIN,
        BOARD_SIZE_MAX,
        ( BOARD_SIZE_MAX - 1 ),
        ( BOARD_SIZE_MIN + 1 )
    );
    vexHT_DLG_Change += vexHT_DLG_Chg_Size;
    vexHT_DLG_Change += wxT(
        "\n"
        "The numerical range of values that can appear on game tiles:\n"
        "  Octal - numbers from 0 to 7\n"
        "  Decimal - numbers from 0 to 9\n"
        "  Hexadecimal - numbers from 0 to 15**\n"
        "**values 10 to 15 denoted by letters A to F\n"
        "\n"
        "Pressing the \"OK\" button will close the dialog."
        " A new puzzle board with selected settings will be started.\n"
        "\n"
        "Clicking \"Cancel\" will close the dialog. No action will be taken.\n"
    );

    wxPanel* pPanel = new wxPanel( mp_Book );
    wxStaticBoxSizer* sbsChgDlg = new wxStaticBoxSizer( wxVERTICAL,
                                                        pPanel,
                                                        vexHT_DLGCHG_LABEL );
    wxStaticText* pText = new wxStaticText( sbsChgDlg->GetStaticBox(),
											wxID_ANY,
											vexHT_DLG_Change);
    pText->SetFont( monoFont );
	pText->Wrap(m_nLineWrap);
    sbsChgDlg->Add( pText, wxSizerFlags( 1 ).Expand().Border( wxALL, 10 ) );

    pPanel->SetSizerAndFit( sbsChgDlg );
    mp_Book->AddPage( pPanel, vexHT_TAB_DLGCHG, bSelected, sequence );
}

void DlgHowTo::MakeTips( const int& sequence, const bool bSelected )
{
    wxString vexHT_TAB_TIPS( " Tips " );
    wxString vexHT_TIPS_LABEL( "  Tips  " );
    wxString vexHT_TIPS_TEXT(
        "All games HAVE A SOLUTION!!!!\n"
        "\n"
        "Smaller board sizes are easier to solve than larger board sizes.\n"
        "\n"
        "Games using Octal values are harder to solve than when using"
        " Decimal values.\n"
        "\n"
        "Games using Hexadecimal values are easier to solve than when"
        " using Decimal values.\n"
        "\n"
        "Square Tile Boards can be harder to solve than Boards with"
        " Hexagon Tiles.\n"
        "\n"
        "Start solving a puzzle by finding \"anomalies\" in the tile values.\n"
        "  - tile value occurs only once - edge piece\n"
        "  - a pair of tiles with matching adjacent\n"
        "    value that occurs only once.\n"
        "\n"
        "You can use the Well Board as a \"staging area\" to test tile"
        " arrangements."
    );

    wxPanel* pPanel = new wxPanel( mp_Book );
    wxStaticBoxSizer* sbsTips = new wxStaticBoxSizer( wxVERTICAL,
                                                      pPanel,
                                                      vexHT_TIPS_LABEL );

    wxStaticText* pText = new wxStaticText( sbsTips->GetStaticBox(),
											wxID_ANY,
											vexHT_TIPS_TEXT);
    pText->SetFont( monoFont );
	pText->Wrap(m_nLineWrap);
    sbsTips->Add( pText, wxSizerFlags( 1 ).Expand().Border( wxALL, 10 ) );

    pPanel->SetSizerAndFit( sbsTips );
    mp_Book->AddPage( pPanel, vexHT_TAB_TIPS, bSelected, sequence );
}
