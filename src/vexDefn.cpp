/*==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
 ==============================================================================
    vexDefn.cpp
    definition of shape properties and routines
*/

#include "vexDefn.hpp"

const double SIN60 = std::sin( M_PI / 3 );

//------------ Time Format Strings
const wxString wxsTIMERDIALOGFMT    { wxT( "%02ld:%02ld:%02ld" ) };
const wxString wxsTIMERSTATUSFMT    { wxT( "Elapsed: %02ld:%02ld:%02ld" ) };

namespace vexTime
{
    void ElapsedCalculate( const long& elapsed,
                           long& hours,
                           long& minutes,
                           long& seconds )
    {
        hours = elapsed / 3600;
        if( hours < 0 ) hours = 0;
        minutes = (elapsed - ( hours * 3600 ) ) / 60;
        if( minutes < 0 ) minutes = 0;
        seconds = (elapsed - ( hours * 3600 ) - ( minutes * 60 ) ) % 60;
        if(seconds < 0 ) seconds = 0;
    }

    // Get string message of elapsed time to solve current puzzle
    // dlgmsg = true for dialog message
    // dlgmsg = false for status bar message
    wxString TimeMsg( long elapsedMS, const bool& dlgmsg )
    {
        long hours, minutes, seconds;
        ElapsedCalculate( elapsedMS, hours, minutes, seconds );
        wxString strRet;
        if( dlgmsg )
        {
            strRet = wxString::Format( wxsTIMERDIALOGFMT,
                                       hours,
                                       minutes,
                                       seconds );
        }
        else
        {
            strRet = wxString::Format( wxsTIMERSTATUSFMT,
                                       hours,
                                       minutes,
                                       seconds );
        }
        return strRet;
    }
}

namespace vexMath
{
    //TODO - not used - still need?
/*
    int PolarLength( const wxPoint& orig, const wxPoint& end )
    {
        wxRealPoint amalgamate = end - orig;
        double dRet = std::sqrt( ( amalgamate.x * amalgamate.x ) + ( amalgamate.y * amalgamate.y ) );
        return std::nearbyint( dRet );
    }

    //TODO - not used - still need?
    int PolarLength(const double& orig, const double& end)
    {
        double dRet = (int)std::sqrt( ( orig * orig ) + ( end * end ) );
        return std::nearbyint( dRet );
    }
*/
    wxPoint PolarDecompose(const double& length, const double& angle)
    {
        // NOTE: angle needs to be in radians!
        double phi = angle * M_PI / 180;
        int x = std::nearbyint( length * std::cos( phi ) );
        int y = std::nearbyint( length * std::sin( phi ) );
        return wxPoint( x, y );
    }

    wxPoint PolarShrink( const double& angle, const int& Shrinkage )
    {
        // shrinkage is one side of triangle
        // NOTE: screen coords != Math coords => compensate (rotate 90 deg)
        double workAngle = angle + 90;
        // angle in the range 0 - 90 deg
        double correctedAngle = std::fmod( workAngle, 90 );
        // we want the "other" interior angle
        if( correctedAngle == 60.0 ) correctedAngle = 30.0;
        // NOTE: angle needs to be in radians!
        double phi = correctedAngle * M_PI / 180;
        // find hypotenuse - shrinkage along line
        double shrunkLength = std::nearbyint( Shrinkage / std::cos(phi) );
        return PolarDecompose( shrunkLength, workAngle );
    }
};

namespace vexStrHelp
{
    wxString PtToString( const wxPoint& ptData )
    {
        wxString retStr = wxString::Format( wxT("pt( %dx%d )"),
                                            ptData.x,
                                            ptData.y );
        return retStr;
    }

    // Keep enabled for debugging purposes
    wxString SzToString( const wxSize& szData )
    {
        wxString retStr = wxString::Format( wxT("sz( %dx%d )"),
                                            szData.x,
                                            szData.y );
        return retStr;
    }
}

//==================== CmdData
CmdData::CmdData() :
    cmdID( 0 ),
    txtLabel( wxEmptyString ),
    txtMenu( wxEmptyString ),
    txtStatus( wxEmptyString ),
    bitmapLrg( wxNullBitmap ),
    bitmapSm( wxNullBitmap )
{}

CmdData::CmdData( const int& a,
                  const wxString& b,
                  const wxString& c,
                  const wxString& d ) :
    cmdID( a ),
    txtLabel( b ),
    txtMenu( c ),
    txtStatus( d ),
    bitmapLrg( wxNullBitmap ),
    bitmapSm( wxNullBitmap )
{}

CmdData::CmdData( const int& a,
                  const wxString& b,
                  const wxString& c,
                  const wxString& d,
                  const char** e,
                  const char** f ) :
    cmdID( a ),
    txtLabel( b ),
    txtMenu( c ),
    txtStatus( d ),
    bitmapLrg( wxBitmap( e ) ),
    bitmapSm( wxBitmap( f ) )
{}

CmdData::CmdData( const int& a,
                  const wxString& b,
                  const wxString& c,
                  const wxString& d,
                  const wxBitmap& e,
                  const wxBitmap& f ) :
    cmdID( a ),
    txtLabel( b ),
    txtMenu( c ),
    txtStatus( d ),
    bitmapLrg( e ),
    bitmapSm( f )
{}

//================= Tile Colours
// NOTE: Colour definition names defined in
// wxWidgets - [git root]/src/common/gcicmn.cpp

ColourPenBrush::ColourPenBrush() :
    mColourBrush( wxColour( "BLACK") ),
    mColourPen( wxColour( "WHITE") )
{}

ColourPenBrush::ColourPenBrush( const wxColour& brushcolour,
                                const wxColour& pencolour ) :
    mColourBrush( brushcolour ),
    mColourPen( pencolour )
{}