/*==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
 ==============================================================================
    vexApp.cpp
    application class implementation
*/

#include "vexApp.hpp"
#include "vexWindow.hpp"

#if defined(__UNIX__) && defined(_WXVER_3110_)
#include <wx/stdpaths.h>
#endif

IMPLEMENT_APP( vexApp );

bool vexApp::OnInit()
{
    // redirect logging to standard out
    wxLog* logger = new wxLogStream( &std::cout );
    wxLog::SetActiveTarget( logger );
    wxLogMessage( wxGetOsDescription() );

    // verify only a single instance running
    mp_checker = new wxSingleInstanceChecker( wxsAPPNAME );
    if( mp_checker->IsAnotherRunning() ) return false;
    if( !wxApp::OnInit() ) return false;

    SetVendorName( wxsVENDORNAME );
    SetAppName( wxsAPPNAME );

    wxInitAllImageHandlers();

// define where game configuration data is stored
// store in file ~/.config/(APPNAME)/(APPNAME)
#if defined(__UNIX__) && defined(_WXVER_3110_)
	//use XDG spec
	wxStandardPathsBase& localPath = wxStandardPaths::Get();
	localPath.SetFileLayout(wxStandardPaths::FileLayout_XDG);
    wxFileName configpath;
	configpath.AssignCwd( localPath.GetUserConfigDir() );
    configpath.AppendDir( wxsAPPNAME );
    if( !configpath.DirExists() ) configpath.Mkdir();
	configpath.SetFullName(wxsAPPNAME);
    mp_Config = new wxFileConfig(wxEmptyString, 
								 wxEmptyString,
								 configpath.GetFullPath(),
								 wxEmptyString,
								 wxCONFIG_USE_LOCAL_FILE);
#elif defined(__UNIX__) && !defined(_WXVER_3110_)
    // manual method
    wxFileName configpath;
	configpath.AssignCwd( wxGetHomeDir() );
	configpath.AppendDir( ".config" );
	if( !configpath.DirExists() ) configpath.Mkdir();
    configpath.AppendDir( wxsAPPNAME );
    if( !configpath.DirExists() ) configpath.Mkdir();
	configpath.SetFullName(wxsAPPNAME);
    mp_Config = new wxFileConfig(wxEmptyString, 
								 wxEmptyString,
								 configpath.GetFullPath(),
								 wxEmptyString,
								 wxCONFIG_USE_LOCAL_FILE);
#else
    // if platform is Windows or Mac - use default config reference
    // wxMac -
    // wxMSW - C:\Users\<username>\AppData\local\vexagon.ini
    mp_Config = new wxFileConfig( wxsAPPNAME, wxsAPPNAME, wxEmptyString, wxEmptyString, wxCONFIG_USE_LOCAL_FILE );
#endif
	std::cout << "File Config: " << configpath.GetFullPath() << std::endl;
    mp_gameWindow = new vexWindow( wxsAPPNAME );
    mp_gameWindow->Show( true );
    return true;
}

int vexApp::OnExit()
{
    if( nullptr != mp_Config ) delete mp_Config;
    if( nullptr != mp_checker ) delete mp_checker;
    return 0;
}
