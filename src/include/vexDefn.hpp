/*==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
 ==============================================================================
    vexDefn.hpp
    declaration of shape properties and routines
*/

#pragma once

// reported issue with M_PI
// see http://stackoverflow.com/a/6563891
#define _USE_MATH_DEFINES

// wxWidgets standard header guards
#ifdef __BORLANDC__
    #pragma hdrstop
#endif
#ifdef WX_PRECOMP
    #include <wx/wxprec.h>
#else
    #include <wx/wx.h>
#endif

// c++ includes
#include <iostream>
#include <cstdlib>
#include <vector>
#include <map>
#include <iterator>
#include <utility>
#include <algorithm>
#include <iomanip>
#include <cmath>
#include <random>       //C++11 random number generator

// wxWidgets includes
#include <wx/app.h>
#include <wx/snglinst.h>
#include <wx/graphics.h>
#include <wx/geometry.h>
#include <wx/region.h>
#include <wx/dcbuffer.h>
#include <wx/artprov.h>
#include <wx/panel.h>
#include <wx/gbsizer.h>
#include <wx/aboutdlg.h>
#include <wx/toolbook.h>
#include <wx/fileconf.h>
#include <wx/spinctrl.h>

// project includes
#include "vexRevision.h"

wxDECLARE_EVENT( vexFINISHEDEVENT, wxCommandEvent );

//------------ Application and Help
const wxString wxsVENDORNAME        { wxT( "FurCaT" ) };
// config folder name also. Created at install
const wxString wxsAPPNAME           { wxT( "vexagon" ) };
const wxString wxsAPPDESC           { wxT( "wxWidgets based tile matching puzzle game" ) };
const wxString wxsAPPCOPY           { wxT( "(C)2015 - 2019" ) };
const wxString wxsAPPDEV            { wxT( "Scott Furry" ) };
const wxString wxsAPPVER            { ID_VEXAGON_REVISION };    //wxT( "1.0.0" )
const wxString wxsAPPURL            { wxT( "http://vexagon.furcat.ca" ) };
const wxString wxsAPPEMAIL          { wxT( "vexagon@furcat.ca" ) };

enum CtrlIDs: int
{
    ID_GAMESEPARTOR = wxID_SEPARATOR,
    ID_GAMENEW      = wxID_HIGHEST + 1,
    ID_GAMENEWCHG,
    ID_GAMERESTART,
    ID_GAMEPAUSE,
    ID_GAMERESUME,
    ID_GAMESOLVE,
    ID_GAMEMOVEUP,
	ID_GAMEMOVEUPRT,
    ID_GAMEMOVERIGHT,
	ID_GAMEMOVEDNRT,
    ID_GAMEMOVEDOWN,
	ID_GAMEMOVEDNLT,
    ID_GAMEMOVELEFT,
	ID_GAMEMOVEUPLT,
    ID_GAMEPANEL,
    ID_GAMEHOWTO,
    ID_GAMETIMER
};

extern const double SIN60;
const short NO_VALUE = -1;

#if defined ( __WXOSX__ ) || defined ( __WINDOWS__ )
// Mac / MSW use 72 DPI
const double SCREEN_SCALE_FACTOR = 1.35;
#else
// linux defaults to 96 DPI
// application default
const double SCREEN_SCALE_FACTOR = 1.00;
#endif

enum LOCATION : short
{
    LOC_NONE = -1,
    LOC_WELL = 0,
    LOC_GAME,
    LOC_MAX = LOC_GAME
};

typedef unsigned long int MOVE_FLAG_TYPE;
enum MOVE_FLAGS : MOVE_FLAG_TYPE
{
    MOVE_NONE = 0x0,
    MOVE_N    = 0x0001,
	MOVE_NE	  = 0x0002,
    MOVE_E    = 0x0004,
	MOVE_SE	  = 0x0008,
    MOVE_S    = 0x0010,
	MOVE_SW	  = 0x0020,
    MOVE_W    = 0x0040,
	MOVE_NW   = 0x0080
};

typedef unsigned short                  TileSideCount;
typedef std::vector<short>              TileValues;
typedef std::vector<int>                LineAngles;
typedef std::vector<wxPoint>            ShapePoints;
typedef std::vector<wxPoint>            ArrPoints;
typedef std::pair< wxPoint, short >     CompareElement;
typedef std::vector< CompareElement >   CompareArr;

namespace vexTime
{
    void    ElapsedCalculate( const long& elapsed,
                              long& hours,
                              long& minutes,
                              long& seconds );
    wxString TimeMsg( long elapsedMS, const bool& dlgmsg = false );
}

namespace vexMath
{
    //TODO - not used - still need?
//    int PolarLength(const wxPoint& orig, const wxPoint& end);
//    int PolarLength(const double& orig, const double& end);
    wxPoint PolarDecompose(const double& length, const double& angle);
    wxPoint PolarShrink(const double& angle, const int& Shrinkage);
}

namespace vexStrHelp
{
    wxString PtToString( const wxPoint& ptData );
    // Keep enabled for debugging purposes
    wxString SzToString( const wxSize& szData );
}

//==================== CmdData
class CmdData
{
public:
    CmdData();
    CmdData(const int& a,
            const wxString& b,
            const wxString& c,
            const wxString& d);
    CmdData(const int& a,
            const wxString& b,
            const wxString& c,
            const wxString& d,
            const char** e,
            const char** f);
    CmdData(const int& a,
            const wxString& b,
            const wxString& c,
            const wxString& d,
            const wxBitmap& e,
            const wxBitmap& f);
    int         cmdID;
    wxString txtLabel;
    wxString txtMenu;
    wxString txtStatus;
    wxBitmap bitmapLrg;
    wxBitmap bitmapSm;
};

typedef std::vector<CmdData> VCmdData;

//================= Tile Colours
//    declaration tile side colours class
//    - possible future use to allow user-defined colours
typedef std::vector<wxColour> ShapeColours;

class ColourPenBrush
{
public:
    ColourPenBrush();
    ColourPenBrush( const wxColour& brushcolour,
                    const wxColour& pencolour );

    ColourPenBrush( const ColourPenBrush& rhs ) = default;
    ColourPenBrush( ColourPenBrush&& rhs ) = default;

    ColourPenBrush& operator=( const ColourPenBrush& rhs ) = default;
    ColourPenBrush& operator=( ColourPenBrush&& rhs ) = default;

    wxColour    mColourBrush;
    wxColour    mColourPen;
};

typedef std::map<short, ColourPenBrush> ColourMap;