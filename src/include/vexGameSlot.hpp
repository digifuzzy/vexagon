/*==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
 ==============================================================================
    vexGameSlot.hpp
    declaration of Game Slot class
    Game Slot can contain a Game Tile
*/

#pragma once

#include "vexDefn.hpp"
#include "vexTileProperty.hpp"
#include "vexTileData.hpp"

const int MINIMUM_SHADE_SIZE = 4;
const int MINIMUM_FONT_SIZE = 6;

//==================== Board Slot
class BoardSlot
{
public:
    BoardSlot();
    BoardSlot( const TileProperty& prop );
    BoardSlot( const BoardSlot& rhs ) = default;
    BoardSlot( BoardSlot&& rhs ) =default;
    ~BoardSlot();

    BoardSlot& operator=( const BoardSlot& rhs ) = default;
    BoardSlot& operator=( BoardSlot&& rhs ) = default;

    void        SlotPaint( wxGraphicsContext* gc );
    void        CursorPaint( wxGraphicsContext* gc );
    void        SlotResize( const int& drawSize,
                            const wxPoint& origin );

    bool        hasTile();
    void        swapData( BoardSlot& rhs );
    void        swapData( TileData& data );
    TileData    getData() const;
    void        setData( const TileValues& inValues,
                         const wxPoint& ptStart,
                         const wxPoint& ptSolve );

    short       getSideValue( const short& side ) const;
    short       getMapStart() const;
    short       getMapSolve() const;
    bool        containsPt( const wxPoint& ptSearch );
    void        setPause( const bool& bPause = false );
    wxSize      GetSlotSize();
private:
    wxPoint     GetTileLogicalOrigin() const;

    void        PaintEmpty( wxGraphicsContext* gc );
    void        PaintPaused( wxGraphicsContext* gc );
    void        PaintSlotFull( wxGraphicsContext* gc );
    void        PaintTileSide( wxGraphicsContext* gc,
                               const wxPoint& ptA,
                               const wxPoint& ptB,
                               const wxPoint& ptC,
                               const short& sideValue,
                               const ColourMap& cmColours );

private:
    bool            m_bPause;
    int             m_nTileFontSz;
    int             m_nShadeSz;
    TileProperty    m_PropertySlot;
    // global coordinates of tile center;
    wxPoint         m_PtCenter;
    // global wxPoints vector defining the outside tile shape
    ShapePoints     m_polyPerimeter;
    // global wxPoints vector defining the tile shape (perimeter less shading)
    ShapePoints     m_polyInterior;
    // Region defining shape - used for mouse click detection
    wxRegion        m_regionPerimieter;
    //
    // Movable tile data
    TileData        m_Data;
};

typedef std::map< short, BoardSlot > BoardSlots;
