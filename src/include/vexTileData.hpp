/*==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
 ==============================================================================
    vexTileData.hpp
    Movable tile data contained in Game Slot
*/

#pragma once

#include "vexDefn.hpp"
#include "vexTileProperty.hpp"

//==================== Txfr Data
class TileData
{
public:
    TileData();
    TileData( const TileValues& inValues,
              const short& mapstart,
              const short& mapsolve);
    TileData( const TileData& rhs ) = default;
    TileData( TileData&& rhs ) = default;
    ~TileData();
    TileData& operator=( const TileData& rhs ) = default;
    TileData& operator=( TileData&& rhs ) = default;

    bool    hasValue();
    size_t  getDataSize();
    short   getSideValue( const short& side ) const;
    short   getStart() const { return m_MapStart; }
    short   getSolve() const { return m_MapSolve; }
    TileValues getValues() { return m_Values; }

private:
    // tile location in Well board (randomized start position)
    short           m_MapStart;
    // tile location in Game board (solved position)
    short           m_MapSolve;
    // vector of values - clockwise from shape(screen) top
    TileValues      m_Values;
};
