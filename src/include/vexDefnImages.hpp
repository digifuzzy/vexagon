/*==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
 ==============================================================================
    vexDefnImages.hpp
    declaration of image variables and image copy routines
*/

#pragma once
#include "vexDefn.hpp"

extern const wxSize DEFAULT_BITMAP_SIZE;

// - using png-based images
const wxBitmapType VEX_BM_TYPE = wxBITMAP_TYPE_PNG;

// see [vexagon]/graphics/README.IMAGES for details on how images are generated
// from source SVG's
// game icon
#include "images/vexagon_img.hpp"
// toolbar images
#include "images/Game_Change_32.hpp"
#include "images/Game_New_32.hpp"
#include "images/Game_Pause_32.hpp"
#include "images/Game_Reset_32.hpp"
#include "images/Game_Resume_32.hpp"
#include "images/Game_Solve_32.hpp"
#include "images/Game_Move_Up_32.hpp"
#include "images/Game_Move_Up_Rt_32.hpp"
#include "images/Game_Move_Right_32.hpp"
#include "images/Game_Move_Dn_Rt_32.hpp"
#include "images/Game_Move_Down_32.hpp"
#include "images/Game_Move_Dn_Lt_32.hpp"
#include "images/Game_Move_Left_32.hpp"
#include "images/Game_Move_Up_Lt_32.hpp"

// menu images
#include "images/Game_Change_16.hpp"
#include "images/Game_New_16.hpp"
#include "images/Game_Pause_16.hpp"
#include "images/Game_Reset_16.hpp"
#include "images/Game_Resume_16.hpp"
#include "images/Game_Solve_16.hpp"
#include "images/Game_Move_Up_16.hpp"
#include "images/Game_Move_Up_Rt_16.hpp"
#include "images/Game_Move_Right_16.hpp"
#include "images/Game_Move_Dn_Rt_16.hpp"
#include "images/Game_Move_Down_16.hpp"
#include "images/Game_Move_Dn_Lt_16.hpp"
#include "images/Game_Move_Left_16.hpp"
#include "images/Game_Move_Up_Lt_16.hpp"

// How To images
#include "images/Game_Ctrls.hpp"
#include "images/Game_Tips.hpp"
#include "images/Action_Cancel.hpp"

enum vexIMAGE_INDEX {
    vexIMG_IDX_CHANGE = 0,
    vexIMG_IDX_CHANGE_SM,
    vexIMG_IDX_NEW,
    vexIMG_IDX_NEW_SM,
    vexIMG_IDX_PAUSE,
    vexIMG_IDX_PAUSE_SM,
    vexIMG_IDX_RESET,
    vexIMG_IDX_RESET_SM,
    vexIMG_IDX_RESUME,
    vexIMG_IDX_RESUME_SM,
    vexIMG_IDX_SOLVE,
    vexIMG_IDX_SOLVE_SM,
    vexIMG_IDX_MV_UP,
    vexIMG_IDX_MV_UP_SM,
    vexIMG_IDX_MV_UPRT,
    vexIMG_IDX_MV_UPRT_SM,
	vexIMG_IDX_MV_RT,
    vexIMG_IDX_MV_RT_SM,
    vexIMG_IDX_MV_DNRT,
    vexIMG_IDX_MV_DNRT_SM,
    vexIMG_IDX_MV_DN,
    vexIMG_IDX_MV_DN_SM,
    vexIMG_IDX_MV_DNLT,
    vexIMG_IDX_MV_DNLT_SM,
    vexIMG_IDX_MV_LT,
    vexIMG_IDX_MV_LT_SM,
    vexIMG_IDX_MV_UPLT,
    vexIMG_IDX_MV_UPLT_SM,
    vexIMG_IDX_ICON,
    vexIMG_IDX_CTRLS,
    vexIMG_IDX_TIPS,
    vexIMG_IDX_CANCEL
};
namespace vexImage
{
    typedef struct vexImage_Item
    {
        const unsigned char*    IMGdata;
        unsigned int            IMGsize;
    } vexImage_Item;

    #define VEXIMAGE( IMAGENAME ) { IMAGENAME, sizeof( IMAGENAME ) }
    wxBitmap vexBitmap( vexIMAGE_INDEX imageindex );
    wxIcon vexIcon( vexIMAGE_INDEX imageindex );
}
