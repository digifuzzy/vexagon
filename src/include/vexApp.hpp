/*==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
 ==============================================================================
    vexApp.cpp
    application class declaration
*/

#pragma once

#include "vexDefn.hpp"

class vexWindow;
class vexApp : public wxApp
{
public:
    vexApp() {}

    virtual bool    OnInit();
    virtual int     OnExit();
    wxFileConfig*   GetAppConfig() { return mp_Config; }

private:
    wxSingleInstanceChecker* mp_checker;
    wxFileConfig*            mp_Config;
    vexWindow*               mp_gameWindow;
};

DECLARE_APP( vexApp );
