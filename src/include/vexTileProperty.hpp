/*==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
 ==============================================================================
    vexTileProperty.hpp
    declaration of common shape properties and routines
*/

#pragma once

#include "vexDefn.hpp"

const unsigned short DEFAULT_SIDES_SQUARE = 4;
const unsigned short DEFAULT_SIDES_HEX    = 6;

const int BOARD_SIZE_MIN = 2;
const int BOARD_SIZE_MAX = 7;
extern const wxPoint DEFAULT_BOARD_SIZE;

/*  square ====     HEX_EVEN===   HEX_ODD===
           |  |         ===/col\        /col\===
           ====        /col\ 2 /        \ 1 /col\
                       \ 1 /===          ===\ 2 /
                        ===                  ===
    all coordinates are referenced to global Top|Left (0,0)
*/
enum GameBoardType : unsigned short
{
    GBT_SQUARE,
    GBT_MINIMUM = GBT_SQUARE,
    GBT_HEX_ODD,
    GBT_HEX_EVEN,
    GBT_MAXIMUM = GBT_HEX_EVEN
};

enum GameBoardBase : unsigned short
{
    GBB_OCT = 8,
    GBB_DEC = 10,
    GBB_HEX = 16,
};

// Board Types
extern const wxString WXSTYPE_SQR;
extern const wxString WXSTYPE_HEXODD;
extern const wxString WXSTYPE_HEXEVEN;

// Numerical Bases for side values
extern const wxString WXSBASE_OCT;
extern const wxString WXSBASE_DEC;
extern const wxString WXSBASE_HEX;

// When defining/building board - values used when populating
// memory overlay array with random numbers.
// Identity of array elements used to populate tile values
extern const wxPoint ValueIncrementColumn;
extern const wxPoint ValueIncrementHexOffset;
extern const wxPoint ValueIncrementRow;

// During play - identify surrounding tiles and sides
// used to ensure a tile can be placed at chosen point
// Use pair <wxPoint, side #>
// wxPoint is a relative offset to selected tile point
extern const CompareArr ArrCompareSqr;
extern const CompareArr ArrCompareHexPEAK;
extern const CompareArr ArrCompareHexTROUGH;

//==================== TileProperty
class TileProperty
{
public:
    TileProperty();
    TileProperty(const GameBoardType& type,
                 const GameBoardBase& base,
                 const wxPoint& dimensions );
    TileProperty( const TileProperty& rhs ) = default;
    TileProperty( TileProperty&& rhs ) = default;
    ~TileProperty();

    TileProperty& operator =( const TileProperty& rhs ) = default;
    TileProperty& operator =( TileProperty&& rhs ) = default;

    void SetType( const GameBoardType& type );
    void SetType( const unsigned int& type );
    void SetType( const wxString& strtype );

    void SetBase( const GameBoardBase& base );
    void SetBase( const unsigned int& base );
    void SetBase( const wxString& strbase );

    void SetDimensions( const wxPoint& dimensions );
    void SetDimensions( const int& width, const int& height );
    bool IsValidBoardPt( const wxPoint& ptTest );

    wxPoint MapToPt( const short& mapid );
    short   PtToMap( const wxPoint& loc );
    short   PtToMap( const wxPoint& loc, const short& col_count );

    inline GameBoardType GetType() const        { return m_tileType; }
    wxString      GetTypeAsString() const;

    inline GameBoardBase GetBase() const        { return m_tileBase; }
    wxString      GetBaseAsString() const;

    inline wxPoint       GetDimensions() const  { return m_boardDimensions; }
    inline TileSideCount GetSideCount() const   { return m_tileNumSides; }
    LineAngles    GetInsideAngles() const;
    ColourMap     GetTileColourMap() const;

    CompareArr    GetCompareArr( const bool& OddColumnNumber = false ) const;

    ShapePoints   PopulatePoints( const int& shapesize );

    ShapeColours  PopulateShading();
    ArrPoints     PopulateValuePts();
    wxPoint       UpdateOffest( const short& currentCol );

    wxRealPoint   PointCalculate();

    wxPoint       OffsetCalculate( const unsigned int& length );
    wxPoint       OffsetStart( const unsigned int& length,
                               const wxPoint& origin,
                               wxPoint& offset );
    wxPoint       OffsetNextInColumn( const unsigned int& length );
    wxPoint       OffsetNextInRow( const unsigned int& length,
                                   const wxPoint& origin,
                                   const wxPoint& offset,
                                   const size_t& column,
                                   const wxPoint& currentLoc );

    int           GapAndPad( const int& shadewidth );

    wxString      toString() const;
    wxString      toStringDisplay() const;

    bool          CanMoveLeftRight();

    wxSize        GetSlotSize( const ShapePoints& ptsPerimeter );
    wxPoint       GetSlotCenter( const ShapePoints& ptsPerimeter );

    static wxString      TypeToString( const GameBoardType& type );
    static GameBoardType StringToType( const wxString& strtype );
    static wxString      BaseToString( const GameBoardBase& base );
    static GameBoardBase StringToBase( const wxString& strbase );
    static double        SizeHexHeight( const double& length );

//FUTURE - restore diamond shape
//    static double SizeDiamondWidth( const double& length );

public:
    // cannot use "brace initialization" for wxArrayString
    // init an array to populate wxArrayString seems redundant
    static const wxString  wxsaBoardTypes[];
    static const int       nBoardTypesCount;
    static const wxString  wxsaBoardBases[];
    static const int       nBoardBasesCount;

private:
    static const ColourMap DefaultColourMap;

    GameBoardType   m_tileType;
    GameBoardBase   m_tileBase;
    wxPoint         m_boardDimensions;
    TileSideCount   m_tileNumSides;
};
