/*
==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
==============================================================================
*/
#pragma once
static const unsigned char Game_Move_Right_32[] = {
  0x89, 0x50, 0x4e, 0x47, 0x0d, 0x0a, 0x1a, 0x0a,
  0x00, 0x00, 0x00, 0x0d, 0x49, 0x48, 0x44, 0x52,
  0x00, 0x00, 0x00, 0x20, 0x00, 0x00, 0x00, 0x20,
  0x08, 0x06, 0x00, 0x00, 0x00, 0x73, 0x7a, 0x7a,
  0xf4, 0x00, 0x00, 0x00, 0x04, 0x67, 0x41, 0x4d,
  0x41, 0x00, 0x00, 0xb1, 0x8f, 0x0b, 0xfc, 0x61,
  0x05, 0x00, 0x00, 0x00, 0x20, 0x63, 0x48, 0x52,
  0x4d, 0x00, 0x00, 0x7a, 0x26, 0x00, 0x00, 0x80,
  0x84, 0x00, 0x00, 0xfa, 0x00, 0x00, 0x00, 0x80,
  0xe8, 0x00, 0x00, 0x75, 0x30, 0x00, 0x00, 0xea,
  0x60, 0x00, 0x00, 0x3a, 0x98, 0x00, 0x00, 0x17,
  0x70, 0x9c, 0xba, 0x51, 0x3c, 0x00, 0x00, 0x00,
  0x06, 0x62, 0x4b, 0x47, 0x44, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0xf9, 0x43, 0xbb, 0x7f, 0x00,
  0x00, 0x00, 0x09, 0x70, 0x48, 0x59, 0x73, 0x00,
  0x00, 0x0e, 0xc4, 0x00, 0x00, 0x0e, 0xc4, 0x01,
  0x95, 0x2b, 0x0e, 0x1b, 0x00, 0x00, 0x00, 0x07,
  0x74, 0x49, 0x4d, 0x45, 0x07, 0xe1, 0x07, 0x1a,
  0x0e, 0x21, 0x18, 0x09, 0x25, 0xa9, 0xd3, 0x00,
  0x00, 0x00, 0x10, 0x63, 0x61, 0x4e, 0x76, 0x00,
  0x00, 0x00, 0xa0, 0x00, 0x00, 0x00, 0x80, 0x00,
  0x00, 0x00, 0x40, 0x00, 0x00, 0x00, 0x40, 0x08,
  0x58, 0x50, 0xd8, 0x00, 0x00, 0x05, 0xbd, 0x49,
  0x44, 0x41, 0x54, 0x58, 0xc3, 0xb5, 0x96, 0x5f,
  0x6c, 0x55, 0x59, 0x15, 0xc6, 0x7f, 0x6b, 0xed,
  0x7d, 0xce, 0x3e, 0xe5, 0x5e, 0xda, 0xdb, 0x96,
  0xa1, 0x74, 0x2a, 0x70, 0xcb, 0x38, 0x61, 0xe2,
  0x04, 0xa4, 0x46, 0xd4, 0x54, 0x33, 0xc9, 0x3c,
  0x68, 0xe6, 0xc1, 0x51, 0x9b, 0x0c, 0xe8, 0x18,
  0x94, 0x17, 0x43, 0xe6, 0x45, 0xcd, 0x30, 0x4e,
  0x74, 0x1e, 0x14, 0x75, 0xfa, 0x00, 0x63, 0xd0,
  0x79, 0x90, 0xa4, 0x69, 0x88, 0x19, 0x22, 0x89,
  0x4a, 0x48, 0x26, 0x21, 0x3a, 0xff, 0x30, 0xc6,
  0xf9, 0xc3, 0x4c, 0xf4, 0x85, 0x4e, 0x22, 0x69,
  0x43, 0x30, 0x10, 0x05, 0x0c, 0x97, 0x8a, 0x2d,
  0xa4, 0xd2, 0x5e, 0xee, 0xd9, 0xcb, 0x87, 0xf6,
  0xde, 0xb6, 0x43, 0xff, 0x81, 0xc3, 0xba, 0xd9,
  0x39, 0xe7, 0xdc, 0xbd, 0xee, 0xf7, 0x7d, 0xfb,
  0xdb, 0x6b, 0xed, 0x7b, 0x84, 0x3b, 0x88, 0xfe,
  0xfe, 0x7e, 0x6a, 0xb5, 0x1a, 0x66, 0x56, 0x10,
  0x91, 0x60, 0x66, 0xd7, 0x9c, 0x73, 0x98, 0x19,
  0xfb, 0xf6, 0xed, 0xbb, 0x13, 0xa8, 0x46, 0xe8,
  0x9d, 0x24, 0x9b, 0x19, 0x66, 0x86, 0xaa, 0xee,
  0x06, 0x8e, 0x8b, 0xc8, 0xe7, 0x54, 0x15, 0x55,
  0xa5, 0xbf, 0xbf, 0xff, 0xde, 0x0b, 0xf0, 0xde,
  0xe3, 0xbd, 0x27, 0x4d, 0xd3, 0x8f, 0x26, 0x49,
  0xf2, 0xa8, 0xf7, 0xfe, 0xb0, 0x99, 0x7d, 0xf1,
  0xe5, 0xe3, 0x2f, 0xa3, 0xaa, 0xec, 0xdf, 0xbf,
  0xff, 0xde, 0x0a, 0xc8, 0xb2, 0x8c, 0xa6, 0xa6,
  0x26, 0xb2, 0xa6, 0x2c, 0x3a, 0x73, 0x54, 0xc7,
  0xaa, 0x9b, 0x93, 0x24, 0x19, 0xd8, 0xf1, 0xe4,
  0x8e, 0xdd, 0xb7, 0x6e, 0xdd, 0xf2, 0xce, 0x39,
  0x0e, 0x1e, 0x3c, 0x78, 0x6f, 0x05, 0x24, 0x49,
  0x42, 0x53, 0x53, 0x13, 0x52, 0x13, 0xce, 0xfd,
  0xf1, 0x1c, 0x63, 0x17, 0xc6, 0xba, 0x12, 0x9f,
  0xfc, 0xa2, 0x50, 0x2c, 0x7c, 0xc7, 0xcc, 0x32,
  0x55, 0xe5, 0xd0, 0xa1, 0x43, 0x1c, 0x38, 0x70,
  0xe0, 0xc3, 0x17, 0x10, 0x42, 0xa0, 0x58, 0x2c,
  0x92, 0x65, 0x19, 0x21, 0x04, 0x26, 0x46, 0x27,
  0x18, 0xfe, 0xc3, 0x30, 0x57, 0x87, 0xaf, 0xb6,
  0x7a, 0xe7, 0x9f, 0x0f, 0x59, 0xf8, 0x11, 0xd0,
  0x0c, 0x50, 0x2e, 0x97, 0x19, 0x18, 0x18, 0x58,
  0x16, 0xd3, 0x9f, 0x3e, 0x7d, 0x1a, 0x11, 0x69,
  0x7c, 0x11, 0x63, 0x24, 0xc6, 0x48, 0xad, 0x56,
  0x23, 0xc6, 0x48, 0x9e, 0xe7, 0xe4, 0x79, 0x4e,
  0x8c, 0x91, 0x6a, 0xb5, 0xca, 0xe4, 0xe4, 0x24,
  0x93, 0x53, 0x93, 0xa4, 0x21, 0x45, 0x54, 0xb8,
  0x39, 0x76, 0x93, 0xe1, 0x57, 0x87, 0xc9, 0xa7,
  0xf2, 0x55, 0xe5, 0x4f, 0x95, 0xbf, 0x97, 0x84,
  0xa4, 0xdd, 0xcc, 0x7e, 0x38, 0x36, 0x36, 0x56,
  0x31, 0x33, 0x06, 0x07, 0x07, 0xd9, 0xb3, 0x67,
  0xcf, 0xe2, 0x0e, 0x98, 0x99, 0x00, 0x25, 0xe0,
  0xfe, 0xfa, 0x30, 0xb3, 0x75, 0x66, 0xd6, 0x11,
  0x63, 0xec, 0x30, 0xb3, 0x8e, 0xfa, 0xbd, 0x88,
  0x74, 0x84, 0x10, 0x3a, 0xb2, 0x2c, 0x2b, 0x84,
  0x34, 0x20, 0x08, 0x08, 0x4c, 0x4d, 0x4c, 0x31,
  0xf2, 0xc6, 0x08, 0xe7, 0xde, 0x3c, 0x97, 0x90,
  0xf3, 0xad, 0x34, 0xa4, 0xbf, 0x04, 0xca, 0x59,
  0x96, 0xe1, 0xbd, 0xe7, 0xc8, 0x91, 0x23, 0x8b,
  0x3b, 0xa0, 0xaa, 0x6d, 0xc0, 0xa0, 0xa8, 0xf4,
  0xa8, 0xaa, 0xa9, 0x29, 0xce, 0xbb, 0x9a, 0xf3,
  0x2e, 0x9f, 0xeb, 0xc0, 0xdc, 0xe1, 0x13, 0xbf,
  0xf6, 0x7a, 0x72, 0xbd, 0x01, 0x22, 0x22, 0xd4,
  0xa6, 0x6a, 0x9c, 0xfd, 0xf3, 0x59, 0xf2, 0xa9,
  0x5c, 0x1f, 0x7e, 0xec, 0xe1, 0x1d, 0x59, 0x31,
  0x2b, 0xc5, 0x18, 0xf7, 0x26, 0x49, 0xf2, 0x37,
  0x33, 0xe3, 0xe8, 0xd1, 0xa3, 0xec, 0xda, 0xb5,
  0xeb, 0x36, 0x01, 0x32, 0x34, 0x34, 0xb4, 0x51,
  0x54, 0x4e, 0x9d, 0x7f, 0xff, 0x7c, 0xd7, 0xf9,
  0xf7, 0xcf, 0x23, 0x2a, 0xa8, 0x2a, 0xe2, 0x04,
  0x8b, 0x46, 0xb4, 0x38, 0xff, 0x1a, 0x23, 0x66,
  0x46, 0xe5, 0x42, 0x85, 0x91, 0x77, 0x46, 0xc8,
  0x6b, 0xf9, 0x2c, 0x9a, 0x81, 0xa8, 0xb0, 0xf1,
  0x13, 0x1b, 0xe9, 0xf9, 0x72, 0x0f, 0xc5, 0xf6,
  0xe2, 0x5f, 0xa3, 0xc5, 0xbd, 0x85, 0x55, 0x85,
  0x53, 0xd5, 0x6a, 0x15, 0x55, 0x65, 0xe7, 0xce,
  0x9d, 0xb7, 0x0b, 0x70, 0xde, 0xbd, 0xfd, 0xfa,
  0xe1, 0xd7, 0xd7, 0xbf, 0x36, 0xf8, 0x1a, 0xce,
  0x3b, 0x10, 0xe6, 0xd5, 0xc5, 0x42, 0x61, 0x66,
  0x4b, 0xe6, 0x74, 0x7d, 0xac, 0x8b, 0xed, 0x4f,
  0x6c, 0xa7, 0xf5, 0x23, 0xad, 0x23, 0x16, 0xed,
  0xd9, 0x8b, 0xff, 0xb8, 0xf8, 0xfb, 0xee, 0x07,
  0xba, 0x51, 0x55, 0xfa, 0xfa, 0xfa, 0x16, 0xe8,
  0x02, 0x99, 0x56, 0x2f, 0x2a, 0xcb, 0x92, 0xd7,
  0x6d, 0x5f, 0x2a, 0x2e, 0x9e, 0xb9, 0xc8, 0xa9,
  0x5f, 0x9f, 0xe2, 0xda, 0x85, 0x6b, 0x0f, 0xa5,
  0x21, 0x1d, 0xe8, 0x7e, 0xa0, 0xfb, 0x1b, 0x31,
  0x8f, 0x4e, 0x44, 0x38, 0x71, 0xe2, 0xc4, 0x02,
  0x02, 0x3e, 0xe4, 0x10, 0x11, 0x2a, 0x7f, 0xaf,
  0xf0, 0xd6, 0x4b, 0x6f, 0x71, 0xf9, 0xcc, 0xe5,
  0xae, 0x24, 0x49, 0x5e, 0x0c, 0x59, 0xf8, 0x76,
  0x8c, 0x31, 0x73, 0xce, 0x71, 0xf2, 0xe4, 0x49,
  0x8e, 0x1d, 0x3b, 0x86, 0x9f, 0x35, 0x40, 0x50,
  0x51, 0x54, 0xe6, 0x6b, 0x12, 0x16, 0x5f, 0xe9,
  0xb2, 0x73, 0x0e, 0x6e, 0x5c, 0xba, 0xc1, 0xbb,
  0x2f, 0xbd, 0x8b, 0x3d, 0x69, 0x6d, 0x9b, 0x3e,
  0xb3, 0xa9, 0xdf, 0x27, 0x7e, 0x8d, 0x45, 0x7b,
  0xc1, 0xcc, 0xae, 0xaf, 0x5b, 0xb7, 0x6e, 0x56,
  0x40, 0x2a, 0x29, 0x45, 0x2d, 0xa2, 0xaa, 0x0b,
  0x82, 0xcf, 0x7d, 0x9e, 0x37, 0x27, 0x4b, 0xcc,
  0xc1, 0x74, 0x31, 0xff, 0xc7, 0x18, 0x3a, 0x3a,
  0x84, 0x4c, 0x4a, 0xe1, 0xc1, 0x47, 0x1f, 0x7c,
  0x56, 0x83, 0xb6, 0xc5, 0x3c, 0xee, 0xab, 0x56,
  0xab, 0x57, 0x3d, 0x80, 0x61, 0x04, 0x0d, 0x34,
  0xbb, 0x66, 0xd4, 0x69, 0x03, 0x44, 0x10, 0x4c,
  0x0c, 0x99, 0xf9, 0x98, 0x59, 0xa3, 0xf7, 0x05,
  0xc1, 0xb0, 0x79, 0xb9, 0x73, 0x45, 0x34, 0xe6,
  0xea, 0xb9, 0x13, 0xc6, 0xd9, 0xdf, 0x9d, 0xc5,
  0x4d, 0xba, 0x74, 0xf3, 0x97, 0x36, 0x3f, 0x95,
  0x84, 0x24, 0x13, 0x91, 0xef, 0x36, 0x1c, 0x08,
  0x12, 0x68, 0xf1, 0x2d, 0xa8, 0xd3, 0xf9, 0x04,
  0x36, 0x87, 0x40, 0x16, 0x20, 0x58, 0xe0, 0x79,
  0xae, 0x2b, 0x62, 0xd3, 0x8b, 0x40, 0xc0, 0x89,
  0x23, 0xcd, 0x53, 0x42, 0x08, 0x82, 0x67, 0x75,
  0x5e, 0xcb, 0x75, 0x56, 0x40, 0xdd, 0x01, 0xaf,
  0xd3, 0xa4, 0x02, 0xd8, 0x0a, 0x2c, 0x9e, 0x3b,
  0x57, 0xef, 0x8c, 0xfa, 0xef, 0x66, 0x70, 0xcc,
  0x0c, 0x5f, 0xf0, 0x74, 0x7f, 0xa5, 0x9b, 0x4d,
  0x7d, 0x9b, 0xa2, 0xcb, 0xdc, 0xb1, 0x5a, 0xad,
  0xf6, 0x03, 0x97, 0xb9, 0xf1, 0xd9, 0x2d, 0x90,
  0xc0, 0x6a, 0xb7, 0x7a, 0xda, 0x81, 0x19, 0x90,
  0x0f, 0xb6, 0xda, 0x52, 0x45, 0xf7, 0x41, 0x97,
  0x1a, 0x42, 0x0c, 0x92, 0x52, 0x42, 0xf9, 0x6b,
  0x65, 0x3a, 0x3f, 0xdf, 0x59, 0x75, 0xc1, 0x1d,
  0xb6, 0xdc, 0x7e, 0x2c, 0x22, 0x57, 0xc7, 0xc7,
  0xc7, 0x67, 0x8b, 0xd0, 0xa9, 0x23, 0xb8, 0x80,
  0xe8, 0xfc, 0x02, 0x5b, 0x29, 0xf9, 0x82, 0x61,
  0x10, 0xee, 0x0b, 0x94, 0x77, 0x95, 0x59, 0xf3,
  0xd9, 0x35, 0x13, 0xe2, 0xe4, 0xe7, 0x44, 0x7e,
  0x26, 0x2a, 0x37, 0x5a, 0x9b, 0x5b, 0x01, 0xe6,
  0xb4, 0xa1, 0x08, 0x4e, 0x1d, 0xe2, 0x6e, 0x27,
  0x5a, 0x94, 0x5c, 0x58, 0x30, 0xd7, 0x30, 0x88,
  0xd0, 0xb4, 0xa1, 0x89, 0xf2, 0xee, 0x32, 0xa5,
  0x6d, 0xa5, 0x7f, 0x23, 0xfc, 0x34, 0xe6, 0x71,
  0x50, 0x55, 0x27, 0x9d, 0x3a, 0xae, 0x5c, 0xb9,
  0x42, 0x6f, 0x6f, 0xef, 0x1c, 0x07, 0x9c, 0x23,
  0x09, 0x09, 0xe2, 0x57, 0x70, 0x0a, 0x32, 0xfd,
  0x3f, 0x61, 0xd1, 0x66, 0x0b, 0x70, 0xee, 0xfe,
  0x1b, 0x14, 0x1e, 0x2a, 0xb0, 0xe1, 0x9b, 0x1b,
  0x28, 0x6e, 0x2e, 0xfe, 0x13, 0xe3, 0x39, 0x8b,
  0xf6, 0x5b, 0x11, 0xc9, 0xcd, 0x8c, 0x2d, 0x5b,
  0xb6, 0x34, 0xb0, 0xa6, 0x6b, 0x20, 0x37, 0x2d,
  0x7d, 0xb2, 0x44, 0xba, 0x26, 0x5d, 0xd1, 0x31,
  0x8c, 0x40, 0xb5, 0x52, 0xa5, 0xf2, 0x4a, 0x85,
  0xfc, 0xbf, 0xf9, 0x6d, 0x4e, 0x34, 0x7f, 0xbc,
  0x99, 0xae, 0xaf, 0x77, 0xb1, 0x6a, 0xc3, 0xaa,
  0x61, 0x33, 0x7b, 0x66, 0xed, 0xfd, 0x6b, 0x5f,
  0xad, 0x5c, 0xae, 0x00, 0xb0, 0x6d, 0xdb, 0xb6,
  0x79, 0xb9, 0x1e, 0x18, 0xc7, 0x38, 0x51, 0x28,
  0x17, 0xb6, 0x16, 0x36, 0x15, 0xe2, 0xca, 0x76,
  0x96, 0xf5, 0xb7, 0xae, 0xdd, 0xea, 0x1e, 0x7f,
  0x6f, 0x9c, 0xa9, 0xa9, 0xa9, 0x46, 0xc7, 0x88,
  0x0a, 0x2d, 0x9f, 0x6e, 0xa1, 0xf3, 0xab, 0x9d,
  0x84, 0xfb, 0xc2, 0x5f, 0xcc, 0xec, 0xe9, 0x6a,
  0xb5, 0xfa, 0xde, 0x62, 0xe4, 0x00, 0x3e, 0xc6,
  0x38, 0xa6, 0xaa, 0xcf, 0x60, 0x64, 0xe4, 0xcb,
  0xb3, 0x8b, 0x48, 0xcd, 0xb0, 0xa7, 0x31, 0x7e,
  0xe2, 0x9c, 0x9b, 0xae, 0x1b, 0x11, 0xc4, 0x0b,
  0xa5, 0x47, 0x4a, 0xac, 0xed, 0x5b, 0x4b, 0xd2,
  0x92, 0xbc, 0x61, 0xd1, 0xf6, 0x8a, 0xc8, 0x99,
  0x2c, 0x64, 0x00, 0x6c, 0xdd, 0xba, 0x75, 0x41,
  0x3c, 0x3f, 0x73, 0xf4, 0xde, 0x9c, 0x19, 0xcb,
  0x46, 0x8c, 0x11, 0x51, 0xb9, 0x59, 0xaf, 0x1b,
  0xaf, 0x1e, 0x6d, 0x52, 0x5a, 0xbf, 0xd0, 0x4a,
  0xfb, 0x63, 0xed, 0x51, 0x57, 0xe9, 0x71, 0x8b,
  0xf6, 0x7d, 0x33, 0xbb, 0x50, 0xcf, 0xef, 0xe9,
  0xe9, 0x59, 0x14, 0xcf, 0x2f, 0x64, 0xcb, 0x52,
  0x31, 0x34, 0x34, 0xd4, 0xb8, 0x77, 0xe2, 0x48,
  0x5b, 0x52, 0xda, 0x1e, 0x6f, 0xa3, 0xe5, 0x91,
  0x96, 0xaa, 0x04, 0xf9, 0x95, 0xe5, 0xb6, 0x4f,
  0x55, 0x2b, 0xf5, 0xf7, 0x85, 0xe5, 0xf0, 0x3d,
  0x77, 0x11, 0xf5, 0x63, 0x37, 0x5d, 0x93, 0x52,
  0x7c, 0xbc, 0xc8, 0xea, 0xed, 0xab, 0x27, 0x70,
  0xbc, 0x68, 0xb9, 0xbd, 0x20, 0x22, 0xd7, 0x4b,
  0xa5, 0x12, 0xa3, 0xa3, 0xa3, 0x4b, 0xae, 0xfc,
  0xff, 0x12, 0x80, 0x81, 0x66, 0x4a, 0xfb, 0x13,
  0xed, 0xa4, 0x5d, 0xe9, 0x35, 0x94, 0xe7, 0x89,
  0x0c, 0x88, 0xc8, 0xa4, 0x73, 0x8e, 0x4b, 0x97,
  0x2e, 0xd1, 0xdb, 0xdb, 0xbb, 0x22, 0xa8, 0xbb,
  0x15, 0xa0, 0x12, 0x84, 0x74, 0x7d, 0x7a, 0x11,
  0x78, 0x4e, 0x90, 0xdf, 0x98, 0x58, 0x0e, 0xcc,
  0xeb, 0xf1, 0x95, 0xc4, 0xdd, 0xbe, 0x11, 0x9d,
  0x07, 0xde, 0xc1, 0x78, 0xea, 0xca, 0xbf, 0xae,
  0x1c, 0x35, 0x9b, 0x26, 0xbf, 0xd3, 0x7a, 0xba,
  0x2b, 0x07, 0x66, 0x0e, 0xaa, 0x13, 0x66, 0xf6,
  0xa7, 0xce, 0xce, 0xce, 0x51, 0x33, 0xe3, 0x6e,
  0xc9, 0x01, 0xfe, 0x07, 0xba, 0x9d, 0x4d, 0x87,
  0xde, 0x86, 0x7d, 0x55, 0x00, 0x00, 0x00, 0x25,
  0x74, 0x45, 0x58, 0x74, 0x64, 0x61, 0x74, 0x65,
  0x3a, 0x63, 0x72, 0x65, 0x61, 0x74, 0x65, 0x00,
  0x32, 0x30, 0x31, 0x37, 0x2d, 0x30, 0x37, 0x2d,
  0x32, 0x36, 0x54, 0x31, 0x34, 0x3a, 0x33, 0x33,
  0x3a, 0x32, 0x34, 0x2d, 0x30, 0x36, 0x3a, 0x30,
  0x30, 0xb8, 0x08, 0xda, 0xaa, 0x00, 0x00, 0x00,
  0x25, 0x74, 0x45, 0x58, 0x74, 0x64, 0x61, 0x74,
  0x65, 0x3a, 0x6d, 0x6f, 0x64, 0x69, 0x66, 0x79,
  0x00, 0x32, 0x30, 0x31, 0x37, 0x2d, 0x30, 0x37,
  0x2d, 0x32, 0x36, 0x54, 0x31, 0x34, 0x3a, 0x33,
  0x33, 0x3a, 0x32, 0x34, 0x2d, 0x30, 0x36, 0x3a,
  0x30, 0x30, 0xc9, 0x55, 0x62, 0x16, 0x00, 0x00,
  0x00, 0x19, 0x74, 0x45, 0x58, 0x74, 0x53, 0x6f,
  0x66, 0x74, 0x77, 0x61, 0x72, 0x65, 0x00, 0x77,
  0x77, 0x77, 0x2e, 0x69, 0x6e, 0x6b, 0x73, 0x63,
  0x61, 0x70, 0x65, 0x2e, 0x6f, 0x72, 0x67, 0x9b,
  0xee, 0x3c, 0x1a, 0x00, 0x00, 0x00, 0x00, 0x49,
  0x45, 0x4e, 0x44, 0xae, 0x42, 0x60, 0x82
};
