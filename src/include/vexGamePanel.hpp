/*==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
 ==============================================================================
    vexGamePanel.hpp
    declarationn of GamePanel Class (derived wxPanel)
    Game board is drawn on Game Panel and presented to user
*/

#pragma once

#include "vexDefn.hpp"
#include "vexTileProperty.hpp"
#include "vexGameBoard.hpp"

extern const wxSize MINIMUM_WIDGET_SIZE;

//==================== GamePanel
//One or more Game boards are drawn on Game Panel and presented to user
class GamePanel : public wxPanel
{
public:
    GamePanel( const TileProperty& property,
               wxWindow *parent,
               wxWindowID id,
               bool single_board = true );
    ~GamePanel();

    void    ReInitPanel( const TileProperty& property );

    void    OnPaint( wxPaintEvent& event );
    void    OnSize( wxSizeEvent& event );
    void    OnMouseDown( wxMouseEvent& event );
    void    OnMouseUp( wxMouseEvent& event );

    void    PanelGameReset();
    void    PanelGameSolve();
    void    PanelGamePause( const bool& bpause );

	bool	canMove(const int& controlID) const;
    inline bool isDirty() const           { return m_is_dirty; }
    void	move(const int& controlID);

private:
    void    ResetCursor();
    void    ReturnTile();
    void    SendPanelEvent();
    void    PanelSize( const wxSize& boardSize );

private:
    // only display a single(empty)board
    bool          m_single_board;
    bool          m_is_dirty;
    bool          m_is_solved;

    int           m_well_offset;
    TileProperty  m_PropertyPanel;

    // cursor data - slot contents in motion - mouse clicks
    bool          m_have_tile;
    TileData      m_CursorData;
    LOCATION      m_CursorLoc;
    short         m_CursorMap;
    //
    GameBoard*    m_board_game;
    GameBoard*    m_board_well;
};
