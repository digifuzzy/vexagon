/*==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
 ==============================================================================
    vexGameBoard.hpp
    declaration of GameBoard class
    Game Board contains m X n Game Slots
*/

#pragma once

#include "vexDefn.hpp"
#include "vexTileProperty.hpp"
#include "vexGameSlot.hpp"

//==================== Game Board
class GameBoard
{
public:
    GameBoard();
    GameBoard( const TileProperty& property,
               const wxSize& drawArea,
               const int& offset,
               const bool& withValues = false );
    GameBoard( const GameBoard& rhs ) = default;
    GameBoard( GameBoard&& rhs ) = default;
    ~GameBoard();

    GameBoard& operator =( const GameBoard& ) = default;
    GameBoard& operator =( GameBoard&& ) = default;

    void    ReInitBoard( const TileProperty& property,
                         const wxSize& drawArea,
                         const int& offset,
                         const bool& withValues = false );

    void    BoardPaint( wxGraphicsContext* gc );
    void    CursorPaint( wxGraphicsContext* gc,
                         const short& mapId );

    void    BoardResize( const wxSize& drawArea,
                         const int& offset);

    void    BoardGamePause( const bool& bPause = false );

    bool    BoardHasTile( const short& loc );
    bool    BoardHasTile( const wxPoint& loc );
    bool    BoardCanFitTile( const TileData& TileData,
                             const short& placement );
    short   LocateSlot( wxPoint boardLoc );
    void    SetSlot( const wxPoint& loc,
                     BoardSlot& slot );
    void    SetBoard( BoardSlots& copyboard,
                      const bool& usingLoc = false,
                      const bool& solving = false );
    bool    TileCursorMove( TileData& tempData,
                            const short& mapTile );

    wxSize  GetSlotSize();
    inline bool isSolved() const          { return m_is_solved; }
	bool	canMove(const int& controlID) const;
	void	move(const int& controlID);

    static BoardSlots MakeEmptyBoard( const wxPoint& boardDimensions );
private:
    void    BoardInitialize();

    void    calculateTiles( const int& tilesize, const wxPoint& origin );
    void    establishValues();
    void    UpdateBoardState();
    void    moveLeft();
    void    moveRight();
    void    moveUp();
    void    moveDown();

private:
    bool            m_is_solved;
    MOVE_FLAG_TYPE  m_moveFlag;
    TileProperty    m_PropertyBoard;
    BoardSlots      ma_Slots;
};
