/*==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
 ==============================================================================
    vexDialogs.hpp
    declaration of dialog classes in vexagon

    DlgNewBoard   - change type/size/base for new puzzle
    DlgGameSolved - user has solved current puzzle
    DlgHowTo      - information on how to play vexagon
*/

#pragma once

#include "vexDefn.hpp"
#include "vexDefnImages.hpp"
#include "vexTileProperty.hpp"

class GamePanel;

enum dlgCtrlIDs
{
    ID_DLG_BOARD_TYPE = wxID_HIGHEST + 1,
    ID_DLG_TILE_BASE,
    ID_DLG_SHAPE_SQR,
    ID_DLG_SHAPE_DIMD,
    ID_DLG_SHAPE_HEXL,
    ID_DLG_SHAPE_HEXR,
    ID_DLG_DIM_HORZ,
    ID_DLG_DIM_VERT
};

//------------
const long STYLE_DEFAULT_TEXTCTRL = wxTE_NO_VSCROLL
                                  | wxTE_MULTILINE
                                  | wxTE_BESTWRAP
                                  | wxTE_READONLY
                                  | wxBORDER_NONE;

#if defined (__WXMAC__) || defined (__WXOSX__) || defined (__WXOSX_COCOA__)
const int  DIALOG_FONT_MONO_SIZE  = 14;
#else
const int  DIALOG_FONT_MONO_SIZE  = 10;
#endif

//==================== Change Board Dialog
class DlgNewBoard : public wxDialog
{
public:
	DlgNewBoard( const TileProperty& property,
                 wxWindow *parent );

    virtual ~DlgNewBoard();

    void OnChoiceSelect(wxCommandEvent& event);
    void OnChangeDimensions(wxSpinEvent& event);
    void OnClose(wxCommandEvent& event);

    TileProperty getProperty() const { return m_PropertyDialog; }

private:
    TileProperty  m_PropertyDialog;
    wxChoice*     mpch_BoardType;
    wxSpinCtrl*   mpctrl_dim_horz;
    wxSpinCtrl*   mpctrl_dim_vert;
    wxChoice*     mpch_BoardBase;
    GamePanel*    mp_BoardSample;
};

//==================== GameSolved Dialog
class DlgGameSolved: public wxDialog
{
public:
    DlgGameSolved ( const TileProperty& property,
                    const long& elapsedtime,
                    wxWindow* parent );
    ~DlgGameSolved();

    void OnClose( wxCommandEvent& event );
private:
    wxFont  monoFont;
};

//==================== How-To Dialog
class DlgHowTo: public wxDialog
{
    public:
    DlgHowTo( wxWindow* parent,
              wxWindowID id,
              const wxString& title,
              const wxPoint& pos = wxDefaultPosition,
              const wxSize& size = wxDefaultSize,
              long style = wxFRAME_TOOL_WINDOW | wxCLOSE_BOX ,
              const wxString& name = wxDialogNameStr );
    virtual ~DlgHowTo();

    void OnClose( wxCommandEvent& WXUNUSED( event ) );

private:
    void MakeBasics( const int& sequence, const bool bSelected = false );
    void MakeControls( const int& sequence, const bool bSelected = false );
    void MakeSolvedDlg( const int& sequence, const bool bSelected = false );
    void MakeChangeDlg( const int& sequence, const bool bSelected = false );
    void MakeTips( const int& sequence, const bool bSelected = false );

private:
    wxSize      szImage;
	int			m_nLineWrap;
    wxToolbook* mp_Book;
    wxFont      monoFont;
    static int  m_nHorzCharCount;
};
