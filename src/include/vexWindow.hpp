/*==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
 ==============================================================================
    vexWindow.hpp
    declaration of application frame class (derived wxFrame)

    Frame draws user controls and Game Panel
*/

#pragma once

#include "vexDefn.hpp"
#include "vexTileProperty.hpp"
#include "vexGamePanel.hpp"

const int DEFAULT_POS_BOARD = 50;

#if defined ( __WXOSX__ )
const int DEFAULT_SIZE_BOARD_HORZ = 1280;
const int DEFAULT_SIZE_BOARD_VERT = 960;
const int fntStatusSize = 11;
#else
const int DEFAULT_SIZE_BOARD_HORZ = 640;
const int DEFAULT_SIZE_BOARD_VERT = 480;
const int fntStatusSize = 9;
#endif

//==================== vexWindow
class vexWindow : public wxFrame
{
public:
    vexWindow(const wxString& title);

    void OnQuit( wxCommandEvent& event );
    void OnAbout( wxCommandEvent& WXUNUSED( event) );
    void OnHowTo( wxCommandEvent& WXUNUSED( event) );
    void OnWindowStateChg( wxIconizeEvent& event );
    void OnGameFinished( wxCommandEvent& WXUNUSED( event) );

    void OnChangeBoard( wxCommandEvent& event );
    void OnGameNew( wxCommandEvent& event );
    void OnGamePause( wxCommandEvent& event );
    void OnGameSolve( wxCommandEvent& event );
    void OnGameRestart( wxCommandEvent& event );
    void OnGameMove( wxCommandEvent& event );
    void OnGameState( wxCommandEvent& event );
    void OnTimer( wxTimerEvent& WXUNUSED( event) );

private:
	void ManageGameControls();
    void EnableEvents(const bool& bEnable);
    void AppendMenu(wxMenu* parentMenu, const VCmdData& items);
    void AppendTools(wxToolBar* pTool, const VCmdData& items);
    void UpdateControlState();
    void InitializeMaps();

private:
    // Game State Variables
    bool          m_bStatePaused;
    bool          m_bStateSolved;
    wxFileConfig* mp_Config;
    // Time Variables
    wxStopWatch   m_stopwatch;
    wxTimer       m_timer;
    // Game Variables
    TileProperty  m_PropertyApp;
    GamePanel*    mp_MainPanel;
    // GUI variables
    wxMenu*       mp_FileMenu;
    wxMenu*       mp_GameMenu;
    wxMenu*       mp_HelpMenu;
    wxStatusBar*  mp_GameStatusBar;
    wxToolBar*    mp_GameTools;

    // use std::map container to set values for menu or event data.
    // Store it once and reuse often!!!
    // create vector of command data (using details stored in map )
    // then iterate over vector to create menus or control details
    typedef std::pair<void (vexWindow::*)( wxCommandEvent&),int> commandpair;
    std::vector<commandpair> m_cmds;
    std::map<int,CmdData>    m_ctrls;
};
