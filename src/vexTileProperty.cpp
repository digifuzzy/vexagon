/*==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
 ==============================================================================
    vexTileProperty.cpp
    implementation of shape properties
*/

#include "vexTileProperty.hpp"

const wxPoint DEFAULT_BOARD_SIZE    { wxPoint( BOARD_SIZE_MIN, BOARD_SIZE_MIN ) };

const wxString WXSTYPE_SQR      { wxT("Square") };
const wxString WXSTYPE_HEXODD   { wxT("Hexagon (ODD)") };
const wxString WXSTYPE_HEXEVEN  { wxT("Hexagon (EVEN)") };

const wxString WXSBASE_OCT      { wxT("Octal") };
const wxString WXSBASE_DEC      { wxT("Decimal") };
const wxString WXSBASE_HEX      { wxT("Hexadecimal") };

// When defining/building board - values used when populating
// memory overlay array with random numbers.
// Identity of array elements used to populate tile values
const wxPoint ValueIncrementColumn      { wxPoint( 2, 0 ) };
const wxPoint ValueIncrementHexOffset   { wxPoint( 0, 1 ) };
const wxPoint ValueIncrementRow         { wxPoint( 0, 2 ) };

// During play - identify surrounding tiles and sides
// used to ensure a tile can be placed at chosen point
// Use pair <wxPoint, side #>
// wxPoint is a relative offset to selected tile point
//                           col row  side
const CompareArr ArrCompareSqr = {
    CompareElement( wxPoint(  0, -1 ), 2 ),
    CompareElement( wxPoint(  1,  0 ), 3 ),
    CompareElement( wxPoint(  0,  1 ), 0 ),
    CompareElement( wxPoint( -1,  0 ), 1 )
};

//                           col row  side
const CompareArr ArrCompareHexPEAK = {
    CompareElement( wxPoint(  0, -1 ), 3 ),
    CompareElement( wxPoint(  1, -1 ), 4 ),
    CompareElement( wxPoint(  1,  0 ), 5 ),
    CompareElement( wxPoint(  0,  1 ), 0 ),
    CompareElement( wxPoint( -1,  0 ), 1 ),
    CompareElement( wxPoint( -1, -1 ), 2 ),
};

//                           col row  side
const CompareArr ArrCompareHexTROUGH = {
    CompareElement( wxPoint(  0, -1 ), 3 ),
    CompareElement( wxPoint(  1,  0 ), 4 ),
    CompareElement( wxPoint(  1,  1 ), 5 ),
    CompareElement( wxPoint(  0,  1 ), 0 ),
    CompareElement( wxPoint( -1,  1 ), 1 ),
    CompareElement( wxPoint( -1,  0 ), 2 ),
};

//==================== TileProperty - static
// adding to board types? adjust nBoardTypesCount
const wxString TileProperty::wxsaBoardTypes[] =
{
    WXSTYPE_SQR,
    WXSTYPE_HEXODD,
    WXSTYPE_HEXEVEN
};

// avoiding usage of sizeof(array)/sizeof(array[0])
// array can't be changed - if it is - adjust this value
const int TileProperty::nBoardTypesCount = 3;

// adding to board bases? adjust nBoardBasesCount
const wxString TileProperty::wxsaBoardBases[] = {
    WXSBASE_OCT,
    WXSBASE_DEC,
    WXSBASE_HEX
};

// avoiding usage of sizeof(array)/sizeof(array[0])
// array can't be changed - if it is - adjust this value
const int TileProperty::nBoardBasesCount = 3;

wxString TileProperty::TypeToString( const GameBoardType& type )
{
    wxString strRet;
    switch (type)
    {
    case( GBT_HEX_ODD ):
        strRet = WXSTYPE_HEXODD;
        break;
    case( GBT_HEX_EVEN ):
        strRet = WXSTYPE_HEXEVEN;
        break;
    case( GBT_SQUARE ):
    default:
        strRet = WXSTYPE_SQR;
        break;
    }
    return strRet;
}

GameBoardType TileProperty::StringToType( const wxString& strtype )
{
    GameBoardType retType;
    if( strtype.Cmp( WXSTYPE_SQR ) == 0 ) retType = GBT_SQUARE;
    else if( strtype.Cmp( WXSTYPE_HEXODD ) == 0 ) retType = GBT_HEX_ODD;
    else if( strtype.Cmp( WXSTYPE_HEXEVEN ) == 0 ) retType = GBT_HEX_EVEN;
    else retType = GBT_SQUARE;
    return retType;
}

wxString TileProperty::BaseToString( const GameBoardBase& base )
{
    wxString strRet;
    switch (base)
    {
    case(GBB_OCT):
        strRet = WXSBASE_OCT;
        break;
    case(GBB_HEX):
        strRet = WXSBASE_HEX;
        break;
    case(GBB_DEC):
    default:
        strRet = WXSBASE_DEC;
        break;
    }
    return strRet;
}

GameBoardBase TileProperty::StringToBase( const wxString& strbase )
{
    GameBoardBase retBase;
    if( strbase.Cmp( WXSBASE_OCT ) == 0 ) retBase = GBB_OCT;
    else if( strbase.Cmp( WXSBASE_DEC ) == 0 ) retBase = GBB_DEC;
    else if( strbase.Cmp( WXSBASE_HEX ) == 0 ) retBase = GBB_HEX;
    else retBase = GBB_DEC;
    return retBase;
}

double TileProperty::SizeHexHeight( const double& length )
{
    return std::nearbyint( length * SIN60 );
}

//FUTURE - restore diamond shape
//double TileProperty::SizeDiamondWidth( const double& length )
//{
//    return std::nearbyint( length * COS45 );
//}

const ColourMap TileProperty::DefaultColourMap = {
    { 0x0, ColourPenBrush( *wxBLACK,                     *wxWHITE ) },
    { 0x1, ColourPenBrush( wxColour( 0x80, 0x00, 0x80 ), *wxWHITE ) },  // Purple
    { 0x2, ColourPenBrush( wxColour( 0xE0, 0x00, 0x00 ), *wxWHITE ) },  // Medium Red
    { 0x3, ColourPenBrush( wxColour( 0x00, 0x00, 0xE0 ), *wxWHITE ) },  // Medium Blue
    { 0x4, ColourPenBrush( wxColour( 0xE4, 0x9C, 0x27 ), *wxBLACK ) },  // Orange
    { 0x5, ColourPenBrush( wxColour( 0x07, 0x90, 0x07 ), *wxWHITE ) },  // Medium Green
    { 0x6, ColourPenBrush( wxColour( 0x90, 0x10, 0x10 ), *wxWHITE ) },  // Maroon
    { 0x7, ColourPenBrush( wxColour( 0xD2, 0xB4, 0x8C ), *wxBLACK ) },  // Tan
    { 0x8, ColourPenBrush( wxColour( 0x00, 0x00, 0x80 ), *wxWHITE ) },  // Navy
    { 0x9, ColourPenBrush( wxColour( 0x3C, 0xB3, 0x71 ), *wxBLACK ) },  // Sea Green
    { 0xA, ColourPenBrush( wxColour( 0xBA, 0x55, 0xD3 ), *wxBLACK ) },  // Medium Orchid
    { 0xB, ColourPenBrush( wxColour( 0x46, 0x82, 0xB4 ), *wxWHITE ) },  // Steel Blue
    { 0xC, ColourPenBrush( wxColour( 0x50, 0x66, 0x2B ), *wxWHITE ) },  // Dk Olive Green
    { 0xD, ColourPenBrush( wxColour( 0xA7, 0x59, 0x35 ), *wxWHITE ) },  // Lt Brown
    { 0xE, ColourPenBrush( wxColour( 0x8F, 0xD0, 0xF0 ), *wxBLACK ) },  // Sky Blue
    { 0xF, ColourPenBrush( *wxWHITE,                     *wxBLACK ) }
};

//==================== TileProperty - instance
TileProperty::TileProperty()
{
    SetType( GBT_MINIMUM );
    SetBase( GBB_DEC );
    SetDimensions( DEFAULT_BOARD_SIZE );
}

TileProperty::TileProperty( const GameBoardType& type,
                            const GameBoardBase& base,
                            const wxPoint& dimensions )
{
    SetType( type );
    SetBase( base );
    SetDimensions( dimensions );
}

TileProperty::~TileProperty()
{}

void TileProperty::SetType( const GameBoardType& type )
{
    switch( type )
    {
        case( GBT_HEX_ODD ):
        case( GBT_HEX_EVEN ):
            m_tileType = type;
            m_tileNumSides = DEFAULT_SIDES_HEX;
            break;
        case( GBT_SQUARE ):
        default:
            m_tileType = GBT_SQUARE;
            m_tileNumSides = DEFAULT_SIDES_SQUARE;
            break;
    }
}

void TileProperty::SetType( const unsigned int& type )
{
    SetType( ( GameBoardType& ) type );
}

void TileProperty::SetType( const wxString& strtype )
{
    SetType( StringToType( strtype ) );
}

void TileProperty::SetBase( const GameBoardBase& base )
{
    switch( base )
    {
        case( GBB_OCT ):
        case( GBB_HEX ):
        case( GBB_DEC ):
            m_tileBase = base;
            break;
        default:
            m_tileBase = GBB_DEC;
            break;
    }
}

void TileProperty::SetBase( const unsigned int& base )
{
    SetBase( ( GameBoardBase& ) base );
}

void TileProperty::SetBase( const wxString& strbase )
{
    SetBase( StringToBase( strbase ) );
}

void TileProperty::SetDimensions( const wxPoint& dimensions )
{
    SetDimensions( dimensions.x, dimensions.y );
}

void TileProperty::SetDimensions( const int& width, const int& height )
{
    if(( width >= BOARD_SIZE_MIN )
    && ( width <= BOARD_SIZE_MAX ))
    {
        m_boardDimensions.x = width;
    }
    else
    {
        m_boardDimensions.x = BOARD_SIZE_MIN;
    }

    if(( height >= BOARD_SIZE_MIN )
    && ( height <= BOARD_SIZE_MAX ))
    {
        m_boardDimensions.y = height;
    }
    else
    {
        m_boardDimensions.y = BOARD_SIZE_MIN;
    }
}

wxPoint TileProperty::MapToPt( const short& mapid )
{
    // row -> horz -> x - division remainder
    // col -> vert -> y - division
    return wxPoint( ( mapid % m_boardDimensions.x ),
                    ( mapid / m_boardDimensions.x ) );
}

short TileProperty::PtToMap( const wxPoint& loc )
{
    if( !IsValidBoardPt( loc ) ) return NO_VALUE;
    else return ( loc.x + ( loc.y * m_boardDimensions.x ) );
}

short TileProperty::PtToMap( const wxPoint& loc,
                             const short& col_count )
{
    if( ( loc.x > ( int )col_count ) || ( loc.x < 0 ) ) return NO_VALUE;
    else return ( loc.x + ( loc.y * col_count ) );
}

bool TileProperty::IsValidBoardPt( const wxPoint& ptTest )
{
    bool bValidPoint = false;
    // validate tile point being sought - is in range?
    if( ( ptTest.x < m_boardDimensions.x )
     && ( ptTest.x >= 0 )
     && ( ptTest.y < m_boardDimensions.y )
     && ( ptTest.y >= 0 ) )
    {
        bValidPoint = true;
    }
    return bValidPoint;
}

wxString TileProperty::GetTypeAsString() const
{
    return TypeToString( m_tileType );
}

wxString TileProperty::GetBaseAsString() const
{
    return BaseToString( m_tileBase );
}

LineAngles TileProperty::GetInsideAngles() const
{
    // Angle of vertex that makes up side of shape
    LineAngles retAngles;
    switch( m_tileType )
    {
        case( GBT_HEX_ODD ):
        case( GBT_HEX_EVEN ):
            retAngles = { 330, 30, 90, 150, 210, 270 };
            break;
        case( GBT_SQUARE ):
        default:
            retAngles = { 315, 45, 135, 225 };
            break;
    }
    return retAngles;
}

ColourMap TileProperty::GetTileColourMap() const
{
    //TODO - fix colour appearance inconsistency between wxGTK and wxMSW
    ColourMap retMap;
    switch( m_tileBase )
    {
        case( GBB_OCT ):
            retMap =
            {
                { 0x0, DefaultColourMap.at(0x0) },
                { 0x1, DefaultColourMap.at(0x1) },
                { 0x2, DefaultColourMap.at(0x2) },
                { 0x3, DefaultColourMap.at(0x3) },
                { 0x4, DefaultColourMap.at(0x4) },
                { 0x5, DefaultColourMap.at(0x5) },
                { 0x6, DefaultColourMap.at(0x6) },
                { 0x7, DefaultColourMap.at(0xF) }
            };
            break;
        case( GBB_HEX ):
            retMap = DefaultColourMap;
            break;
        case( GBB_DEC ):
        default:
            retMap =
            {
                { 0x0, DefaultColourMap.at(0x0) },
                { 0x1, DefaultColourMap.at(0x1) },
                { 0x2, DefaultColourMap.at(0x2) },
                { 0x3, DefaultColourMap.at(0x3) },
                { 0x4, DefaultColourMap.at(0x4) },
                { 0x5, DefaultColourMap.at(0x5) },
                { 0x6, DefaultColourMap.at(0x6) },
                { 0x7, DefaultColourMap.at(0x7) },
                { 0x8, DefaultColourMap.at(0x8) },
                { 0x9, DefaultColourMap.at(0xF) }
            };
            break;
    }
    return retMap;
}

CompareArr TileProperty::GetCompareArr( const bool& OddColumnNumber ) const
{
    // During play - identify surrounding tiles and sides
    // used to ensure a tile can be placed at chosen point
    // Use pair <wxPoint, side #>
    // wxPoint is a relative offset to selected tile point
    CompareArr retArr;
    switch( m_tileType )
    {
        case( GBT_HEX_ODD ):
            if( OddColumnNumber ) retArr = ArrCompareHexTROUGH;
            else retArr = ArrCompareHexPEAK;
            break;
        case( GBT_HEX_EVEN ):
            if( OddColumnNumber ) retArr = ArrCompareHexPEAK;
            else retArr = ArrCompareHexTROUGH;
            break;
        case( GBT_SQUARE ):
        default:
            retArr = ArrCompareSqr;
            break;
    }
    return retArr;
}

ShapePoints TileProperty::PopulatePoints( const int& shapesize )
{
    ShapePoints retPts;
    retPts.reserve( m_tileNumSides );
    retPts.resize( m_tileNumSides );

    switch( m_tileType )
    {
        //all coordinate references are to origin(top|left)
        //point Zero is center of shape
        //Start center then top left corner. go clockwise
        case( GBT_HEX_ODD ):
        case( GBT_HEX_EVEN ):
        {
            int horz_half = (int)std::nearbyint( shapesize / 2 );
            int horz_qtr  = (int)std::nearbyint( shapesize / 4 );

            int vert_full = (int)SizeHexHeight( shapesize );
            int vert_half = (int)std::nearbyint( vert_full / 2 );

            //point L|R - flat up|down
            retPts[0] = wxPoint( horz_qtr,              0 );
            retPts[1] = wxPoint( horz_half + horz_qtr,  0 );
            retPts[2] = wxPoint( shapesize,             vert_half );
            retPts[3] = wxPoint( horz_half + horz_qtr,  vert_full );
            retPts[4] = wxPoint( horz_qtr,              vert_full );
            retPts[5] = wxPoint( 0,                     vert_half );
            break;
        }
        case( GBT_SQUARE ):
        default:
        {
            retPts[0] = wxPoint( 0,         0 );
            retPts[1] = wxPoint( shapesize, 0 );
            retPts[2] = wxPoint( shapesize, shapesize );
            retPts[3] = wxPoint( 0,         shapesize );
            break;
        }
    }
    return retPts;
}

ShapeColours TileProperty::PopulateShading()
{
    wxColour DEFAULT_SHADE_LOW( 64, 64, 64 );
    wxColour DEFAULT_SHADE_LOWMED( 96, 96, 96 );
    wxColour DEFAULT_SHADE_HIGH( 240, 240, 240 );
    wxColour DEFAULT_SHADE_HIGHMED( 192, 192, 192 );
    ShapeColours shading;

    switch( m_tileType )
    {
        case( GBT_HEX_ODD ):
        case( GBT_HEX_EVEN ):
        shading = {
                    DEFAULT_SHADE_HIGHMED,
                    DEFAULT_SHADE_LOWMED,
                    DEFAULT_SHADE_LOW,
                    DEFAULT_SHADE_LOWMED,
                    DEFAULT_SHADE_HIGHMED,
                    DEFAULT_SHADE_HIGH
                  };
            break;
        case( GBT_SQUARE ):
        default:
        shading = {
                    DEFAULT_SHADE_HIGH ,
                    DEFAULT_SHADE_LOWMED,
                    DEFAULT_SHADE_LOW,
                    DEFAULT_SHADE_HIGHMED
                  };
            break;
    }
    return shading;
}

ArrPoints TileProperty::PopulateValuePts()
{
    // When establishing tile values - id surrounding tiles to get values
    // in memory overlay array
    ArrPoints retArr;
    switch( m_tileType )
    {
        case( GBT_HEX_ODD ):
        case( GBT_HEX_EVEN ):
            retArr = {
                wxPoint( 1, 0 ),
                wxPoint( 2, 0 ),
                wxPoint( 2, 1 ),
                wxPoint( 1, 2 ),
                wxPoint( 0, 1 ),
                wxPoint( 0, 0 )
            };
            break;
        case( GBT_SQUARE ):
        default:
            retArr = {
                wxPoint( 1, 0 ),
                wxPoint( 2, 1 ),
                wxPoint( 1, 2 ),
                wxPoint( 0, 1 )
            };
            break;
    }
    return retArr;
}


wxPoint TileProperty::UpdateOffest( const short& currentCol )
{
    wxPoint retPt = wxPoint( 0, 0 );
    switch( m_tileType )
    {
        case( GBT_HEX_ODD ):
            if( ( currentCol % 2 ) != 0 ) retPt = ValueIncrementHexOffset;
            break;
        case( GBT_HEX_EVEN ):
            if( ( currentCol % 2 ) == 0 ) retPt = ValueIncrementHexOffset;
            break;
        case( GBT_SQUARE ):
        default:
            retPt = wxPoint( 0, 0 );
            break;
    }
    return retPt;
}

wxRealPoint TileProperty::PointCalculate()
{
    wxRealPoint retPt;
    int horizontal = m_boardDimensions.x;
    int vertical   = m_boardDimensions.y;
    switch( m_tileType )
    {
        case( GBT_HEX_ODD ):
        case( GBT_HEX_EVEN ):
            // horizontal: overlapping tiles ==> -0.25 for overlap for each column
            // vertical ==> +0.5 for overlapped tiles
            retPt = wxRealPoint( (double)( ( ( horizontal - 1) * 0.75 ) + 1 ),
                                 (double)( ( ( vertical * SIN60 ) + 0.5 ) ) );
            break;
        case( GBT_SQUARE ):
        default:
            retPt = wxRealPoint( (double)horizontal, (double)vertical );
            break;
    }
    return retPt;
}

wxPoint TileProperty::OffsetCalculate( const unsigned int& length )
{
    wxPoint retPt;
    switch( m_tileType )
    {
        case( GBT_HEX_ODD ):
        case( GBT_HEX_EVEN ):
            retPt = wxPoint( (int)( std::nearbyint( length * 3 / 4) ),
                             (int)SizeHexHeight( length / 2 ) );
            break;
        case( GBT_SQUARE ):
        default:
            retPt = wxPoint( (int)length, (int)length );
            break;
    }
    return retPt;
}

wxPoint TileProperty::OffsetStart( const unsigned int& length,
                                   const wxPoint& origin,
                                   wxPoint& offset )
{
    wxPoint retPt;
    switch( m_tileType )
    {
        case( GBT_HEX_ODD ):
            retPt = origin;
            offset.y += origin.y;
            break;
        case( GBT_HEX_EVEN ):
            retPt = wxPoint( origin.x,
                             origin.y + SizeHexHeight( length / 2 ) );
            break;
        case( GBT_SQUARE ):
        default:
            retPt = origin;
            break;
    }
    return retPt;
}

wxPoint TileProperty::OffsetNextInColumn( const unsigned int& length )
{
    wxPoint retPt;
    switch( m_tileType )
    {
        case( GBT_HEX_ODD ):
        case( GBT_HEX_EVEN ):
            retPt = wxPoint( 0, SizeHexHeight( length ) );
            break;
        case( GBT_SQUARE ):
        default:
            retPt = wxPoint( 0, (int)length );
            break;
    }
    return retPt;
}

wxPoint TileProperty::OffsetNextInRow( const unsigned int& length,
                                       const wxPoint& origin,
                                       const wxPoint& offset,
                                       const size_t& column,
                                       const wxPoint& currentLoc )
{
    wxPoint retPt;
    switch( m_tileType )
    {
        case( GBT_HEX_ODD ):
            retPt.x = currentLoc.x + offset.x;
            if(column % 2 == 0 ) retPt.y = offset.y;
            else retPt.y = origin.y;
            break;
        case( GBT_HEX_EVEN ):
            retPt.x = currentLoc.x + offset.x;
            if(column % 2 != 0 ) retPt.y = origin.y + SizeHexHeight( length / 2 );
            else retPt.y = origin.y;
            break;
        case( GBT_SQUARE ):
        default:
            retPt = wxPoint( ( currentLoc.x + offset.x ), origin.y );
            break;
    }
    return retPt;
}

int TileProperty::GapAndPad( const int& shadewidth )
{
    int retInt;
    switch( m_tileType )
    {
        case( GBT_HEX_ODD ):
        case( GBT_HEX_EVEN ):
            retInt = std::ceil( shadewidth * 7 / 10 );
            break;
        case( GBT_SQUARE ):
        default:
            retInt = std::nearbyint( shadewidth / 2 );
            break;
    }
    return retInt;
}

wxString TileProperty::toString() const
{
    wxString STR_FRMT_GAME_STR( wxT( "Type:%s Base:%s Dimensions:%s" ) );
    return wxString::Format( STR_FRMT_GAME_STR,
                             GetTypeAsString(),
                             GetBaseAsString(),
                             vexStrHelp::PtToString( m_boardDimensions ) );
}

wxString TileProperty::toStringDisplay() const
{
    wxString wxsFORMAT_CURRENT( wxT( "Board: %d x %d %s  Base: %s" ) );
    return wxString::Format( wxsFORMAT_CURRENT,
                             m_boardDimensions.x,
                             m_boardDimensions.y,
                             GetTypeAsString(),
                             GetBaseAsString() );
}

bool TileProperty::CanMoveLeftRight()
{
    bool bRet;
    switch( m_tileType )
    {
        case( GBT_HEX_ODD ):
        case( GBT_HEX_EVEN ):
            bRet = false;
            break;
        case( GBT_SQUARE ):
        default:
            bRet = true;
            break;
    }
    return bRet;
}

wxSize TileProperty::GetSlotSize( const ShapePoints& ptsPerimeter )
{
    wxSize retSize;
    int nXLeft;
    int nXRight;
    int nYUp;
    int nYDown;
    switch( m_tileType )
    {
        case( GBT_HEX_ODD ):
        case( GBT_HEX_EVEN ):
            //ASSERT - ptsPerimeter.size != DEFAULT_SIDES_HEX(?)
            nXLeft  = ptsPerimeter[ 5 ].x;
            nXRight = ptsPerimeter[ 2 ].x;
            nYUp    = ptsPerimeter[ 0 ].y;
            nYDown  = ptsPerimeter[ 4 ].y;
            break;
        case( GBT_SQUARE ):
        default:
            //ASSERT - ptsPerimeter.size != DEFAULT_SIDES_SQUARE(?)
            nXLeft  = ptsPerimeter[ 0 ].x;
            nXRight = ptsPerimeter[ 1 ].x;
            nYUp    = ptsPerimeter[ 0 ].y;
            nYDown  = ptsPerimeter[ 3 ].y;
            break;
    }
    retSize.SetWidth( nXRight - nXLeft );
    retSize.SetHeight( nYDown - nYUp );
    return retSize;
}

wxPoint TileProperty::GetSlotCenter( const ShapePoints& ptsPerimeter )
{
    wxPoint ptCenter;
    double dXLeft, dXRight, dYUp, dYDown;
    switch( m_tileType )
    {
        case( GBT_HEX_ODD ):
        case( GBT_HEX_EVEN ):
            //ASSERT - ptsPerimeter.size != DEFAULT_SIDES_HEX(?)
            dXLeft  = ptsPerimeter[ 5 ].x;
            dXRight = ptsPerimeter[ 2 ].x;
            dYUp    = ptsPerimeter[ 0 ].y;
            dYDown  = ptsPerimeter[ 4 ].y;
            break;
        case( GBT_SQUARE ):
        default:
            //ASSERT - ptsPerimeter.size != DEFAULT_SIDES_SQUARE(?)
            dXLeft  = ptsPerimeter[ 0 ].x;
            dXRight = ptsPerimeter[ 1 ].x;
            dYUp    = ptsPerimeter[ 0 ].y;
            dYDown  = ptsPerimeter[ 3 ].y;
            break;
    }
    ptCenter.x = std::nearbyint( dXLeft + ( ( dXRight - dXLeft ) / 2 ) );
    ptCenter.y = std::nearbyint( dYUp + ( ( dYDown - dYUp ) / 2 ) );
    return ptCenter;
}
