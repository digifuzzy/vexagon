/*==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
 ==============================================================================
    vexGameBoard.cpp
    definition of GameBoard class
    Game Board contains m X n Game Slots
*/

#include "vexGameBoard.hpp"
#include "vexApp.hpp"

//==================== Game Board - static
BoardSlots GameBoard::MakeEmptyBoard( const wxPoint& boardDimensions )
{
    BoardSlots emptyBoard;
    short map_size = boardDimensions.x * boardDimensions.y;
    for( short sIndx = 0; sIndx < map_size; ++sIndx )
    {
        emptyBoard[sIndx] = BoardSlot();
    }
    return emptyBoard;
}

//==================== Game Board - instance
GameBoard::GameBoard() :
    m_is_solved( false ),
    m_moveFlag( MOVE_NONE ),
    m_PropertyBoard(),
    ma_Slots()
{
    BoardInitialize();
}

GameBoard::GameBoard( const TileProperty& property,
                      const wxSize& drawArea,
                      const int& offset,
                      const bool& withValues ) :
    m_is_solved( false ),
    m_moveFlag( MOVE_NONE ),
    m_PropertyBoard( property ),
    ma_Slots()
{
    BoardInitialize();
    if( withValues ) establishValues();
    BoardResize( drawArea, offset );
}

GameBoard::~GameBoard()
{
    ma_Slots.clear();
}

void GameBoard::BoardPaint( wxGraphicsContext* gc )
{
    BoardSlots::const_iterator itr = ma_Slots.begin();
    for( ; itr != ma_Slots.end(); ++itr )
    {
        BoardSlot slot = (*itr).second;
        slot.SlotPaint( gc );
    }
}

void GameBoard::CursorPaint( wxGraphicsContext* gc,
                             const short& mapId )
{
    if( ( mapId < 0 ) || ( mapId > (short)ma_Slots.size() ) ) return;
    ma_Slots[ mapId ].CursorPaint( gc );
}

void GameBoard::ReInitBoard( const TileProperty& property,
                             const wxSize& drawArea,
                             const int& offset,
                             const bool& withValues )
{
    m_is_solved     = false;
    m_moveFlag      = MOVE_NONE;
    m_PropertyBoard = property;
    ma_Slots.clear();
    short map_size = m_PropertyBoard.GetDimensions().x * m_PropertyBoard.GetDimensions().y;
    for( short sIndx = 0; sIndx < map_size; ++sIndx )
    {
        // re-initialize game tiles
        ma_Slots[sIndx] = BoardSlot( property );
    }
    if( withValues ) establishValues();
    BoardResize( drawArea, offset );
}

void GameBoard::BoardResize( const wxSize& drawArea,
                             const int& offset )
{
    const wxRealPoint boarder( 0.25, 0.25 );
    //TODO - rework calculation - remove PointCalculate
    wxRealPoint sizeCalc = m_PropertyBoard.PointCalculate();
    wxRealPoint sizeCalcBorder = sizeCalc + boarder;

    double horizontal = std::nearbyint( drawArea.x / sizeCalcBorder.x );
    double vertical = std::nearbyint( drawArea.y / sizeCalcBorder.y );

    int ntileSize = ( int )wxMin( horizontal, vertical );

    double boardsize_horz = std::nearbyint( sizeCalc.x * ntileSize );
    double boardsize_vert = std::nearbyint( sizeCalc.y * ntileSize );
    int horz_gap = std::abs( drawArea.x - boardsize_horz );
    int vert_gap = std::abs( drawArea.y - boardsize_vert );

    if( horz_gap < 2 ) horz_gap = 2;
    if( vert_gap < 1 ) vert_gap = 1;
    int start_origin_x = offset + ( horz_gap / 2 );
    int start_origin_y = ( vert_gap / 2 );
    calculateTiles( ntileSize, wxPoint(start_origin_x, start_origin_y));
}

void GameBoard::BoardGamePause( const bool& bPause )
{
    for( auto& slot: ma_Slots )
    {
        slot.second.setPause( bPause );
    }
}

bool GameBoard::BoardHasTile(const short& loc)
{
    bool bReturn = false;
    if( ( loc >= 0 ) && ( loc < (short) ma_Slots.size() ) )
    {
        bReturn = ma_Slots[ loc ].hasTile();
    }
    return bReturn;
}

bool GameBoard::BoardHasTile(const wxPoint& loc)
{
    return BoardHasTile( m_PropertyBoard.PtToMap( loc ) );
}

bool  GameBoard::BoardCanFitTile( const TileData& TileData,
                                  const short& placement )
{
    bool bTileFits = false;
    if( ( placement < 0 ) || ( placement >= (short)ma_Slots.size() ) ) return bTileFits;
    wxPoint ptPlace = m_PropertyBoard.MapToPt( placement );

    short testside = 0;
    bool bOddColumn = ( ( ptPlace.x % 2 ) != 0 );
    CompareArr retArr = m_PropertyBoard.GetCompareArr( bOddColumn );
    for( CompareElement& element : retArr )
    {
        wxPoint ptOffset( element.first );
        int side = element.second;
        wxPoint testloc = ptPlace + ptOffset;
        short mapid = m_PropertyBoard.PtToMap( testloc );
        if( mapid != NO_VALUE )
        {
            BoardSlot testSlot = ma_Slots[mapid];
            if( !ma_Slots[mapid].hasTile() ) bTileFits = true;
            else
            {
                bTileFits = ( TileData.getSideValue( testside ) == testSlot.getSideValue( side ) );
                if( !bTileFits ) break;
            }
        }
        else
        {
            bTileFits = true;
        }
        ++testside;
    }
    return bTileFits;
}

// find tile region that contains global event location
short GameBoard::LocateSlot( wxPoint boardLoc )
{
    short retMap = NO_VALUE;
    BoardSlots::const_iterator itrSlots = ma_Slots.begin() ;
    for( ; itrSlots != ma_Slots.end(); ++itrSlots)
    {
        BoardSlot slot = (*itrSlots).second;
        if( slot.containsPt( boardLoc ) )
        {
            // encompassing tile found - get its board location
            retMap = (*itrSlots).first;
            break;
        }
    }
    return retMap;
}

void GameBoard::SetSlot( const wxPoint& loc,
                         BoardSlot& slot )
{
    short tileid = m_PropertyBoard.PtToMap( loc );
    if( tileid != NO_VALUE )
    {
        ma_Slots[ tileid ].swapData( slot );
    }
}

void GameBoard::SetBoard( BoardSlots& copyboard,
                          const bool& usingLoc,
                          const bool& solving )
{
    if( usingLoc )
    {
        for( BoardSlots::iterator itr = copyboard.begin();
             itr != copyboard.end();
             ++itr )
        {
            if( !(*itr).second.hasTile() ) continue;

            short placeLoc;
            if( solving ) placeLoc = (*itr).second.getMapSolve();
            else placeLoc = (*itr).second.getMapStart();
            if( placeLoc != NO_VALUE )
            {
                ma_Slots[ placeLoc ].swapData( (*itr).second );
            }
        }
    }
    else
    {
        BoardSlots::iterator itrCopy = copyboard.begin();
        BoardSlots::iterator itrBoard = ma_Slots.begin();
        for( ; itrBoard != ma_Slots.end(); ++itrBoard, ++itrCopy )
        {
            (*itrCopy).second.swapData( (*itrBoard).second );
        }
    }
    UpdateBoardState();
}

bool GameBoard::TileCursorMove( TileData& tempData,
                                const short& mapTile )
{
    bool bRet = false;
    if( ( mapTile < 0 ) || ( mapTile >= (short)ma_Slots.size() ) ) return bRet;
    if( ma_Slots[mapTile].hasTile() )
    {
        // moving tile from board to cursor
        ma_Slots[mapTile].swapData( tempData );
        UpdateBoardState();
        bRet = true;
    }
    else if( !ma_Slots[mapTile].hasTile() && tempData.hasValue() )
    {
        ma_Slots[mapTile].swapData( tempData );
        UpdateBoardState();
        bRet = true;
    }
    else
    {
        wxString stmtDebug = wxT("GameBoard::TileCursorMove - ELSE CONDITION");
        wxLogMessage( stmtDebug );
    }
    return bRet;
}

wxSize GameBoard::GetSlotSize()
{
    return ma_Slots[0].GetSlotSize();
}

bool GameBoard::canMove(const int& controlID) const
{
	bool bReturn = false;
	switch(controlID)
	{
		case(ID_GAMEMOVEUP):
		default:
			bReturn = (m_moveFlag & MOVE_N) > MOVE_NONE;
			break;
		case(ID_GAMEMOVEUPRT):
			bReturn = (m_moveFlag & MOVE_NE) > MOVE_NONE;
			break;
		case(ID_GAMEMOVERIGHT):
			bReturn = (m_moveFlag & MOVE_E) > MOVE_NONE;
			break;
		case(ID_GAMEMOVEDNRT):
			bReturn = (m_moveFlag & MOVE_SE) > MOVE_NONE;
			break;
		case(ID_GAMEMOVEDOWN):
			bReturn = (m_moveFlag & MOVE_S) > MOVE_NONE;
			break;
		case(ID_GAMEMOVEDNLT):
			bReturn = (m_moveFlag & MOVE_SW) > MOVE_NONE;
			break;
		case(ID_GAMEMOVELEFT):
			bReturn = (m_moveFlag & MOVE_W) > MOVE_NONE;
			break;
		case(ID_GAMEMOVEUPLT):
			bReturn = (m_moveFlag & MOVE_NW) > MOVE_NONE;
			break;
	}
	return bReturn;
}

void GameBoard::move(const int& controlID)
{
	switch(controlID)
	{
		case(ID_GAMEMOVEUP):
		default:
			moveUp();
			break;
		case(ID_GAMEMOVEUPRT):
			//moveUpRt();
			break;
		case(ID_GAMEMOVERIGHT):
			moveRight();
			break;
		case(ID_GAMEMOVEDNRT):
			//moveDnRt();
			break;
		case(ID_GAMEMOVEDOWN):
			moveDown();
			break;
		case(ID_GAMEMOVEDNLT):
			//moveDnLt();
			break;
		case(ID_GAMEMOVELEFT):
			moveLeft();
			break;
		case(ID_GAMEMOVEUPLT):
			//moveUpLt();
			break;
	}
}

void GameBoard::BoardInitialize()
{
    short map_size = m_PropertyBoard.GetDimensions().x * m_PropertyBoard.GetDimensions().y;
    ma_Slots.clear();
    for( short sIndx = 0; sIndx < map_size; ++sIndx )
    {
        // initialize game tiles
        ma_Slots[sIndx] = BoardSlot( m_PropertyBoard );
    }
}

void GameBoard::calculateTiles( const int& tilesize, const wxPoint& origin )
{
    // where to draw next tile
    wxPoint offsetCalc = m_PropertyBoard.OffsetCalculate( tilesize );
    // where to start drawing board
    wxPoint localorigin = m_PropertyBoard.OffsetStart( tilesize,
                                                       origin,
                                                       offsetCalc );
    // calculate origins of each tile
    wxPoint boarddim = m_PropertyBoard.GetDimensions();
    for( short horz = 0; horz < boarddim.x; ++horz)
    {
        for( short vert = 0; vert < boarddim.y; ++vert)
        {
            short mapid = m_PropertyBoard.PtToMap( wxPoint( horz, vert ) );
            ma_Slots[ mapid ].SlotResize( tilesize, localorigin );
            // setup for next iteration
            wxPoint colOffset = m_PropertyBoard.OffsetNextInColumn( tilesize );
            localorigin += colOffset;
        }
        // where to start top of next column
        localorigin = m_PropertyBoard.OffsetNextInRow( tilesize,
                                                       origin,
                                                       offsetCalc,
                                                       horz,
                                                       localorigin );
    }
}

void GameBoard::establishValues()
{
    wxPoint boarddim = m_PropertyBoard.GetDimensions();
    short max_row = ( boarddim.x * 2 ) + 2;
    short max_col = ( boarddim.y * 2 ) + 2;
    short max_size = max_row * max_col;

    BoardSlots srcBoard;
    short map_size = max_col * max_row;
    for( short sIndx = 0; sIndx < map_size; ++sIndx )
    {
        srcBoard[sIndx] = BoardSlot( m_PropertyBoard );
    }

	typedef std::vector< short > GeneratedBoard;
    GeneratedBoard boardValues;
    boardValues.resize( max_size );
    boardValues.reserve( max_size );
    std::fill( boardValues.begin(), boardValues.end(), NO_VALUE );

    // initialize seed generator
    unsigned seed = ( unsigned )wxGetLocalTime();
    //std::default_random_engine generator(seed);
    // initialize generator
    std::knuth_b generator( seed );
    std::uniform_int_distribution<short> distribution( 0, ( m_PropertyBoard.GetBase() - 1 ) );

    for( GeneratedBoard::iterator itr = boardValues.begin();
         itr != boardValues.end();
         ++itr)
    {
        *itr = (short)distribution(generator);
    }

    // create "solved board" in memory
    for( short horz = 0; horz < boarddim.x; ++horz)
    {
        wxPoint altX = horz * ValueIncrementColumn;
        for( short vert = 0; vert < boarddim.y; ++vert)
        {
            wxPoint altY = vert * ValueIncrementRow;
            wxPoint ValueLocOffset = altX + altY + m_PropertyBoard.UpdateOffest( horz );
            ArrPoints workingArr = m_PropertyBoard.PopulateValuePts();

            TileValues theValues;
            for( auto& valueLocation: workingArr )
            {
                wxPoint theLocation = valueLocation + ValueLocOffset;
                size_t valueLoc = m_PropertyBoard.PtToMap( theLocation, max_col );
                short theValue = boardValues[valueLoc];

                //if there is an error in getting a value - default to 0
                if( ( theValue < 0 )
                 || ( theValue > m_PropertyBoard.GetBase() ) ) theValue = 0;
                theValues.push_back(theValue);
            }
            short tileid = m_PropertyBoard.PtToMap( wxPoint( horz, vert ) );
            srcBoard[ tileid ].setData( theValues,
                                        wxPoint( horz, vert ),
                                        wxPoint( horz, vert ) );
        }
    }

    // fill game board randomly
    // randomize map keys
	typedef std::vector< short >	TileKeys;
    TileKeys vKeys;
    short boardsize = boarddim.x * boarddim.y;
    for( short index = 0; index < boardsize; ++index )
    {
        vKeys.push_back( index );
    }
    std::shuffle( vKeys.begin(), vKeys.end(), generator );

    // for each key - move into game board in sequence
    TileKeys::iterator itr = vKeys.begin();
    for( short index = 0; itr != vKeys.end(); ++itr, ++index )
    {
        TileData data = srcBoard.at( vKeys [ index ] ).getData();
        ma_Slots[ index ].setData( data.getValues(),
                                   m_PropertyBoard.MapToPt( index ),
                                   m_PropertyBoard.MapToPt( data.getSolve() ) );
    }
}

void GameBoard::UpdateBoardState()
{
    //TODO - rewrite - can be optimized/improved
    wxPoint boarddim = m_PropertyBoard.GetDimensions();
    // check move north
    bool bCanMove = false;
    short vert = 0;
    for( short horz = 0; horz < boarddim.x; ++horz)
    {
        short mapId = m_PropertyBoard.PtToMap( wxPoint( horz, vert ) );
        BoardSlot theSlot = ma_Slots[ mapId ];
        bCanMove = !theSlot.hasTile();
        if( !bCanMove ) break;
    }
    if( bCanMove ) m_moveFlag |= MOVE_N;
    else m_moveFlag &= ~(MOVE_N);


    // check move south
    bCanMove = false;
    vert = boarddim.y - 1;
    for( short horz = 0; horz < boarddim.x; ++horz)
    {
        short mapId = m_PropertyBoard.PtToMap( wxPoint( horz, vert ) );
        BoardSlot theSlot = ma_Slots[ mapId ];
        bCanMove = !theSlot.hasTile();
        if( !bCanMove ) break;
    }
    if( bCanMove ) m_moveFlag |= MOVE_S;
    else m_moveFlag &= ~(MOVE_S);


    // check move west
    // Left/Right Check disabled for hex
    if( m_PropertyBoard.CanMoveLeftRight() )
    {
        bCanMove = false;
        short horz = 0;
        for( short vert = 0; vert < boarddim.y; ++vert)
        {
            short mapId = m_PropertyBoard.PtToMap( wxPoint( horz, vert ) );
            BoardSlot theSlot = ma_Slots[ mapId ];
            bCanMove = !theSlot.hasTile();
            if( !bCanMove ) break;
        }
        if( bCanMove ) m_moveFlag |= MOVE_W;
        else m_moveFlag &= ~(MOVE_W);

        // check move east
        bCanMove = false;
        horz = boarddim.x - 1;
        for( short vert = 0; vert < boarddim.y; ++vert)
        {
            short mapId = m_PropertyBoard.PtToMap( wxPoint( horz, vert ) );
            BoardSlot theSlot = ma_Slots[ mapId ];
            bCanMove = !theSlot.hasTile();
            if( !bCanMove ) break;
        }
        if( bCanMove ) m_moveFlag |= MOVE_E;
        else m_moveFlag &= ~(MOVE_E);
    }

    //TODO - only check for solved board if no tiles can be moved
    bool bSolved = false;
    for( BoardSlots::iterator itr = ma_Slots.begin();
         itr != ma_Slots.end();
         ++itr )
    {
        bSolved = (*itr).second.hasTile();
        if( !bSolved ) break;
    }
    m_is_solved = bSolved;
}

void GameBoard::moveLeft()
{
    wxPoint boarddim = m_PropertyBoard.GetDimensions();
    for( short horz = 1; horz < boarddim.x; ++horz)
    {
        for( short vert = 0; vert < boarddim.y; ++vert)
        {
            short idSrc = m_PropertyBoard.PtToMap( wxPoint( horz, vert ) );
            short idDest = m_PropertyBoard.PtToMap( wxPoint( ( horz - 1 ), vert ) );
            ma_Slots[idSrc].swapData( ma_Slots[idDest] );
        }
    }
    UpdateBoardState();
}

void GameBoard::moveRight()
{
    wxPoint boarddim = m_PropertyBoard.GetDimensions();
    for( short horz = ( boarddim.x - 2 ); horz >= 0; --horz)
    {
        for( short vert = 0; vert < boarddim.y; ++vert)
        {
            short idSrc = m_PropertyBoard.PtToMap( wxPoint( horz, vert ) );
            short idDest = m_PropertyBoard.PtToMap( wxPoint( ( horz + 1 ), vert ) );
            ma_Slots[idSrc].swapData( ma_Slots[idDest] );
        }
    }
    UpdateBoardState();
}

void GameBoard::moveUp()
{
    wxPoint boarddim = m_PropertyBoard.GetDimensions();
    for( short horz = 0; horz < boarddim.x; ++horz)
    {
        for( short vert = 1; vert < boarddim.y; ++vert)
        {
            short idSrc = m_PropertyBoard.PtToMap( wxPoint( horz, vert ) );
            short idDest = m_PropertyBoard.PtToMap( wxPoint( horz, ( vert - 1 ) ) );
            ma_Slots[idSrc].swapData( ma_Slots[idDest] );
        }
    }
    UpdateBoardState();
}

void GameBoard::moveDown()
{
    wxPoint boarddim = m_PropertyBoard.GetDimensions();
    for( short horz = 0; horz < boarddim.x; ++horz)
    {
        for( short vert = ( boarddim.y - 2 ); vert >= 0; --vert)
        {
            short idSrc = m_PropertyBoard.PtToMap( wxPoint( horz, vert ) );
            short idDest = m_PropertyBoard.PtToMap( wxPoint( horz, ( vert + 1 ) ) );
            ma_Slots[idSrc].swapData( ma_Slots[idDest] );
        }
    }
    UpdateBoardState();
}
