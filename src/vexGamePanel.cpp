/*==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
 ==============================================================================
    vexGamePanel.cpp
    implementation of Game Panel class (derived wxPanel)
    Game board is drawn on Game Panel and presented to user
*/

#include "vexGamePanel.hpp"

const wxSize MINIMUM_WIDGET_SIZE( 250, 250 );

//==================== GamePanel
GamePanel::GamePanel( const TileProperty& property,
                      wxWindow *parent,
                      wxWindowID id,
                      bool single_board ):
    wxPanel( parent, id ),
    m_single_board( single_board ),
    m_is_dirty( false ),
    m_is_solved( false ),
    m_PropertyPanel( property ),
    m_have_tile( false ),
    m_CursorLoc( LOC_NONE ),
    m_CursorMap( NO_VALUE )
{
    // using wxAutoBufferedPaintDC in OnPaint - background style setting needed
    SetBackgroundStyle( wxBG_STYLE_PAINT );
    SetMinSize( MINIMUM_WIDGET_SIZE );
    wxSize drawArea = GetClientSize();
    if( drawArea.GetHeight() < MINIMUM_WIDGET_SIZE.GetHeight() )
    {
        drawArea.SetHeight( MINIMUM_WIDGET_SIZE.GetHeight() );
    }
    if( drawArea.GetWidth() < MINIMUM_WIDGET_SIZE.GetWidth() )
    {
        drawArea.SetWidth( MINIMUM_WIDGET_SIZE.GetWidth() );
    }

    if( !m_single_board )
    {
        wxSize eachboard( drawArea );
        m_well_offset = eachboard.GetWidth() / 2;
        eachboard.SetWidth( m_well_offset );
        m_well_offset += 1;
        m_board_game = new GameBoard( m_PropertyPanel,
                                      eachboard,
                                      0,
                                      false );
        m_board_well = new GameBoard( m_PropertyPanel,
                                      eachboard,
                                      m_well_offset,
                                      true );
        ResetCursor();
    }
    else
    {
        m_well_offset = 0;
        m_board_game = new GameBoard( m_PropertyPanel, drawArea, 0 );
        m_board_well = nullptr;
    }

    // connect event handlers
    Bind( wxEVT_PAINT, &GamePanel::OnPaint, this, wxID_ANY );
    Bind( wxEVT_SIZE, &GamePanel::OnSize, this, wxID_ANY );
    Bind( wxEVT_LEFT_DOWN, &GamePanel::OnMouseDown, this );
    Bind( wxEVT_LEFT_UP, &GamePanel::OnMouseUp, this );
}

GamePanel::~GamePanel()
{
    // disconnect event handlers
    Unbind( wxEVT_LEFT_UP, &GamePanel::OnMouseUp, this );
    Unbind( wxEVT_LEFT_DOWN, &GamePanel::OnMouseDown, this );
    Unbind( wxEVT_SIZE, &GamePanel::OnSize, this, wxID_ANY );
    Unbind( wxEVT_PAINT, &GamePanel::OnPaint, this, wxID_ANY );

    if( !m_single_board ) delete m_board_well;
    delete m_board_game;
}

void GamePanel::ReInitPanel(const TileProperty& property)
{
    m_PropertyPanel = property;
    wxSize drawArea = GetClientSize();
    if( !m_single_board )
    {
        m_is_dirty  = false;
        m_is_solved = false;

        wxSize eachboard( drawArea );
        m_well_offset = eachboard.GetWidth() / 2;
        eachboard.SetWidth( m_well_offset );
        m_well_offset += 1;
        m_board_game->ReInitBoard( m_PropertyPanel, eachboard, 0 );
        m_board_well->ReInitBoard( m_PropertyPanel, eachboard, m_well_offset, true );
        // Re-Init Cursor Data
        ResetCursor();
    }
    else
    {
        m_board_game->ReInitBoard( m_PropertyPanel, drawArea, 0 );
    }
    Refresh( true );
}

void GamePanel::OnPaint(wxPaintEvent &WXUNUSED(event))
{
    // let API determine buffering
    wxAutoBufferedPaintDC pdc( this );
    // clear any previous usage - line is needed!
    pdc.Clear();
    wxGraphicsContext *gc = wxGraphicsContext::Create( pdc );
    if( gc )
    {
        m_board_game->BoardPaint( gc );
        if( !m_single_board ) m_board_well->BoardPaint( gc );
        delete gc;
    }
}

void GamePanel::OnSize(wxSizeEvent &WXUNUSED(event))
{
    wxSize drawArea = GetClientSize();
    PanelSize( drawArea );
}

void GamePanel::OnMouseDown( wxMouseEvent& event )
{
    if( m_is_solved || m_single_board )
    {
        event.Skip();
        return;
    }
    // find if Screen Logical coordinates fit in any tiles
    wxPoint eventLoc = event.GetLogicalPosition( wxClientDC( this ) );
    short mapInGame = m_board_game->LocateSlot( eventLoc );
    short mapInWell = m_board_well->LocateSlot( eventLoc );
    if( ( mapInGame != NO_VALUE )
     || ( mapInWell != NO_VALUE ) )
    {
        bool bProcessCursor = false;

        wxColour DEFAULT_MASK( 192, 0, 128 );

        wxSize tilesize = m_board_game->GetSlotSize();
        wxBitmap* cursorBM = new wxBitmap( tilesize, wxBITMAP_SCREEN_DEPTH );

        wxMemoryDC memdc;
        memdc.SelectObject( *cursorBM );
        wxGraphicsContext* memGC = wxGraphicsContext::Create( memdc );
        memdc.SetBackground( wxBrush (DEFAULT_MASK) );
        memdc.Clear();

        if( ( mapInGame != NO_VALUE ) & m_board_game->BoardHasTile( mapInGame ) )
        {
            // get tile from game board
            m_board_game->CursorPaint( memGC, mapInGame );
            m_board_game->TileCursorMove( m_CursorData,
                                          mapInGame );
            m_CursorLoc = LOC_GAME;
            m_CursorMap = mapInGame;
            bProcessCursor = true;

        }
        else if( ( mapInWell != NO_VALUE ) & m_board_well->BoardHasTile( mapInWell ) )
        {
            // get tile from well board
            m_board_well->CursorPaint( memGC, mapInWell );
            m_board_well->TileCursorMove( m_CursorData,
                                          mapInWell );
            m_CursorLoc = LOC_WELL;
            m_CursorMap = mapInWell;
            bProcessCursor = true;
        }

        if( bProcessCursor )
        {
            // wxMSW uses small images as cursor (same as Qt by default all platforms)
            //TODO - look into replacement
            //
            // selected tile has been copied cursor slot
            m_have_tile = true;
            m_is_dirty = true;

            memdc.SelectObject( wxNullBitmap );
            // cursorBM should now be available
            wxImage img = cursorBM->ConvertToImage();
            img.SetMaskColour( DEFAULT_MASK.Red(),
                               DEFAULT_MASK.Green(),
                               DEFAULT_MASK.Blue() );
            delete cursorBM;

            // resize imgage of tile smaller than original
            tilesize.Scale( 0.75, 0.75 );
            img.Rescale( tilesize.x, tilesize.y );
            // "hotspot" of tile is center or 1/2 of current size
            tilesize.Scale( 0.5, 0.5 );
            img.SetOption(wxIMAGE_OPTION_CUR_HOTSPOT_X, tilesize.x );
            img.SetOption(wxIMAGE_OPTION_CUR_HOTSPOT_Y, tilesize.y );

            // use the image as the current cursor
            wxCursor cursorTile( img );
            SetCursor( cursorTile );
            SendPanelEvent();
            Refresh();
        }
    }
}

void GamePanel::OnMouseUp( wxMouseEvent& event )
{
    if( m_have_tile )   // !m_single_board
    {
        // has a tile been "captured"
        wxPoint eventLoc = event.GetLogicalPosition( wxClientDC( this ) );
        // find if Screen Logical coordinates fit in any tiles
        short mapInGame = m_board_game->LocateSlot( eventLoc );
        short mapInWell = m_board_well->LocateSlot( eventLoc );
        if( ( mapInGame != NO_VALUE )
         || ( mapInWell != NO_VALUE ) )
        {
            if( mapInGame != NO_VALUE )
            {
                // mouse up in game board
                if( ( !m_board_game->BoardHasTile(mapInGame) )
                 && ( m_board_game->BoardCanFitTile( m_CursorData, mapInGame) ) )
                {
                        m_board_game->TileCursorMove( m_CursorData, mapInGame );
                }
                else ReturnTile();
            }
            else if( mapInWell != NO_VALUE )
            {
                // mouse up in well board
                if( !m_board_well->BoardHasTile(mapInWell) )
                {
                    m_board_well->TileCursorMove( m_CursorData, mapInWell );
                }
                else ReturnTile();
            }
            m_CursorLoc = LOC_NONE;
            m_CursorMap = NO_VALUE;
            if( m_board_game->isSolved() )
            {
                //generate solved event
                m_is_solved = true;
                SetCursor( wxNullCursor );
                wxCommandEvent finEvent( vexFINISHEDEVENT );
                wxPostEvent( GetParent(), finEvent );
            }
        }
        else
        {
            // mouse up on screen but not in game or well boards
            ReturnTile();
        }
        m_have_tile = false;
        SetCursor( wxNullCursor );
        Refresh();
        SendPanelEvent();
    }
}

void GamePanel::PanelGameReset()
{
    if( m_single_board ) return;
    BoardSlots copyTiles = GameBoard::MakeEmptyBoard( m_PropertyPanel.GetDimensions() );
    // transfer all tiles on well board to temporary copy array
    m_board_well->SetBoard( copyTiles );
    // transfer all tiles in copy array to tiles on well board at start points
    m_board_well->SetBoard( copyTiles, true );

    // transfer all tiles on game board to temporary copy array
    m_board_game->SetBoard( copyTiles );
    m_board_well->SetBoard( copyTiles, true );
    // reset local
    m_is_dirty = false;
    m_is_solved = false;
    ResetCursor();

    Refresh( true );
}

void GamePanel::PanelGameSolve()
{
    if( m_single_board ) return;
    BoardSlots copyTiles = GameBoard::MakeEmptyBoard( m_PropertyPanel.GetDimensions() );
    // transfer all tiles on well board to temporary copy array
    m_board_game->SetBoard( copyTiles );
    // transfer all tiles in copy array to tiles on game board at solve points
    m_board_game->SetBoard( copyTiles, true, true );

    // transfer all tiles on game board to temporary copy array
    m_board_well->SetBoard( copyTiles );
    m_board_game->SetBoard( copyTiles, true, true );

    // reset local
    m_is_dirty = true;
    m_is_solved = true;
    ResetCursor();

    Refresh( true );
}

void GamePanel::PanelGamePause( const bool& bpause )
{
    m_board_game->BoardGamePause( bpause );
    if( !m_single_board ) m_board_well->BoardGamePause( bpause );
}

bool GamePanel::canMove(const int& controlID) const
{
	return m_board_game->canMove(controlID);
}

void GamePanel::move(const int& controlID)
{
	m_board_game->move(controlID);
	Refresh();
}

void GamePanel::ResetCursor()
{
    m_have_tile = false;
    TileSideCount sideCount = m_PropertyPanel.GetSideCount();
    TileValues emptyValues;
    emptyValues.reserve( sideCount );
    emptyValues.resize( sideCount );
    std::fill( emptyValues.begin(), emptyValues.end(), NO_VALUE );
    m_CursorData = TileData( emptyValues,
                             NO_VALUE,
                             NO_VALUE );
    m_CursorLoc  = LOC_NONE;
    m_CursorMap  = NO_VALUE;
}

void GamePanel::ReturnTile()
{
    // action to move "tile" back to last location
    if( m_CursorLoc == LOC_WELL )
    {

        m_board_well->TileCursorMove( m_CursorData,
                                      m_CursorMap );
    }
    else if( m_CursorLoc == LOC_GAME )
    {
        m_board_game->TileCursorMove( m_CursorData,
                                      m_CursorMap );
    }
    else
    {
        // something bad happened
        // lost tile reference
    }
    m_CursorLoc = LOC_NONE;
    m_CursorMap = NO_VALUE;
}

void GamePanel::SendPanelEvent()
{
    wxCommandEvent updateevent( wxEVT_MENU, GetId() );
    updateevent.SetEventObject( GetParent() );
    ProcessWindowEvent( updateevent );
}

void GamePanel::PanelSize( const wxSize& boardSize )
{
    if( !m_single_board )
    {
        wxSize eachboard( boardSize );
        m_well_offset = eachboard.GetWidth() / 2;
        eachboard.SetWidth( m_well_offset );
        m_well_offset += 1;
        m_board_game->BoardResize( eachboard, 0 );
        m_board_well->BoardResize( eachboard, m_well_offset );
    }
    else
    {
        m_board_game->BoardResize( boardSize, 0 );
    }
    this->Refresh( true );
}
