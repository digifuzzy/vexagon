/*==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
 ==============================================================================
    vexTileData.cpp
    Movable tile data contained in Game Slot
*/
#include "vexTileData.hpp"

//==================== Txfr Data
TileData::TileData() :
    m_MapStart( NO_VALUE ),
    m_MapSolve( NO_VALUE ),
    m_Values()
{
    // ensure fn hasValue returns
    m_Values.reserve( 1 );
    m_Values.resize( 1 );
    std::fill( m_Values.begin(), m_Values.end(), NO_VALUE );
}

TileData::TileData( const TileValues& inValues,
                    const short& mapstart,
                    const short& mapsolve) :
    m_MapStart( mapstart ),
    m_MapSolve( mapsolve ),
    m_Values( inValues )
{}

TileData::~TileData()
{
    m_Values.clear();
}

bool TileData::hasValue()
{
    return ( m_Values[0] != NO_VALUE );
}

size_t TileData::getDataSize()
{
    return m_Values.size();
}

short TileData::getSideValue( const short& side ) const
{
    short SideValue = NO_VALUE;
    if( ( side >= 0 )
     && ( side <= ( short )m_Values.size() ) )
    {
        SideValue = m_Values[side];
    }
    return SideValue;
}
