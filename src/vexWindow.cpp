/*==============================================================================
    Copyright (C) 2015-2018 Scott Furry

    This file is part of vexagon.

    vexagon is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    vexagon is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with vexagon.  If not, see <http://www.gnu.org/licenses/>.
 ==============================================================================
    vexWindow.cpp
    implementation of application frame ( derived wxFrame )

    Frame draws user controls and Game Panel
*/

#include "vexWindow.hpp"
#include "vexDefnImages.hpp"
#include "vexDialogs.hpp"
#include "wxWidgetsLicense.hpp"

#include "vexApp.hpp"
#include <wx/defs.h>

// define in same file as event is processed
wxDEFINE_EVENT( vexFINISHEDEVENT, wxCommandEvent);

//------------ Application - status strings - hover
const wxString strSTAT_CHANGE       { wxT( "Start a new game with different board settings" ) };
const wxString strSTAT_NEW          { wxT( "Start a new game with current board settings" ) };
const wxString strSTAT_PAUSE        { wxT( "Pause current game" ) };
const wxString strSTAT_RESUME       { wxT( "Resume current game" ) };
const wxString strSTAT_RESTART      { wxT( "Restart current game" ) };
const wxString strSTAT_SOLVE        { wxT( "Solve current game" ) };
const wxString strSTAT_MOVEUP       { wxT( "Move Game Tiles Up" ) };
const wxString strSTAT_MOVEUPRT     { wxT( "Move Game Tiles Up and Right" ) };
const wxString strSTAT_MOVERT       { wxT( "Move Game Tiles Right" ) };
const wxString strSTAT_MOVEDNRT     { wxT( "Move Game Tiles Down and Right" ) };
const wxString strSTAT_MOVEDN       { wxT( "Move Game Tiles Down" ) };
const wxString strSTAT_MOVEDNLT     { wxT( "Move Game Tiles Down and Left" ) };
const wxString strSTAT_MOVELT       { wxT( "Move Game Tiles Left" ) };
const wxString strSTAT_MOVEUPLT     { wxT( "Move Game Tiles Up and Left" ) };
//------------ Application - status strings - post action
const wxString strGAME_CHANGE       { wxT( "New Game - New Board Settings" ) };
const wxString strGAME_NEW          { wxT( "New Game - Current Board" ) };
const wxString strGAME_PAUSE        { wxT( "Pausing Game" ) };
const wxString strGAME_RESUME       { wxT( "Resuming Game" ) };
const wxString strGAME_RESTART      { wxT( "Restart Game" ) };
const wxString strGAME_SOLVE        { wxT( "Solving Game" ) };
const wxString strGAME_MOVEUP       { wxT( "Game Tiles Moved Up" ) };
const wxString strGAME_MOVEUPRT     { wxT( "Game Tiles Moved Up and Right" ) };
const wxString strGAME_MOVERT       { wxT( "Game Tiles Moved Right" ) };
const wxString strGAME_MOVEDNRT     { wxT( "Game Tiles Moved Down and Right" ) };
const wxString strGAME_MOVEDN       { wxT( "Game Tiles Moved Down" ) };
const wxString strGAME_MOVEDNLT     { wxT( "Game Tiles Moved Down and Left" ) };
const wxString strGAME_MOVELT       { wxT( "Game Tiles Moved Left" ) };
const wxString strGAME_MOVEUPLT     { wxT( "Game Tiles Moved Up and Left" ) };
const wxString strGAME_FINISHED     { wxT( "Puzzle Solved!" ) };
//------------ Application - settings Strings
const wxString strSET_GAME          { wxT( "/Game" ) };
const wxString strSET_GAME_HORZ     { wxT( "horz" ) };
const wxString strSET_GAME_VERT     { wxT( "vert" ) };
const wxString strSET_GAME_TYPE     { wxT( "type" ) };
const wxString strSET_GAME_BASE     { wxT( "base" ) };
const wxString strSET_FRAME         { wxT( "/Frame" ) };
const wxString strSET_FRAME_POSX    { wxT( "x" ) };
const wxString strSET_FRAME_POSY    { wxT( "y" ) };
const wxString strSET_FRAME_WIDTH   { wxT( "w" ) };
const wxString strSET_FRAME_HEIGHT  { wxT( "h" ) };
//------------ Change Board Dialog
const wxString vexCHGDLG_LABEL      { wxT( " New Game - Change Board " ) };
//------------ How To Dialog Strings
const wxString vexHT_LABEL          { wxT( " How To Play Vexagon " ) };

//==================== vexWindow
vexWindow::vexWindow( const wxString& title ):
    wxFrame( NULL, wxID_ANY, title ),
    m_bStatePaused( false ),
    m_bStateSolved( false ),
    m_timer( this, ID_GAMETIMER ),
    m_PropertyApp()
{
    // need to initialize map of control IDs - see function for reason
    InitializeMaps();
    // restore game state
    // read tile type and orientation
    mp_Config = wxGetApp().GetAppConfig();
    mp_Config->SetPath( strSET_GAME );
    int horz = mp_Config->Read( strSET_GAME_HORZ, BOARD_SIZE_MIN );
    int vert = mp_Config->Read( strSET_GAME_VERT, BOARD_SIZE_MIN );
    m_PropertyApp.SetType( mp_Config->Read( strSET_GAME_TYPE, GBT_MINIMUM ) );
    m_PropertyApp.SetBase( mp_Config->Read( strSET_GAME_BASE, GBB_DEC ) );
    m_PropertyApp.SetDimensions( wxPoint( horz, vert ) );
    wxLogMessage( m_PropertyApp.toString() );

    // restore frame position and size
    wxRect rScreen;
    mp_Config->SetPath( strSET_FRAME );
    rScreen.x       = mp_Config->Read( strSET_FRAME_POSX, DEFAULT_POS_BOARD );
    rScreen.y       = mp_Config->Read( strSET_FRAME_POSY, DEFAULT_POS_BOARD );
    rScreen.width   = mp_Config->Read( strSET_FRAME_WIDTH, DEFAULT_SIZE_BOARD_HORZ );
    rScreen.height  = mp_Config->Read( strSET_FRAME_HEIGHT, DEFAULT_SIZE_BOARD_VERT );
    wxLogMessage( "Settings Parsed and Loaded" );

    SetMinSize( wxSize( DEFAULT_SIZE_BOARD_HORZ, DEFAULT_SIZE_BOARD_VERT ) );
    SetIcon( vexImage::vexIcon( vexIMG_IDX_ICON ) );

    mp_FileMenu = new wxMenu;
    // make a vector of data items for file menu
    VCmdData fileMenuData = {
        m_ctrls[ ID_GAMENEWCHG ],
        m_ctrls[ ID_GAMENEW ],
        m_ctrls[ ID_GAMESEPARTOR ],
        m_ctrls[ wxID_EXIT ]
    };
    AppendMenu( mp_FileMenu, fileMenuData );

    mp_GameMenu = new wxMenu;
    // make a vector of data items for game menu
    VCmdData gameMenuData = {
        m_ctrls[ ID_GAMESOLVE ],
        m_ctrls[ ID_GAMERESTART ],
        m_ctrls[ ID_GAMESEPARTOR ],
        m_ctrls[ ID_GAMEPAUSE ],
        m_ctrls[ ID_GAMERESUME ],
        m_ctrls[ ID_GAMESEPARTOR ]
    };
    AppendMenu( mp_GameMenu, gameMenuData );

    // make a vector of data items for help menu
    mp_HelpMenu = new wxMenu;
    VCmdData gameHelpData = {
        m_ctrls[ ID_GAMEHOWTO ],
        m_ctrls[ ID_GAMESEPARTOR ],
        m_ctrls[ wxID_ABOUT ]
    };
    AppendMenu( mp_HelpMenu, gameHelpData );

    // put all the menus together
    wxMenuBar *menuBar = new wxMenuBar();
    menuBar->Append( mp_FileMenu, "&File" );
    menuBar->Append( mp_GameMenu, "&Game" );
    menuBar->Append( mp_HelpMenu, "&Help" );

    // status bar
    wxFont fntSTATUSBAR = wxFont( fntStatusSize,
                                  wxFONTFAMILY_MODERN,
                                  wxFONTSTYLE_NORMAL,
                                  wxFONTWEIGHT_NORMAL,
                                  false,
                                  wxEmptyString,
                                  wxFONTENCODING_SYSTEM );

    mp_GameStatusBar = CreateStatusBar( 3, wxSTB_DEFAULT_STYLE, -1 );
    mp_GameStatusBar->SetFont( fntSTATUSBAR );
    SetStatusText( wxEmptyString, 0 );

    // tool bar
    mp_GameTools = CreateToolBar();
    mp_GameTools->SetToolBitmapSize( DEFAULT_BITMAP_SIZE );
    // make a vector of data items for toolbar
    VCmdData toolDataSqr = {
        m_ctrls[ ID_GAMENEWCHG ],
        m_ctrls[ ID_GAMENEW ],
        m_ctrls[ ID_GAMESEPARTOR ],
        m_ctrls[ ID_GAMESOLVE ],
        m_ctrls[ ID_GAMERESTART ],
        m_ctrls[ ID_GAMESEPARTOR ],
        m_ctrls[ ID_GAMEPAUSE ],
        m_ctrls[ ID_GAMERESUME ],
        m_ctrls[ ID_GAMESEPARTOR ]
    };
    AppendTools( mp_GameTools, toolDataSqr );
    mp_GameTools->Realize();

    // setup display based on restored/default settings
    Move( rScreen.x, rScreen.y );
    SetSize( rScreen.width, rScreen.height );
    SetStatusText( m_PropertyApp.toStringDisplay(), 1 );
    // setup main game panel
    wxBoxSizer *sizer = new wxBoxSizer( wxVERTICAL );
    mp_MainPanel = new GamePanel( m_PropertyApp, this, ID_GAMEPANEL, false );
    sizer->Add( mp_MainPanel, 1, wxEXPAND );

    // final window setup
	ManageGameControls();
    SetSizer(sizer );
    SetMenuBar( menuBar );
    SetToolBar( mp_GameTools );
    Layout();
    // setup events and update game controls
    EnableEvents( true );
    UpdateControlState();

    // Start the clock
    m_timer.Start( 1000, wxTIMER_CONTINUOUS );

    wxLogMessage( "Frame Init Complete" );
}

void vexWindow::OnQuit( wxCommandEvent& event )
{
    // write out to config file
    if( nullptr != mp_Config )
    {
        // write tile type
        // write tile orientation
        // write board size
        mp_Config->SetPath( strSET_GAME );
        wxPoint board = m_PropertyApp.GetDimensions();
        mp_Config->Write( strSET_GAME_HORZ, ( long )board.x );
        mp_Config->Write( strSET_GAME_VERT, ( long )board.y );
        mp_Config->Write( strSET_GAME_TYPE, ( long )m_PropertyApp.GetType() );
        mp_Config->Write( strSET_GAME_BASE, ( long )m_PropertyApp.GetBase() );

        // save the frame position
        wxRect rScreen = GetRect();
        mp_Config->SetPath( strSET_FRAME );
        mp_Config->Write( strSET_FRAME_POSX,   ( long )rScreen.GetLeft() );
        mp_Config->Write( strSET_FRAME_POSY,   ( long )rScreen.GetTop() );
        mp_Config->Write( strSET_FRAME_WIDTH,  ( long )rScreen.GetWidth() );
        mp_Config->Write( strSET_FRAME_HEIGHT, ( long )rScreen.GetHeight() );

        mp_Config->Flush();
        wxLogMessage( "Settings Saved" );
        mp_Config = nullptr;
    }
    // disconnect events
    EnableEvents( false );
    // clear control maps
    m_ctrls.clear();
    m_cmds.clear();
    // signal to wxApp we're done
    Close( true );
}

void vexWindow::OnHowTo( wxCommandEvent& WXUNUSED( event) )
{
    wxLogMessage( "OnHowTo" );
    DlgHowTo* dlgHow = new DlgHowTo( ( wxWindow* )this,
                                     wxID_ANY,
                                     vexHT_LABEL );
    dlgHow->ShowModal();
    Refresh();
}

void vexWindow::OnAbout( wxCommandEvent& WXUNUSED( event) )
{
    wxAboutDialogInfo aboutInfo;
    aboutInfo.SetName( wxsAPPNAME );
    aboutInfo.SetVersion( wxString("\n" + wxsAPPVER ));
    aboutInfo.SetDescription( wxsAPPDESC );
    aboutInfo.SetCopyright( wxsAPPCOPY );
    aboutInfo.SetWebSite( wxsAPPURL );
    aboutInfo.AddDeveloper( wxsAPPDEV );
    aboutInfo.SetLicence( wxString( license_wxwidgets ) );
    wxAboutBox( aboutInfo, this);
}

void vexWindow::OnWindowStateChg( wxIconizeEvent& event )
{
    wxLogMessage( "OnWindowStateChg" );
    if( !m_bStateSolved && event.IsIconized() )
    {
        m_bStatePaused = true;
        mp_MainPanel->PanelGamePause( m_bStatePaused );
        UpdateControlState();
        m_stopwatch.Pause();
    }
    event.Skip();
}

void vexWindow::OnGameFinished( wxCommandEvent& WXUNUSED( event) )
{
    wxLogMessage( "vexWindow::OnGameFinished" );
    //Game is finished - reset cursor if set.
    SetCursor( wxNullCursor );
    mp_MainPanel->SetCursor( wxNullCursor );
    m_bStateSolved = true;
    UpdateControlState();

    wxString msg( strGAME_FINISHED );
    wxLogMessage( msg );
    SetStatusText( msg, 0 );
    m_stopwatch.Pause();

    DlgGameSolved* dlgSolved = new DlgGameSolved( m_PropertyApp,
                                                  (long)( m_stopwatch.Time() / 1000 ),
                                                  ( wxWindow* )this );
    dlgSolved->SetTitle( strGAME_FINISHED );
    // dialog return value used to trigger user selection
    int endModal = dlgSolved->ShowModal();
    if( endModal != wxID_CANCEL)
    {
        // ID_GAMENEWCHG - change board
        // ID_GAMENEW    - new game same board
        wxCommandEvent updateevent( wxEVT_MENU, endModal );
        updateevent.SetEventObject( this );
        ProcessWindowEvent( updateevent );
    }
}

void vexWindow::OnChangeBoard( wxCommandEvent& event )
{
    wxLogMessage( "OnGameNewChg" );
    DlgNewBoard* dlg = new DlgNewBoard( m_PropertyApp,
                                        ( wxWindow* )this );
    dlg->SetTitle( vexCHGDLG_LABEL );
    if( dlg->ShowModal() == wxID_OK )
    {
        // stop the clock
        m_stopwatch.Pause();
        // Get user selected board
        m_PropertyApp = dlg->getProperty();
        wxLogMessage( m_PropertyApp.toString() );
        // Reset game controls
        m_bStateSolved = false;
    	m_bStatePaused = false;
    	mp_MainPanel->ReInitPanel( m_PropertyApp );
		ManageGameControls();
        UpdateControlState();
        // Update display
        SetStatusText( strGAME_CHANGE, 0 );
        SetStatusText( m_PropertyApp.toStringDisplay(), 1 );
    	this->Refresh( true );
        // start the clock
        m_stopwatch.Start();
    } else {
        wxLogMessage( "Cancel selected" );
    }
    // stop event propagation
    event.Skip( true );
}

void vexWindow::OnGameNew( wxCommandEvent& event )
{
    wxLogMessage( "OnGameNew" );
    // stop the clock
    m_stopwatch.Pause();
    // reset certain controls
    m_bStatePaused = false;
    m_bStateSolved = false;
    // new board with current game settings
    mp_MainPanel->ReInitPanel( m_PropertyApp );
    // Update display
    SetStatusText( strGAME_NEW, 0 );
    SetStatusText( m_PropertyApp.toStringDisplay(), 1 );
    this->Refresh( true );
    // start the clock
    m_stopwatch.Start();
    UpdateControlState();
    // stop event propagation
    event.Skip( true );
}

void vexWindow::OnGamePause( wxCommandEvent& event )
{
    if( event.GetId() == ID_GAMEPAUSE ) m_bStatePaused = true;
    if( event.GetId() == ID_GAMERESUME ) m_bStatePaused = false;
    UpdateControlState();
    wxString msg;
    if( m_bStatePaused )
    {
        m_stopwatch.Pause();
        msg = strGAME_PAUSE;
    }
    else
    {
        m_stopwatch.Resume();
        msg = strGAME_RESUME;
    }
    mp_MainPanel->PanelGamePause( m_bStatePaused );
    wxLogMessage( msg );
    SetStatusText( msg, 0 );
    Refresh( true );
    event.Skip( true );
}

void vexWindow::OnGameRestart( wxCommandEvent& event )
{

    m_bStateSolved = false;
    m_bStatePaused = false;
    mp_MainPanel->PanelGameReset();
    UpdateControlState();

    m_stopwatch.Start();
    wxString msg( strGAME_RESTART );
    wxLogMessage( msg );
    SetStatusText( msg, 0 );
    event.Skip( true );
}

void vexWindow::OnGameSolve( wxCommandEvent& event )
{
    m_stopwatch.Pause();
    mp_MainPanel->PanelGameSolve();

    m_bStateSolved = true;
    m_bStatePaused = true;

    UpdateControlState();

    wxString msg( strGAME_SOLVE );
    wxLogMessage( msg );
    SetStatusText( msg, 0 );
    event.Skip( true );
}

void vexWindow::OnGameMove( wxCommandEvent& event )
{
	wxString evtMsg(wxEmptyString);
	int id = event.GetId();
	switch(id)
	{
		case(ID_GAMEMOVEUP):
			evtMsg = strGAME_MOVEUP;
			break;
		case(ID_GAMEMOVEUPRT):
			evtMsg = strGAME_MOVEUPRT;
			break;
		case(ID_GAMEMOVERIGHT):
			evtMsg = strGAME_MOVERT;
			break;
		case(ID_GAMEMOVEDNRT):
			evtMsg = strGAME_MOVEDNRT;
			break;
		case(ID_GAMEMOVEDOWN):
			evtMsg = strGAME_MOVEDN;
			break;
		case(ID_GAMEMOVEDNLT):
			evtMsg = strGAME_MOVEDNLT;
			break;
		case(ID_GAMEMOVELEFT):
			evtMsg = strGAME_MOVELT;
			break;
		case(ID_GAMEMOVEUPLT):
			evtMsg = strGAME_MOVEUPLT;
			break;
	}
	if(evtMsg != wxEmptyString)
	{
		mp_MainPanel->move(id);
		UpdateControlState();
		wxLogMessage( evtMsg );
		SetStatusText( evtMsg, 0 );
		event.Skip( true );
		
	}
}

void vexWindow::OnGameState( wxCommandEvent& event )
{
    // Update Game Controls
    UpdateControlState();
    // Stop event propagation
    event.Skip( true );
}

void vexWindow::OnTimer( wxTimerEvent& WXUNUSED( event) )
{
    long elapsed = m_stopwatch.Time() / 1000;
    long hours, minutes, seconds;
    vexTime::ElapsedCalculate( elapsed, hours, minutes, seconds );

    wxString timemsg = vexTime::TimeMsg( elapsed );
    SetStatusText( timemsg, 2 );
}

void vexWindow::AppendMenu( wxMenu* parentMenu, const VCmdData& items )
{
    //assert pMenu is null(?) - should not happen
    for( CmdData itemAdd: items )
    {
        if( ID_GAMESEPARTOR == itemAdd.cmdID ) parentMenu->AppendSeparator();
        else
        {
            wxMenuItem* pNewItem = new wxMenuItem
            (
                parentMenu, itemAdd.cmdID,
                wxString( itemAdd.txtMenu ),
                wxString( itemAdd.txtStatus )
            );
            pNewItem->SetBitmap( itemAdd.bitmapSm );
            parentMenu->Append(pNewItem );
        }
    }
}

void vexWindow::AppendTools( wxToolBar* pTool, const VCmdData& items )
{
    //assert pTool is null
    for( CmdData itemAdd: items )
    {
        if( ID_GAMESEPARTOR == itemAdd.cmdID ) pTool->AddSeparator();
        else
        {
            pTool->AddTool
            (
                itemAdd.cmdID,
                wxString( itemAdd.txtLabel ),
                itemAdd.bitmapLrg,
                wxNullBitmap,
                wxITEM_NORMAL,
                wxString( itemAdd.txtLabel ),
                wxString( itemAdd.txtStatus )
            );
        }
    }
}

void vexWindow::ManageGameControls()
{
	int CtrlsSqr[] = { 
		ID_GAMEMOVEUP,
		ID_GAMEMOVERIGHT,
        ID_GAMEMOVEDOWN,
        ID_GAMEMOVELEFT
	};
	int CtrlsHex[] = {
		ID_GAMEMOVEUPLT,
		ID_GAMEMOVEUP,
		ID_GAMEMOVEUPRT,
		ID_GAMEMOVEDNRT,
        ID_GAMEMOVEDOWN,
        ID_GAMEMOVEDNLT
	};
	
	// Ensure ANY control is removed from menu/toolbar
	//cycle through CtrlsSqr
	for(int& ctrl: CtrlsSqr)
	{
		//	remove each item from Game Menu
		if(mp_GameMenu->FindItem(ctrl,nullptr) != nullptr)
		{
			mp_GameMenu->Remove(ctrl);
		}
		//	remove each item from Toolbar
		if(mp_GameTools->FindById(ctrl) != nullptr)
		{
			mp_GameTools->RemoveTool(ctrl);
		}
	}
	//cycle through CtrlsHex
	for(int& ctrl: CtrlsHex)
	{
		//	remove each item from Game Menu
		if(mp_GameMenu->FindItem(ctrl,nullptr) != nullptr)
		{
			mp_GameMenu->Remove(ctrl);
		}
		//	remove each item from Toolbar
		if(mp_GameTools->FindById(ctrl) != nullptr)
		{
			mp_GameTools->RemoveTool(ctrl);
		}
	}	

	// Add controls back based on type
	GameBoardType boardType = m_PropertyApp.GetType();
	if( boardType == GBT_SQUARE)
	{
		//cycle through CtrlsSqr
		for(int& ctrl: CtrlsSqr)
		{
			//	append each item to Game Menu
			CmdData itemAdd = m_ctrls[ctrl];
            wxMenuItem* pNewItem = new wxMenuItem
            (
                mp_GameMenu, itemAdd.cmdID,
                wxString( itemAdd.txtMenu ),
                wxString( itemAdd.txtStatus )
            );
            pNewItem->SetBitmap( itemAdd.bitmapSm );
            mp_GameMenu->Append(pNewItem );
			//	append each item to Toolbar
            mp_GameTools->AddTool
            (
                itemAdd.cmdID,
                wxString( itemAdd.txtLabel ),
                itemAdd.bitmapLrg,
                wxNullBitmap,
                wxITEM_NORMAL,
                wxString( itemAdd.txtLabel ),
                wxString( itemAdd.txtStatus )
            );
		}
	}
	else if( (boardType == GBT_HEX_ODD) || (boardType == GBT_HEX_EVEN))
	{
		//cycle through CtrlsHex
		for(int& ctrl: CtrlsHex)
		{
			//	append each item to Game Menu
			CmdData itemAdd = m_ctrls[ctrl];
            wxMenuItem* pNewItem = new wxMenuItem
            (
                mp_GameMenu, itemAdd.cmdID,
                wxString( itemAdd.txtMenu ),
                wxString( itemAdd.txtStatus )
            );
            pNewItem->SetBitmap( itemAdd.bitmapSm );
            mp_GameMenu->Append(pNewItem );
			//	append each item to Toolbar
            mp_GameTools->AddTool
            (
                itemAdd.cmdID,
                wxString( itemAdd.txtLabel ),
                itemAdd.bitmapLrg,
                wxNullBitmap,
                wxITEM_NORMAL,
                wxString( itemAdd.txtLabel ),
                wxString( itemAdd.txtStatus )
            );
		}
	}
}

void vexWindow::EnableEvents( const bool& bEnable )
{
    if( bEnable )
    {
        // connect event handlers
		// m_cmds defined in InitializeMaps
        for( commandpair& cmd: m_cmds )
        {
            Bind( wxEVT_MENU, cmd.first, this, cmd.second );
        }
        Bind( wxEVT_ICONIZE, &vexWindow::OnWindowStateChg, this );
        Bind( wxEVT_TIMER, &vexWindow::OnTimer, this, ID_GAMETIMER );
        Bind( vexFINISHEDEVENT, &vexWindow::OnGameFinished, this );
    }
    else
    {
        // disconnect event handlers
        Unbind( vexFINISHEDEVENT, &vexWindow::OnGameFinished, this );
        Unbind( wxEVT_TIMER, &vexWindow::OnTimer, this, ID_GAMETIMER );
        Unbind( wxEVT_ICONIZE, &vexWindow::OnWindowStateChg, this );
        for( commandpair& cmd: m_cmds )
        {
            Unbind( wxEVT_MENU, cmd.first, this, cmd.second );
        }
    }
}

void vexWindow::UpdateControlState()
{
    bool bDirty = mp_MainPanel->isDirty();
    // m_bStateSolved set in OnGameFinished event
    // m_bStatePaused set in OnGamePause event

    // assumes controls are usable/accessible
    // Menu Controls
    mp_GameMenu->Enable( ID_GAMERESTART,        bDirty );
    mp_GameMenu->Enable( ID_GAMESOLVE,          bDirty && !m_bStateSolved );
    mp_GameMenu->Enable( ID_GAMEPAUSE,          !m_bStatePaused && !m_bStateSolved );
    mp_GameMenu->Enable( ID_GAMERESUME,         m_bStatePaused && !m_bStateSolved );
    //Toolbar controls
    mp_GameTools->EnableTool( ID_GAMERESTART,   bDirty );
    mp_GameTools->EnableTool( ID_GAMESOLVE,     bDirty && !m_bStateSolved );
    mp_GameTools->EnableTool( ID_GAMEPAUSE,     !m_bStatePaused && !m_bStateSolved );
    mp_GameTools->EnableTool( ID_GAMERESUME,    m_bStatePaused && !m_bStateSolved );
	
	// for given board type
	// cycle through allowable controls and test enable for each
	GameBoardType boardType = m_PropertyApp.GetType();
	if( boardType == GBT_SQUARE)
	{
		int CtrlsSqr[] = { 
			ID_GAMEMOVEUP,
			ID_GAMEMOVERIGHT,
			ID_GAMEMOVEDOWN,
			ID_GAMEMOVELEFT
		};
		for(int& ctrl: CtrlsSqr)
		{
			bool bCanMove = mp_MainPanel->canMove(ctrl);
			// Menu item for this control
			mp_GameMenu->Enable( ctrl,         !m_bStatePaused && bCanMove );
			// Toolbar item for this control
			mp_GameTools->EnableTool( ctrl,    !m_bStatePaused && bCanMove );
		}
	}
	else if( (boardType == GBT_HEX_ODD) || (boardType == GBT_HEX_EVEN))
	{
		int CtrlsHex[] = {
			ID_GAMEMOVEUPLT,
			ID_GAMEMOVEUP,
			ID_GAMEMOVEUPRT,
			ID_GAMEMOVEDNRT,
	        ID_GAMEMOVEDOWN,
	        ID_GAMEMOVEDNLT
		};
		for(int& ctrl: CtrlsHex)
		{
			bool bCanMove = mp_MainPanel->canMove(ctrl);
			// Menu item for this control
			mp_GameMenu->Enable( ctrl,         !m_bStatePaused && bCanMove );
			// Toolbar item for this control
			mp_GameTools->EnableTool( ctrl,    !m_bStatePaused && bCanMove );
		}
	}
    if( !m_bStatePaused && !m_bStateSolved && bDirty ) SetStatusText( wxEmptyString, 0 );
}

// associate control ID's with string and graphic data
void vexWindow::InitializeMaps()
{
    m_ctrls =
    {
        std::make_pair( ID_GAMESEPARTOR,
                        CmdData( ID_GAMESEPARTOR,
                                 wxEmptyString,
                                 wxEmptyString,
                                 wxEmptyString)),
        std::make_pair( wxID_EXIT,
                        CmdData( wxID_EXIT,
                                 wxEmptyString,
                                 wxT("&Quit\tCtrl+Q"),
                                 wxT("Quit Vexagon")
                        )),
        std::make_pair( wxID_ABOUT,
                        CmdData( wxID_ABOUT,
                                 wxEmptyString,
                                 wxT("&About\tF1"),
                                 wxT("About Vexagon")
                        )),
        std::make_pair( ID_GAMENEWCHG,
                        CmdData( ID_GAMENEWCHG,
                                 wxT("Change Board..."),
                                 wxT("C&hange Board...\tCtrl+H"),
                                 strSTAT_CHANGE,
                                 vexImage::vexBitmap(vexIMG_IDX_CHANGE),
                                 vexImage::vexBitmap(vexIMG_IDX_CHANGE_SM)
                        )),
        std::make_pair( ID_GAMENEW,
                        CmdData( ID_GAMENEW,
                                 wxT("New Puzzle"),
                                 wxT("&New Puzzle\tCtrl+N"),
                                 strSTAT_NEW,
                                 vexImage::vexBitmap(vexIMG_IDX_NEW),
                                 vexImage::vexBitmap(vexIMG_IDX_NEW_SM)
                        )),
        std::make_pair( ID_GAMEPAUSE,
                        CmdData( ID_GAMEPAUSE,
                                 wxT("Pause Game"),
                                 wxT("&Pause\tPAUSE"),
                                 strSTAT_PAUSE,
                                 vexImage::vexBitmap(vexIMG_IDX_PAUSE),
                                 vexImage::vexBitmap(vexIMG_IDX_PAUSE_SM)
                        )),
        std::make_pair( ID_GAMERESUME,
                        CmdData( ID_GAMERESUME,
                                 wxT("Resume Game"),
                                 wxT("&Resume\tCtrl+R"),
                                 strSTAT_RESUME,
                                 vexImage::vexBitmap(vexIMG_IDX_RESUME),
                                 vexImage::vexBitmap(vexIMG_IDX_RESUME_SM)
                        )),
        std::make_pair( ID_GAMERESTART,
                        CmdData( ID_GAMERESTART,
                                 wxT("Restart Game"),
                                 wxT("&Restart\tDel"),
                                 strSTAT_RESTART,
                                 vexImage::vexBitmap(vexIMG_IDX_RESET),
                                 vexImage::vexBitmap(vexIMG_IDX_RESET_SM)
                        )),
        std::make_pair( ID_GAMESOLVE,
                        CmdData( ID_GAMESOLVE,
                                 wxT("Solve Game"),
                                 wxT("&Solve\tCtrl+S"),
                                 strSTAT_SOLVE,
                                 vexImage::vexBitmap(vexIMG_IDX_SOLVE),
                                 vexImage::vexBitmap(vexIMG_IDX_SOLVE_SM)
                        )),
		//TODO - accelerators / keyboard shortcuts for board movement
		// Association to Number pad keys in wxW not functional at time of writing
		// Needs further investigation
        std::make_pair( ID_GAMEMOVEUP,
                        CmdData( ID_GAMEMOVEUP,
                                 wxT("Move Up"),
                                 wxT("Move Up"),
                                 strSTAT_MOVEUP,
                                 vexImage::vexBitmap(vexIMG_IDX_MV_UP),
                                 vexImage::vexBitmap(vexIMG_IDX_MV_UP_SM)
                        )),
        std::make_pair( ID_GAMEMOVEUPRT,
                        CmdData( ID_GAMEMOVEUPRT,
                                 wxT("Move Up and Right"),
                                 wxT("Move Up and Right"),
                                 strSTAT_MOVEUPRT,
                                 vexImage::vexBitmap(vexIMG_IDX_MV_UPRT),
                                 vexImage::vexBitmap(vexIMG_IDX_MV_UPRT_SM)
                        )),
        std::make_pair( ID_GAMEMOVERIGHT,
                        CmdData( ID_GAMEMOVERIGHT,
                                 wxT("Move Right"),
                                 wxT("Move Right"),
                                 strSTAT_MOVERT,
                                 vexImage::vexBitmap(vexIMG_IDX_MV_RT),
                                 vexImage::vexBitmap(vexIMG_IDX_MV_RT_SM)
                        )),
        std::make_pair( ID_GAMEMOVEDNRT,
                        CmdData( ID_GAMEMOVEDNRT,
                                 wxT("Move Down and Right"),
                                 wxT("Move Down and Right"),
                                 strSTAT_MOVEDNRT,
                                 vexImage::vexBitmap(vexIMG_IDX_MV_DNRT),
                                 vexImage::vexBitmap(vexIMG_IDX_MV_DNRT_SM)
                        )),
        std::make_pair( ID_GAMEMOVEDOWN,
                        CmdData( ID_GAMEMOVEDOWN,
                                 wxT("Move Down"),
                                 wxT("Move Down"),
                                 strSTAT_MOVEDN,
                                 vexImage::vexBitmap(vexIMG_IDX_MV_DN),
                                 vexImage::vexBitmap(vexIMG_IDX_MV_DN_SM)
                        )),
        std::make_pair( ID_GAMEMOVEDNLT,
                        CmdData( ID_GAMEMOVEDNLT,
                                 wxT("Move Down and Left"),
                                 wxT("Move Down and Left"),
                                 strSTAT_MOVEDNLT,
                                 vexImage::vexBitmap(vexIMG_IDX_MV_DNLT),
                                 vexImage::vexBitmap(vexIMG_IDX_MV_DNLT_SM)
                        )),
        std::make_pair( ID_GAMEMOVELEFT,
                        CmdData( ID_GAMEMOVELEFT,
                                 wxT("Move Left"),
                                 wxT("Move Left"),
                                 strSTAT_MOVELT,
                                 vexImage::vexBitmap(vexIMG_IDX_MV_LT),
                                 vexImage::vexBitmap(vexIMG_IDX_MV_LT_SM)
                        )),
        std::make_pair( ID_GAMEMOVEUPLT,
                        CmdData( ID_GAMEMOVEUPLT,
                                 wxT("Move Up and Left"),
                                 wxT("Move Up and Left"),
                                 strSTAT_MOVEUPLT,
                                 vexImage::vexBitmap(vexIMG_IDX_MV_UPLT),
                                 vexImage::vexBitmap(vexIMG_IDX_MV_UPLT_SM)
                        )),
        std::make_pair( ID_GAMEHOWTO,
                        CmdData( ID_GAMEHOWTO,
                                 wxEmptyString,
                                 wxT("How To...\tF2"),
                                 wxT("How To Play Vexagon")
                                ))
    };

    // associate frame function with control ID
    m_cmds =
    {
        ( std::make_pair( &vexWindow::OnQuit,       wxID_EXIT )       ),
        ( std::make_pair( &vexWindow::OnAbout,      wxID_ABOUT )      ),
        ( std::make_pair( &vexWindow::OnChangeBoard,ID_GAMENEWCHG )   ),
        ( std::make_pair( &vexWindow::OnGameNew,    ID_GAMENEW )      ),
        ( std::make_pair( &vexWindow::OnGamePause,  ID_GAMEPAUSE )    ),
        ( std::make_pair( &vexWindow::OnGamePause,  ID_GAMERESUME )   ),
        ( std::make_pair( &vexWindow::OnGameRestart,ID_GAMERESTART )  ),
        ( std::make_pair( &vexWindow::OnGameSolve,  ID_GAMESOLVE )    ),
        ( std::make_pair( &vexWindow::OnGameMove,	ID_GAMEMOVEUP )   ),
        ( std::make_pair( &vexWindow::OnGameMove,	ID_GAMEMOVEUPRT ) ),
        ( std::make_pair( &vexWindow::OnGameMove,	ID_GAMEMOVERIGHT )),
        ( std::make_pair( &vexWindow::OnGameMove,	ID_GAMEMOVEDNRT ) ),
        ( std::make_pair( &vexWindow::OnGameMove,	ID_GAMEMOVEDOWN ) ),
        ( std::make_pair( &vexWindow::OnGameMove,	ID_GAMEMOVEDNLT ) ),
        ( std::make_pair( &vexWindow::OnGameMove,	ID_GAMEMOVELEFT ) ),
        ( std::make_pair( &vexWindow::OnGameMove,	ID_GAMEMOVEUPLT ) ),
        ( std::make_pair( &vexWindow::OnHowTo,      ID_GAMEHOWTO    ) ),
        ( std::make_pair( &vexWindow::OnGameState,  ID_GAMEPANEL )    )
    };
}
