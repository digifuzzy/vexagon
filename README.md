Vexagon
=======

Similar in nature to Gnome's *tetravex*, **vexagon** is [wxWidgets](www.wxwidgets.org) tile puzzle game.<br/>
**vexagon** uses both square and hexagon tiles with different numerical bases(octal,decimal,hexadecimal).<br/>
Other features/improvements to follow...

Building
--------
See file ```BUILD.README``` for details

Status:<br/>
wxGTK - builds and executes<br/>
wxMAC - builds and executes<br/>
wxMSW - builds with some runtime issues (cursor/colours/text spacing)<br/>

TODO
-----
- improve autobuild configuration<br/>
- resolve cross-platform issues<br/>
- distribution packaging

Forward questions to information at furcat dot ca.
